<?php

class AlunosController extends \BaseController
{
    public function index()
    {

        return View::make('alunos.list');

    }


    public function copy($id)
    {
        $aluno = Aluno::find($id)->replicate();
        unset($aluno->foto);
        $aluno->nome .= " (Registro copiado)";
        $aluno->save();

        return Redirect::route('alunos.edit',$aluno->id);
    }


    public function autorizacao($id)
    {
        $aluno = Aluno::find($id);

        // PDF::SetTitle('Hello World');
        // PDF::SetAuthor('Daniel Lemes');
        // PDF::SetCreator('Daniel Lemes');

        PDF::AddPage();
        PDF::SetFont('times', '', 18);
        PDF::Multicell(0, 2, 'Autorização Uso de Imagem', 0, 'C');
        PDF::SetFont('times', '', 12);
        PDF::ln(25);


        if ($aluno->idade >= 18) {
            $texto = "Eu ".$aluno->nome." portador (a) RG ".$aluno->rg." ,
            Residente a Rua " . $aluno->endereco . ", ".$aluno->numero.", inscrito no PROJARI na(s)  ". implode(", ", $aluno->turmas->lists('nomeCompleto')) ." em ".$aluno->created_at->format('d/m/Y').
            " autorizo automaticamente  a Associação Beneficente São José, o uso de imagem,  para fins de editar, reproduzir,
            por meio de meios de jornais, revistas, sites e vídeos, sem qualquer ônus ou ressarcimento.
            ";
            $assina = $aluno->nome;

        } else {

            $texto = "Eu, ".$aluno->familiar_nome1.", portador do RG/CPF ".$aluno->familiar_documento1.", responsável legal pelo participante ".$aluno->nome.", inscrito no PROJARI na(s) Turma(s) ".implode(", ", $aluno->turmas->lists('nomeCompleto'))." em ".$aluno->created_at->format('d/m/Y').", autorizo automaticamente a Associação Beneficente São José, o uso de imagem, para fins de editar, reproduzir, por meio de jornais, revistas, site e vídeos, sem qualquer ônus ou ressarcimento.";

            $assina = $aluno->familiar_nome1;
        }

        PDF::Multicell(0, 2, $texto);

        PDF::ln(25);

        PDF::SetFont('times', '', 18);
        PDF::Multicell(0, 2, 'Orientação para os pais', 0, 'C');
        PDF::SetFont('times', '', 12);
        PDF::ln(25);
        PDF::Multicell(0, 2, '1. Cada participantes pode se inscrever em até três oficinas, sem colisão de horário;');
        PDF::Multicell(0, 2, '2. Os participantes devem respeitar e observar os dias e horários(de entrada e saída) de suas atividades;');
        PDF::Multicell(0, 2, '3. Somente será permitido o acesso ao interior da entidade aos participantes inscritos nas oficinas com dias e horários determinados;');
        PDF::Multicell(0, 2, '4. Os participantes devem permanecer na sala de suas atividades durante a permanência na instituição;');
        PDF::Multicell(0, 2, '5. Os participantes devem vestir a roupa adequada para a atividades que realiza;');
        PDF::Multicell(0, 2, '6. A  data e horário de participação na oficina irá respeitar o horário de frequência escolar;');
        PDF::Multicell(0, 2, '7. Para assegurar o bom desempenho da atividade, os pais e responsáveis devem zelar pela participação e o comprometimento em todas as oficinas e atividades complementares; ');
        PDF::Multicell(0, 2, '8. Os pais e familiares devem avisar imediatamente a entidade em qualquer caso de doença ou impossibilidade de participação da atividade;');
        PDF::Multicell(0, 2, '9. A entidade oferece, de acordo com a legislação, estrutura adequada para todas as oficinas, equipamentos e materiais didático-pedagógicos, instrutores capacitados e  lanche no intervalo das oficinas;');
        PDF::Multicell(0, 2, '10.  Para participar em atividades, competições e eventos a entidade oferece (conforme a disponibilidade) transporte e acompanhamento de responsáveis. As saídas somente serão realizadas com autorização dos pais e responsáveis, respeitando os horários previstos;');
        PDF::Multicell(0, 2, '11. Os pais ou responsáveis devem assinar no ato da inscrição a autorização para uso de imagem;');
        PDF::Multicell(0, 2, '12. Mediante atos de indisciplina ou danos a equipamentos e estruturas os pais ou responsáveis devem comparecer na entidade para abordagem da ocorrência, com a equipe técnica e direção;');
        PDF::Multicell(0, 2, '13. No ato da inscrição, os pais ou responsáveis devem informar se o participante está autorizado a deixar sozinho a instituição ou sairá acompanhado de familiar após a atividade. Nesse caso, a pessoa deve se identificar na recepção;');
        PDF::Multicell(0, 2, '14. Roupas ou figurinos usadas em apresentações, ensaios ou em eventos devem ser devolvidas ao monitor no final da atividade. No entanto, as roupas do esporte ou artes marciais serão levadas para casa e devem retornar com o participante limpas e em perfeita condição de uso;');
        PDF::Multicell(0, 2, '15. A comunicação via celular dos pais com participante deve ser realizada antes ou após o término da atividade. Durante a oficina, os aparelhos eletrônicos permanecerão recolhido  em caixas identificadas. Em caso de emergência, ligar para o telefone da entidade: (51) 3491 3266;');
        PDF::Multicell(0, 2, '16. No ato de inscrição os pais ou responsáveis devem informar as condições de saúde dos filhos(respondendo à questionário) e apresentar atestado médico até o início das atividades em 1º de março');

        PDF::ln(25);

        PDF::Multicell(0, 2, '______________________________________', 0, 'C');
        PDF::Multicell(0, 2, $assina, 0, 'C');

        PDF::ln(10);

        foreach ($aluno->turmas as $oficina) {
            PDF::Multicell(0, 2, 'Turma '.$oficina->nomeCompleto, 0, 'C');
            PDF::SetFillColor(255, 255, 255);
            PDF::ln(10);
            PDF::MultiCell(90, 2, 'Horário: '.substr($oficina->hora_inicio, 0, 5)." às ".substr($oficina->hora_fim, 0, 5), 0, 'L', 1, 0);
            PDF::MultiCell(90, 2, 'Turno: '.$oficina->turno, 0, 'L', 0, 1);
            PDF::Multicell(0, 2, 'Dia da semana: '.$oficina->dias, 0, 'L');

            if (!empty($aluno->oficinas[0]->obs_autorizacao)) {
                PDF::Multicell(0, 2, $oficina->obs_autorizacao, 0, 'J');
            }
            PDF::ln(10);

        }

        $rodape = "Avenida João Salazar, 250 – Bairro Bom Fim – Guaíba/RS – CEP 92500-000
                Telefone: (51) 3491-3266 –www.projari.com.br";

        PDF::Output(Str::slug($aluno->nome).'.pdf');
    }


    public function postSave()
    {
        $data = Input::all();

        if(isset($data)) {
            //$aluno['id'];
            if(isset($data['id']))
                $aluno = Aluno::find($data['id']);
            else
                $aluno = new Aluno;



            $aluno->nome = $data['nome'];
            $aluno->nascimento = $data['nascimento'];
            $aluno->sexo = $data['sexo'];
            $aluno->estado_civil_id = $data['estado_civil_id'];
            $aluno->etnia_id = $data['etnia_id'];
            //$aluno->foto = $data['foto'];
            $aluno->escola_id = $data['escola_id'];
            $aluno->serie = $data['serie'];
            $aluno->turno = $data['turno'];
            $aluno->carteira_escolar = $data['carteira_escolar'];
            $aluno->deslocamento = $data['deslocamento'];
            $aluno->ja_atendido = $data['ja_atendido'];
            $aluno->carteira_vacinacao = $data['carteira_vacinacao'];
            $aluno->medicacao = $data['medicacao'];
            $aluno->medicacao_nome = $data['medicacao_nome'];
            $aluno->atestado_medico = $data['atestado_medico'];
            $aluno->alergia = $data['alergia'];
            $aluno->alergia_qual = $data['alergia_qual'];
            $aluno->emergencia_nome = $data['emergencia_nome'];
            $aluno->emergencia_endereco = $data['emergencia_endereco'];
            $aluno->emergencia_fone = $data['emergencia_fone'];
            $aluno->emergencia_parentesco = $data['emergencia_parentesco'];
            $aluno->encaminhamento = $data['encaminhamento'];
            $aluno->encaminhamento_outros = $data['encaminhamento_outros'];
            $aluno->doenca_familia = $data['doenca_familia'];
            $aluno->doenca_qual = $data['doenca_qual'];
            $aluno->doenca_qual_familiar = $data['doenca_qual_familiar'];
            $aluno->doenca_tratamento = $data['doenca_tratamento'];
            $aluno->doenca_medico = $data['doenca_medico'];
            $aluno->gestante_familia = $data['gestante_familia'];
            $aluno->gestante_quem = $data['gestante_quem'];
            $aluno->gestante_mes = $data['gestante_mes'];
            $aluno->gestante_pre_natal = $data['gestante_pre_natal'];
            $aluno->necessidade_especial = $data['necessidade_especial'];
            $aluno->necessidade_qual = $data['necessidade_qual'];
            $aluno->necessidade_acompanhamento = $data['necessidade_acompanhamento'];
            $aluno->necessidade_bpc = $data['necessidade_bpc'];
            $aluno->endereco = $data['endereco'];
            $aluno->numero = $data['numero'];
            $aluno->bairro = $data['bairro'];
            $aluno->fone = $data['fone'];
            $aluno->nis = $data['nis'];
            $aluno->rg = $data['rg'];

            $aluno->medida_protecao = $data['medida_protecao'];
            $aluno->medida_conselheiro = $data['medida_conselheiro'];
            $aluno->conheceu = $data['conheceu'];
            $aluno->ja_fez_parte = $data['ja_fez_parte'];
            $aluno->obs = $data['obs'];

            for ($i = 1; $i <= 14; $i++){
                $familiar_nome = 'familiar_nome'.$i;
                $familiar_documento = 'familiar_documento'.$i;
                $familiar_parentesco = 'familiar_parentesco'.$i;
                $familiar_idade = 'familiar_idade'.$i;
                $familiar_sexo = 'familiar_sexo'.$i;
                $familiar_escolaridade = 'familiar_escolaridade'.$i;
                $familiar_profissao = 'familiar_profissao'.$i;
                $familiar_salario = 'familiar_salario'.$i;

                $aluno->$familiar_nome = $data['familiar_nome' . $i];

                $aluno->$familiar_parentesco = $data['familiar_parentesco' . $i];
                $aluno->$familiar_documento = $data['familiar_documento' . $i];
                $aluno->$familiar_idade = $data['familiar_idade' . $i];
                $aluno->$familiar_sexo = $data['familiar_sexo' . $i];
                $aluno->$familiar_escolaridade = $data['familiar_escolaridade' . $i];
                $aluno->$familiar_profissao = $data['familiar_profissao' . $i];
                $aluno->$familiar_salario = $data['familiar_salario' . $i];
            }

            $aluno->responsavel_situacao = $data['responsavel_situacao'];
            $aluno->responsavel_atividade = $data['responsavel_atividade'];
            $aluno->beneficio = $data['beneficio'];
            $aluno->bolsa_familia = $data['bolsa_familia'];
            $aluno->bpc = $data['bpc'];
            $aluno->bolsa_outros = $data['bolsa_outros'];
            $aluno->bolsa_outros_valor = $data['bolsa_outros_valor'];
            $aluno->programa = $data['programa'];
            $aluno->programa_qual = $data['programa_qual'];
            $aluno->casa = $data['casa'];
            $aluno->casa_valor = $data['casa_valor'];
            $aluno->casa_tipo = $data['casa_tipo'];
            $aluno->casa_comodos = $data['casa_comodos'];
            $aluno->casa_situacao = $data['casa_situacao'];
            $aluno->casa_energia = $data['casa_energia'];
            $aluno->casa_agua = $data['casa_agua'];
            $aluno->casa_sanidade = $data['casa_sanidade'];

            $aluno->save();

            $aluno->turmas()->attach(isset($data['turma'])?$data['turma']:[]);
        }

        return Redirect::to('/alunos');
    }


    public function novo()
    {
        $oficinas = Turma::get();
        return View::make('alunos.form')
                    ->with("title", 'Cadastro de novo aluno')
                    ->with('oficinas', $oficinas);

    }


    public function getEdit($id)
    {
        $aluno = Aluno::findOrFail($id);

        $oficinas = Turma::all();

        return View::make('alunos.form')
                    ->with('oficinas', $oficinas)
                    ->with('title', 'Editando '.$aluno->nome)
                    ->with('aluno', $aluno);
    }


    public function getExport($id)
    {
        $aluno     = Aluno::findOrFail($id);
        $planilha  = \Str::slug($aluno->nome);

        \Excel::create($planilha, function($excel) use($aluno) {
            $oficinas = Turma::all();

            $excel->sheet('dados básicos', function($sheet) use($aluno) {
                $sheet->loadView('excel.alunos.dados_basicos')
                      ->with('aluno', $aluno);
            });

            $excel->sheet('escolaridade e saúde', function($sheet) use($aluno) {
                $sheet->loadView('excel.alunos.escolaridade_saude')
                      ->with('aluno', $aluno);
            });

            $excel->sheet('endereço', function($sheet) use($aluno) {
                $sheet->loadView('excel.alunos.endereco')
                      ->with('aluno', $aluno);
            });

            $excel->sheet('habitação', function($sheet) use($aluno) {
                $sheet->loadView('excel.alunos.habitacao')
                      ->with('aluno', $aluno);
            });

            $excel->sheet('família', function($sheet) use($aluno) {
                $sheet->loadView('excel.alunos.familia')
                      ->with('aluno', $aluno);
            });

            $excel->sheet('oficinas', function($sheet) use($aluno) {
                $sheet->loadView('excel.alunos.oficinas')
                      ->with('aluno', $aluno);
            });

            $excel->sheet('outros', function($sheet) use($aluno) {
                $sheet->loadView('excel.alunos.outros')
                      ->with('aluno', $aluno);
            });
        })->export('xls');
    }


    public function delete($id)
    {
        $aluno = Aluno::findOrFail($id);
        $aluno->delete();

        return Redirect::to('/alunos');
    }


    public function report($action='')
    {
        $oficinas = Oficina::lists('nome','id');
        $dados    = Input::all();
        $result    = [];
        $count    = 0;
        $qString  = "";

        if (Request::isMethod('post') || $action == 'export' || $action == 'print') {
            $where  = "AT.cancelado = 0";

            if(!empty($dados['turno'])) {
                $qString .= (empty($qString) ? "?" : "&") . "turno=" . $dados['turno'];
                $where  .= " AND T.turno = '" . $dados['turno'] . "'";
            }

            if(!empty($dados['oficina'])) {
                $qString .= (empty($qString) ? "?" : "&") . "oficina=" . $dados['oficina'];
                $where .= ' AND O.id = ' . $dados['oficina'];
            }

            if(!empty($dados['sexo'])){
                $qString .= (empty($qString) ? "?" : "&") . "sexo=" . $dados['sexo'];

                if($dados['sexo'] == 'Feminino') {
                    $where .= " AND (A.sexo = '" . $dados['sexo'] . "' OR A.sexo = '')";
                }
                else {
                    $where .= " AND A.sexo = '" . $dados['sexo'] . "'";
                }
            }

            if(!empty($dados['idade_i'])){
                $now = Carbon\Carbon::now();
                $idade = $now->subYears($dados['idade_i']);

                $qString .= (empty($qString) ? "?" : "&") . "idade_i=" . $dados['idade_i'];
                $where .= " AND A.nascimento <= '" . substr($idade, 0, 10) . "'";
            }
            if(!empty($dados['idade_s'])){
                $now = Carbon\Carbon::now();
                $idade = $now->subYears($dados['idade_s']);

                $qString .= (empty($qString) ? "?" : "&") . "idade_s=" . $dados['idade_s'];
                $where .= " AND A.nascimento >= '" . substr($idade, 0, 10) . "'";
            }

            $result = DB::select("SELECT O.id AS oficina_id,
                                                           O.nome AS oficina,
                                                           AT.turma_id AS turma_id,
                                                           T.nome AS turma,
                                                           T.turno AS turno,
                                                           C.id AS colaborador_id,
                                                           C.nome AS colaborador,
                                                           AT.aluno_id AS aluno_id,
                                                           A.nome AS aluno,
                                                           A.sexo AS sexo,
                                                           TIMESTAMPDIFF(YEAR, A.nascimento, NOW()) AS idade
                                             FROM `aluno_turma` AT
                                             INNER JOIN `alunos` A ON (A.id = AT.aluno_id)
                                             INNER JOIN `turmas` T ON (T.id = AT.turma_id)
                                             INNER JOIN `colaboradores` C ON (C.id = T.responsavel_id)
                                             INNER JOIN `oficinas` O ON (O.id = T.oficina_id)
                                             WHERE $where
                                             GROUP BY aluno_id, turma_id
                                             ORDER BY O.id, T.id
                                            ");

            $count = count($result);
        }

        if($action=='export') {
            \Excel::create('alunos', function($excel) use ($result, $count) {
                $excel->sheet('alunos', function($sheet) use ($result, $count) {
                    $sheet->loadView('excel.alunos.report')
                               ->with("result", $result)
                               ->with("count", $count);
                });
            })->export('xls');
        }
        else {
            if($action=='print') {
                $limitTime =ini_get('max_execution_time');
                ini_set('max_execution_time', 600);

                $this->pdfReport($result);

                ini_set('max_execution_time', $limitTime);

//                // Utilização do domPDF
//                $params               = array();
//                $params['result']  = $result;
//                $params['count']  = $count;
//
//                $pdf = App::make('dompdf');
//                $pdf->loadView('pdf.alunos.report', $params);
//                return $pdf->stream("alunos.pdf");
            }
            else {
                return View::make('alunos.report')
                    ->with("qString", $qString)
                    ->with("title", 'Relatório de alunos')
                    ->with("result", $result)
                    ->with("count", $count)
                    ->with('oficinas', $oficinas);
            }
        }
    }


    public function pdfReport($result) {
        PDF::AddPage();
        PDF::setPageOrientation('R');
        PDF::SetFont('times', '', 12);
        PDF::SetFillColor(255, 255, 255);

        $count       = count($result);
        $tbl            = '';
        $oficina_id = '';
        $turma_id  = '';
        $countLin  = 1;

        $tbl .= '<h3 style="text-align: center;">Relatório de Alunos</h3>';

        $tbl .= '<div>';
        $tbl .= 'Total de registros: ' . $count;
        $tbl .= '</div>';

        $tbl .= '<table>';

        foreach($result as $a) {
            if($a->oficina_id != $oficina_id) {
                $oficina_id = $a->oficina_id;

                if($oficina_id != '') {
                    $tbl .= '<tr>';
                    $tbl .= '    <td colspan="4">&nbsp;</td>';
                    $tbl .= '</tr>';
                }

                $tbl .= '<tr>';
                $tbl .= '   <td colspan="4"> Oficina: <b>' . $a->oficina . '</b></td>';
                $tbl .= '</tr>';
            }

            if($a->turma_id != $turma_id) {
                $countLin  = 1;
                $turma_id = $a->turma_id;

                if($turma_id != '') {
                    $tbl .= '<tr>';
                    $tbl .= '    <td colspan="4">&nbsp;</td>';
                    $tbl .= '</tr>';
                }

                $tbl .= '<tr>';
                $tbl .= '    <td colspan="2"> Turma: <b>' . $a->turma . ' - ' . $a->turno . '</b></td>';
                $tbl .= '    <td colspan="2"> Colaborador: <b>' . $a->colaborador .'</b></td>';
                $tbl .= '</tr>';
                $tbl .= '<tr>';
                $tbl .= '    <td colspan="4">&nbsp;</td>';
                $tbl .= '</tr>';
                $tbl .= '<tr style="text-align: left;">';
                $tbl .= '    <td width="10%">Código</td>';
                $tbl .= '    <td width="60%">Nome</td>';
                $tbl .= '    <td width="20%">Sexo</td>';
                $tbl .= '    <td width="10%">Idade</td>';
                $tbl .= '</tr>';
            }

            if($countLin%2 == 0) {
                $tbl .= '<tr>';
            }
            else {
                $tbl .= '<tr bgcolor="#DADADA">';
            }

            $tbl .= '   <td width="10%">' . $a->aluno_id . '</td>';
            $tbl .= '   <td width="60%">' . $a->aluno . '</td>';
            $tbl .= '   <td width="20%">' . (!empty($a->sexo) ? $a->sexo : 'Feminino') . '</td>';
            $tbl .= '   <td width="10%">' .  $a->idade . '</td>';
            $tbl .= '</tr>';
            $countLin++;
        }

        $tbl .= '</table>';

        PDF::writeHTML($tbl, true, false, false, false, '');
        PDF::Output('alunos.pdf');
    }


    public function newReport($action='')
    {
        $dados = DB::select("SELECT DISTINCT A.id, A.sexo FROM `aluno_turma` AT
                                                           LEFT JOIN `alunos` A ON (A.id = AT.aluno_id)
                                                           LEFT JOIN `turmas` T ON (T.id = AT.turma_id)
                                                           LEFT JOIN `oficinas` O ON (O.id = T.oficina_id)
                                                           WHERE AT.cancelado = 0 AND
                                                                        T.oficina_id IN (
                                                                            SELECT id
                                                                            FROM `oficinas`
                                                                            WHERE nome LIKE '2016%')"
                                                         );
        $numeroInscritos = count($dados);
        $masculino           = 0;
        $feminino             = 0;
        $outros                 = 0;

        foreach ($dados as $key => $value) {
            if(strtolower($value->sexo) == 'masculino') {
                $masculino++;
            }
            elseif(strtolower($value->sexo) == 'outros'){
                $outros++;
            }
            else {
                $feminino++;
            }
        }

        $dados = DB::select("SELECT A.id, A.nome AS aluno, A.sexo,  AT.aluno_id, AT.turma_id, O.nome AS oficina
                                           FROM `aluno_turma` AT
                                           LEFT JOIN `alunos` A ON (A.id = AT.aluno_id)
                                           LEFT JOIN `turmas` T ON (T.id = AT.turma_id)
                                           LEFT JOIN `oficinas` O ON (O.id = T.oficina_id)
                                           WHERE AT.cancelado = 0 AND
                                                        T.oficina_id IN (SELECT id FROM `oficinas`
                                                              WHERE nome LIKE '2016%')
                                           GROUP BY aluno_id, turma_id
                                           ORDER BY O.nome
                                        ");

        if($action == 'export') {
            \Excel::create('matriculados', function($excel) use ($dados, $masculino, $outros, $feminino, $numeroInscritos) {
                $excel->sheet('matriculados', function($sheet) use ($dados, $masculino, $outros, $feminino, $numeroInscritos) {
                    $sheet->loadView('excel.alunos.matriculados2016')
                            ->with("dados", $dados)
                            ->with("title", 'Relatório de alunos')
                            ->with("masculino", $masculino)
                            ->with("outros", $outros)
                            ->with("feminino", $feminino)
                            ->with('numeroInscritos', $numeroInscritos);
                });
            })->export('xls');
        }
        else {
            return View::make('alunos.report2')
                            ->with("dados", $dados)
                            ->with("title", 'Relatório de alunos')
                            ->with("masculino", $masculino)
                            ->with("outros", $outros)
                            ->with("feminino", $feminino)
                            ->with('numeroInscritos', $numeroInscritos);
        }
    }
}

