<?php

class FormasPagamentosController extends \BaseController {

    /**
     * Display a listing of the resource.
     * GET /formas_pagamentos
     *
     * @return Response
     */
    public function index() {

        $formas = FormasPagamentos::all();

        return View::make('formas_pagamento.index', compact('formas'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function form($action = 'add', $id = null) {
        if ($action == 'add') {
            $form = AutoForm::model(new FormasPagamentos)->make();
            return View::make('formas_pagamento.form', compact('form', 'action'));
        } else {
            if ($id != null) {
                $form = AutoForm::model(new FormasPagamentos, $id)->make();
                return View::make('formas_pagamento.form', compact('form', 'action'));
            } else {
                return View::make('formas_pagamento');
            }
        }
    }

    /**
     * Update the specified resource in storage.
     * PUT /colaboradores/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function save($action = 'add', $id = 0) {

        $rules     = ['forma' => 'required|min:3'];
        $type      = 'info';
        $message   = '';
        $validator = Validator::make($data = Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        }
        
        unset($data['_token']);
        
        try {
            AutoForm::model(new FormasPagamentos,$id)->save($data);
            $message = 'Registro salvo com sucesso!';
            $type = 'success';
        } catch (Exception $ex) {
            $message = 'Erro ao salvar registro! '.$ex->getMessage();
            $type = 'danger';
        }        

        Session::flash('message', $message);
        Session::flash('type', $type);
        return Redirect::route('formas_pagamento');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id = null) {

        $message = 'Operação não realizada, tente novamente!';
        $type = 'info';

        if ($id != null) {
            $forma = FormasPagamentos::findOrFail(Crypt::decrypt($id));

            try {
                $forma->delete();
                $message = 'Registro excluído com sucesso!';
                $type = 'success';
            } catch (Exception $e) {
                $message = 'Erro ao excluir registro!';
                $type = 'danger';
            }
        }
        
        Session::flash('message', $message);
        Session::flash('type', $type);

        $formas = FormasPagamentos::all();
        return View::make('formas_pagamento.index', compact('formas'));
    }

}
