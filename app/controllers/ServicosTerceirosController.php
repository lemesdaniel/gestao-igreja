<?php

class ServicosTerceirosController extends \BaseController {

    /**
     * Display a listing of the resource.
     * GET /servicos_terceiros
     *
     * @return Response
     */
    public function index() {

        $servicos = ServicosTerceiros::all();

        return View::make('servicos_terceiros.index', compact('servicos'));
    }
    
    /**
     * Show the form for creating a new resource.
     * GET /servicos_terceiros/create
     *
     * @return Response
     */
    public function form($action = 'add', $id = null) {
        if ($action == 'add') {
            $form = AutoForm::model(new ServicosTerceiros)->make();
            return View::make('servicos_terceiros.form', compact('form', 'action'));
        } else {
            if ($id != null) {
                $form = AutoForm::model(new ServicosTerceiros, $id)->make();
                return View::make('servicos_terceiros.form', compact('form', 'action'));
            } else {
                return View::make('servicos_terceiros');
            }
        }
    }

    /**
     * Update the specified resource in storage.
     * PUT /servicos_terceiros/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function save($action = 'add', $id = 0) {

        $rules = ['nome_empresa' => 'required|min:5',
                  'email' => 'email',
                  'formas_pagamentos_id' => 'required'];

        $type = 'info';
        $message = '';

        $validator = Validator::make($data = Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        }
        
        // if (Strings::FormatDatetoBD($data['data_servico']) < date('Y-m-d',strtotime("+30 days"))) {
        //     $message = 'O serviço deve ser solicitado no mínimo com 30 dias de antecedencia!';
        //     $type = 'danger';
        //     Session::flash('message', $message);
        //     Session::flash('type', $type);
        //     return Redirect::back()->withInput(Input::all());
        // }
        
        unset($data['_token']);
        
        try {
            AutoForm::model(new ServicosTerceiros,$id)->save($data);
            $message = 'Registro salvo com sucesso!';
            $type = 'success';
        } catch (Exception $ex) {
            $message = 'Erro ao salvar registro! '.$ex->getMessage();
            $type = 'danger';
        }        

        Session::flash('message', $message);
        Session::flash('type', $type);
        return Redirect::route('servicos_terceiros');
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /servicos_terceiros/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id = null) {

        $message = 'Operação não realizada, tente novamente!';
        $type = 'info';

        if ($id != null) {
            $servico = ServicosTerceiros::findOrFail(Crypt::decrypt($id));

            try {
                $servico->delete();
                $message = 'Registro excluído com sucesso!';
                $type = 'success';
            } catch (Exception $e) {
                $message = 'Erro ao excluir registro!';
                $type = 'danger';
            }
        }
        
        Session::flash('message', $message);
        Session::flash('type', $type);

        $servicos = ServicosTerceiros::all();
        return View::make('servicos_terceiros.index', compact('servicos'));
    }

}
