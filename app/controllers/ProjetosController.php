<?php

class ProjetosController extends \BaseController
{

    /**
	 * Display a listing of the resource.
	 * GET /colaboradores
	 *
	 * @return Response
	 */
    public function getIndex()
    {

        $filter = DataFilter::source(Projeto::with('responsavel', 'oficinas'));
        $filter->attributes(array('class' => 'form-inline'));
        $filter->add('projeto', 'Projeto?', 'text');
        $filter->submit('Buscar');
        $filter->reset('Limpar filtro');

        $grid = DataGrid::source($filter);
        $grid->attributes(array("class" => "table table-striped"));
        $grid->add('id', 'ID', true)->style("width:100px");
        $grid->add('projeto', 'Projeto');
        $grid->add('{{ implode(", ", $oficinas->lists("nome")) }}', 'Oficinas envolvidas');
        $grid->edit('/pedagogico/projetos/form', 'Ações', 'show|modify|delete');
        $grid->link('/pedagogico/projetos/form', "Novo", "TR");
        $grid->orderBy('projeto');
        $grid->paginate(20);

        $title = 'Projetos';

        return View::make('padrao.index', compact('filter', 'grid', 'title'));
    }

    /**
	 * Show the form for creating a new resource.
	 * GET /oficinas/create
	 *
	 * @return Response
	 */
    public function anyForm()
    {

        $edit = DataEdit::source(new Projeto());
        $edit->link("/pedagogico/projetos", "Listagem", "TR")->back();
        $action = Input::all();
        // if(isset($action['show']))
        //     $edit->link("oficinas/alunos/".Input::get('show'),"Visualizar matriculados", "TR");

        $edit->add('projeto', 'Projeto', 'text');
        $edit->add('periodo', 'Periodo de Execução', 'text')->rule('required');
        $edit->add('oficinas', 'Oficinas Envolvidas: ', 'checkboxgroup')->options(Turma::orderBy('nome')->lists('nome', 'id'));
        $edit->add('arquivo', 'Arquivo', 'file')->move('photos/');
        $edit->add('objetivos', 'Objetivos', 'textarea');
        $edit->add('atividades', 'Atividades', 'textarea');
        $edit->add('metodologia', 'Metodologia', 'textarea');
        $edit->add('responsavel_id', 'Colaborador', 'select')->options(Colaborador::lists("nome", "id"))->rule('required');
        $edit->add('conclusao', 'Conclusão', 'textarea');
        $title = 'Projetos';

        return $edit->view('padrao.form', compact('edit', 'title'));

    }

}

