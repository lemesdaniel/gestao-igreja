<?php

class EtniasController extends \BaseController {

    /**
     * Display a listing of the resource.
     * GET /colaboradores
     *
     * @return Response
     */
    public function getIndex() {

        // $funcoes = Funcoes::all();

        // return View::make('funcoes.index', compact('funcoes'));

        $filter = DataFilter::source(new Etnia);
        $filter->attributes(array('class'=>'form-inline'));
        $filter->add('nome','Etnia', 'text');
        $filter->submit('Buscar');
        $filter->reset('Limpar filtro');


        $grid = DataGrid::source($filter);
        $grid->attributes(array("class"=>"table table-striped"));
        $grid->add('id','ID', true)->style("width:100px");
        $grid->add('{{ Str::words($nome,30) }}','Etnia');
        
        $grid->edit('/etnias/form', 'Ações','show|modify|delete');

        $grid->link('/etnias/form',"Nova Etnia", "TR");
        $grid->orderBy('id','desc');
        $grid->paginate(10);
        $title = 'Etnias';

        return  View::make('padrao.index', compact('filter', 'grid', 'title'));


    }
    
    /**
     * Show the form for creating a new resource.
     * GET /colaboradores/create
     *
     * @return Response
     */
    public function anyForm($action = 'add', $id = null) {
        $edit = DataEdit::source(new Etnia());
        $edit->link("etnias","Etnia", "TR")->back();
        $edit->add('nome','Etnia', 'text')->rule('required|min:4');
        $title = 'Etnias';

        
        return $edit->view('padrao.form', compact('edit', 'title'));

    }


}
