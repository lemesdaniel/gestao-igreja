<?php

class OrientacoesController extends \BaseController
{

    /**
	 * Display a listing of the resource.
	 * GET /colaboradores
	 *
	 * @return Response
	 */
    public function getIndex()
    {

        $filter = DataFilter::source(Orientacao::with('responsavel'));
        $filter->attributes(array('class' => 'form-inline'));
        $filter->add('quem', 'Quem?', 'text');
        $filter->submit('Buscar');
        $filter->reset('Limpar filtro');

        $grid = DataGrid::source($filter);
        $grid->attributes(array("class" => "table table-striped"));
        $grid->add('id', 'ID', true)->style("width:100px");
        $grid->add('quem', 'Quem?');
        $grid->add('data', 'Data');
        $grid->edit('/pedagogico/orientacoes/form', 'Ações', 'show|modify|delete');
        $grid->link('/pedagogico/orientacoes/form', "Novo atendimento", "TR");
        $grid->orderBy('id', 'desc');
        $grid->paginate(10);

        $title = 'Orientações';

        return View::make('padrao.index', compact('filter', 'grid', 'title'));
    }

    /**
	 * Show the form for creating a new resource.
	 * GET /oficinas/create
	 *
	 * @return Response
	 */
    public function anyForm()
    {

        $edit = DataEdit::source(new Orientacao());
        $edit->link("/pedagogico/orientacoes", "Listagem", "TR")->back();
        $action = Input::all();
        // if(isset($action['show']))
        //     $edit->link("oficinas/alunos/".Input::get('show'),"Visualizar matriculados", "TR");

        $edit->add('quem', 'Quem', 'text');
        $edit->add('responsavel_id', 'Colaborador', 'select')->options(Colaborador::lists("nome", "id"))->rule('required');
        $edit->add('data', 'Data', 'date')->format('d/m/Y', 'pt-BR')->rule('required');
        $edit->add('hora', 'Hora', 'text');
        $edit->add('metodologia', 'Avaliação da Atividade e metodologia', 'textarea')->rule('required');

        $title = 'Orientações';

        return $edit->view('padrao.form', compact('edit', 'title'));

    }

}

