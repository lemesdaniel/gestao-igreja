<?php

class RelatorioDiarioController extends \BaseController
{

    /**
	 * Display a listing of the resource.
	 * GET /colaboradores
	 *
	 * @return Response
	 */
    public function getIndex()
    {

        $filter = DataFilter::source(RelatorioDiario::with('responsavel', 'oficina'));
        $filter->attributes(array('class' => 'form-inline'));
        $filter->add('oficina.nome', 'oficina_id', 'select')->options(Turma::lists("nome", "id"));
        $filter->add('responsavel.nome', 'responsavel_id', 'select')->options(Colaborador::lists("nome", "id"));
        $filter->submit('Buscar');
        $filter->reset('Limpar filtro');

        $grid = DataGrid::source($filter);
        $grid->attributes(array("class" => "table table-striped"));
        $grid->add('id', 'ID', true)->style("width:100px");
        $grid->add('{{ Str::words($oficina->nome,30) }}', 'Turma');
        $grid->add('{{ Str::words($responsavel->nome,30) }}', 'Responsável');
        $grid->edit('/pedagogico/relatorio-diario/form', 'Ações', 'show|modify|delete');
        $grid->link('/pedagogico/relatorio-diario/form', "Novo", "TR");
        $grid->orderBy('id', 'desc');
        $grid->paginate(10);

        $title = 'Relatório Diário';

        return View::make('padrao.index', compact('filter', 'grid', 'title'));
    }

    /**
	 * Show the form for creating a new resource.
	 * GET /oficinas/create
	 *
	 * @return Response
	 */
    public function anyForm()
    {

        $edit = DataEdit::source(new RelatorioDiario());
        $edit->link("pedagogico/relatorio-diario", "Listagem", "TR")->back();
        $action = Input::all();
        // if(isset($action['show']))
        //     $edit->link("oficinas/alunos/".Input::get('show'),"Visualizar matriculados", "TR");

        $edit->add('oficina_id', 'Turma', 'select')->options(Turma::orderBy('nome')->lists("nome", "id"))->rule('required');
        $edit->add('responsavel_id', 'Colaborador', 'select')->options(Colaborador::orderBy('nome')->lists("nome", "id"))->rule('required');
        $edit->add('data', 'Data', 'date')->format('d/m/Y', 'pt-BR')->rule('required');
        $edit->add('turno', 'Turno', 'text');
        $edit->add('atividades', 'Atividades desenvolvidas', 'textarea');
        $edit->add('n_participantes', 'Número de participantes', 'text');
        $edit->add('avaliacao', 'Avaliação', 'text');
        for ($i = 1; $i <= 5; $i++) {
            $edit->add('foto'.$i, 'Foto', 'image')->move('photos/')->preview(240, 160);
        }

        $title = 'Relatório diário';

        return $edit->view('padrao.form', compact('edit', 'title'));

    }

}

