<?php

class EstadoCivilController extends \BaseController {

    /**
     * Display a listing of the resource.
     * GET /colaboradores
     *
     * @return Response
     */
    public function getIndex() {

        // $funcoes = Funcoes::all();

        // return View::make('funcoes.index', compact('funcoes'));

        $filter = DataFilter::source(new EstadoCivil);
        $filter->attributes(array('class'=>'form-inline'));
        $filter->add('nome','Nome', 'text');
        $filter->submit('Buscar');
        $filter->reset('Limpar filtro');


        $grid = DataGrid::source($filter);
        $grid->attributes(array("class"=>"table table-striped"));
        $grid->add('id','ID', true)->style("width:100px");
        $grid->add('{{ Str::words($nome,30) }}','Estado Civil');
        
        $grid->edit('/estado-civil/form', 'Ações','show|modify|delete');

        $grid->link('/estado-civil/form',"Novo estado civil", "TR");
        $grid->orderBy('id','desc');
        $grid->paginate(10);
        $title = 'Estado Civil';

        return  View::make('padrao.index', compact('filter', 'grid', 'title'));


    }
    
    /**
     * Show the form for creating a new resource.
     * GET /colaboradores/create
     *
     * @return Response
     */
    public function anyForm($action = 'add', $id = null) {
        $edit = DataEdit::source(new EstadoCivil());
        
        $edit->link("estado-civil","EstadoCivil", "TR")->back();

        $edit->add('nome','Estado Civil', 'text')->rule('required|min:4');
        $title = 'Estado Civil';        
        return $edit->view('padrao.form', compact('edit', 'title'));

    }


}
