<?php

class ProdutosController extends \BaseController
{

    /**
	 * Display a listing of the resource.
	 * GET /colaboradores
	 *
	 * @return Response
	 */
    public function getIndex()
    {

        $filter = DataFilter::source(new Produto);
        $filter->attributes(array('class' => 'form-inline'));
        $filter->add('nome', 'fonte', 'text');
        $filter->submit('Buscar');
        $filter->reset('Limpar filtro');

        $grid = DataGrid::source($filter);
        $grid->attributes(array("class" => "table table-striped"));
        $grid->add('id', 'ID', true)->style("width:100px");
        $grid->add('nome', 'Nome');
        $grid->add('{{ ($tipo=="C"?"Bem de consumo":"Bem durável") }}', 'Tipo');
        $grid->edit('/estoque/produtos/form', 'Ações', 'show|modify|delete');
        $grid->link('/estoque/produtos/form', "Novo", "TR");
        $grid->orderBy('id', 'desc');
        $grid->paginate(10);

        $title = 'Produtos';

        return View::make('padrao.index', compact('filter', 'grid', 'title'));
    }

    /**
	 * Show the form for creating a new resource.
	 * GET /oficinas/create
	 *
	 * @return Response
	 */
    public function anyForm()
    {

        $edit = DataEdit::source(new Produto());
        $edit->link("/estoque/produtos", "Listagem", "TR")->back();
        $edit->add('nome', 'Nome', 'text');
        $edit->add('tipo', 'Tipo de produto', 'select')->options(['C'=>'Bem de consumo', 'D'=>'Bem durável']);
        $title = 'Produtos';

        return $edit->view('padrao.form', compact('edit', 'title'));

    }

}

