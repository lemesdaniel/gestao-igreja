<?php

class RecursosController extends \BaseController
{

    /**
	 * Display a listing of the resource.
	 * GET /colaboradores
	 *
	 * @return Response
	 */
    public function getIndex()
    {

        $filter = DataFilter::source(new Recurso);
        $filter->attributes(array('class' => 'form-inline'));
        $filter->add('nome', 'fonte', 'text');
        $filter->submit('Buscar');
        $filter->reset('Limpar filtro');

        $grid = DataGrid::source($filter);
        $grid->attributes(array("class" => "table table-striped"));
        $grid->add('id', 'ID', true)->style("width:100px");
        $grid->add('fonte', 'Nome');
        $grid->edit('/financeiro/recurso/form', 'Ações', 'show|modify|delete');
        $grid->link('/financeiro/recurso/form', "Novo", "TR");
        $grid->orderBy('id', 'desc');
        $grid->paginate(10);

        $title = 'Recursos';

        return View::make('padrao.index', compact('filter', 'grid', 'title'));
    }

    /**
	 * Show the form for creating a new resource.
	 * GET /oficinas/create
	 *
	 * @return Response
	 */
    public function anyForm()
    {

        $edit = DataEdit::source(new Recurso());
        $edit->link("financeiro/recurso", "Listagem", "TR")->back();
        $action = Input::all();
        // if(isset($action['show']))
        //     $edit->link("oficinas/alunos/".Input::get('show'),"Visualizar matriculados", "TR");

        $edit->add('fonte', 'Nome', 'text');
        $title = 'Fonte de recursos.';

        return $edit->view('padrao.form', compact('edit', 'title'));

    }

}

