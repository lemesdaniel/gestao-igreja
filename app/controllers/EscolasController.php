<?php

class EscolasController extends \BaseController {

    /**
     * Display a listing of the resource.
     * GET /colaboradores
     *
     * @return Response
     */
    public function getIndex() {

        // $funcoes = Funcoes::all();

        // return View::make('funcoes.index', compact('funcoes'));

        $filter = DataFilter::source(new Escola);
        $filter->attributes(array('class'=>'form-inline'));
        $filter->add('nome','Nome', 'text');
        $filter->submit('Buscar');
        $filter->reset('Limpar filtro');


        $grid = DataGrid::source($filter);
        $grid->attributes(array("class"=>"table table-striped"));
        $grid->add('id','ID', true)->style("width:100px");
        $grid->add('{{ Str::words($nome,30) }}','Nome');
        $grid->add('tipo','Tipo');
        
        $grid->edit('/escolas/form', 'Ações','show|modify|delete');

        $grid->link('/escolas/form',"Nova Escola", "TR");
        $grid->orderBy('nome');
        $grid->paginate(20);
        $title = 'Escolas';

        return  View::make('padrao.index', compact('filter', 'grid', 'title'));


    }
    
    /**
     * Show the form for creating a new resource.
     * GET /colaboradores/create
     *
     * @return Response
     */
    public function anyForm($action = 'add', $id = null) {
        $edit = DataEdit::source(new Escola());

        $edit->link("escolas","Escola", "TR")->back();
        $edit->add('nome','Nome', 'text')->rule('required|min:4');
        $edit->add('tipo','Tipo','select')->options(['Estadual'=>'Estadual',
                                                     'Municipal'=>'Municipal',
                                                     'Federal'=>'Federal',
                                                     'Particular'=>'Particular']);
        $title = 'Escolas';

        
        return $edit->view('padrao.form', compact('edit', 'title'));

    }


}
