<?php

class ApiController  extends \BaseController{

    public function  getSaldo($conta)
    {
        $saldo =  Caixa::where('conta_id', $conta)->sum('valor');
        return Response::json(['saldo' => $saldo]);
    }

    public function postCancelaturma()
    {

        $data = \Input::all();

        $aluno = \Aluno::findOrFail($data['id']);

        $aluno->turmas()->updateExistingPivot($data['oficina'], ['motivo'=>$data['motivo'],
                                                         'cancelado' => true
                                                        ]);

        return "Cancelamento efetuado com sucesso!!";   
    }

    public function getCaixa()
    {
        return Datatable::collection(Caixa::with('oficina')->with('recurso')->with('conta')->get())
            ->addColumn('Tipo',function($model)
                {
                    return $model->tipo=="E"?'Entrada':'Saída';
                }
            )
            ->addColumn('conta',function($model)
                {
                    if(isset($model->conta->nome))
                        return $model->conta->nome;
                    else
                        return '';
                }
            )
            ->addColumn('oficina',function($model)
                {
                   if(isset($model->oficina->nome))
                        return $model->oficina->nome;
                    else
                        return '';
                }
            )
            ->addColumn('valor',function($model)
                {
                    return $model->valor;
                }
            )
            ->addColumn('usuario',function($model)
                {
                    return $model->usuario->username;
                }
            )
            ->addColumn('Ações',function($model)
            {
                ///alunos/delete/".$model->id."
                $links = " <a href='#'><i class='fa fa-trash-o'></i> ";
                return $links;
            })
            ->orderColumns('created_at')
            ->make();        
    }

    public function getOficina()
    {

        return Datatable::collection(Aluno::with('turmas')->with('escola')->get())
            ->showColumns('id', 'nome')
            ->addColumn('escola',function($model)
            {
                if(isset($model->escola->nome))
                    return $model->escola->nome;
                else
                    return 'Nenhuma escola';
            })
            ->addColumn('oficina',function($model)
            {
                $matriculados = [];
                foreach ($model->turmas as $turma) {
                    if(!$turma->pivot->cancelado)
                        $matriculados[] = $turma->nomeCompleto;
                }
                
                return implode(", ", $matriculados);
            }
            )
            ->addColumn('Ações',function($model)
            {

                $links  = " <a href='/alunos/edit/".$model->id."'><i class='fa fa-pencil'></i> ";
                $links .= " <a href='/alunos/autorizacao/".$model->id."' target='_blank'><i class='fa fa-print'></i> ";
                $links .= " <a href='/alunos/copy/".$model->id."'><i class='fa fa-clipboard'></i> ";
                $links .= " <a href='/alunos/export/".$model->id."'><i class='fa fa-file-excel-o'></i> ";
                $links .= " <a href='/alunos/delete/".$model->id."'><i class='fa fa-trash-o'></i> ";

                return $links;
            }
            )
            ->searchColumns('nome')
            ->orderColumns('nome')
            ->make();

    }

}