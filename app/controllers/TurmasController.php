<?php

class TurmasController extends \BaseController
{

    /**
	 * Display a listing of the resource.
	 * GET /colaboradores
	 *
	 * @return Response
	 */
    public function getIndex()
    {

        $filter = DataFilter::source(Turma::with('colaboradores', 'alunos', 'oficinas'));
        $filter->attributes(array('class' => 'form-inline'));
        $filter->add('nome', 'Turma', 'text');
        $filter->submit('Buscar');
        $filter->reset('Limpar filtro');

        $grid = DataGrid::source($filter);
        $grid->attributes(array("class" => "table table-striped"));
        $grid->add('id', 'ID', true)->style("width:100px");
        $grid->add('{{ isset($oficinas->nome)?$oficinas->nome:""  }}', 'Oficina');
        $grid->add('{{ Str::words($nome,30) }}', 'Turma', true);
        $grid->add('local', 'Local', true);
        $grid->add('turno', 'Turno', true);
        $grid->add('{{ $colaboradores->nome }}', 'Colaborador');
        $grid->add('{{ $alunos->count() }}', 'Matriculados');
        $grid->edit('turmas/form', 'Edit', 'show|modify|delete');
        $grid->link('turmas/form', "Nova Turma", "TR");
        $grid->orderBy('nome');
        $grid->paginate(20);

        $title = 'Oficinas';


        return View::make('padrao.index', compact('filter', 'grid', 'title'));
    }

    public function anyChamada($oficina)
    {
        $oficina = Turma::FindOrFail($oficina);

        PDF::AddPage();
        PDF::setPageOrientation('L');
        PDF::SetFont('times', '', 18);
        PDF::Multicell(0, 2, 'Turma '.$oficina->nome, 0, 'C');

        PDF::SetFont('times', '', 12);
        PDF::Multicell(0, 2, 'Monitores: '.$oficina->colaboradores->nome, 0, 'L');
        PDF::SetFillColor(255, 255, 255);
        PDF::MultiCell(90, 2, 'Horário: '.substr($oficina->hora_inicio, 0, 5)." às ".substr($oficina->hora_fim, 0, 5), 0, 'L', 1, 0);
        PDF::MultiCell(90, 2, 'Turno: '.$oficina->turno, 0, 'L', 0, 1);
        PDF::Multicell(0, 2, 'Dia da semana: '.$oficina->dias, 0, 'L');

        PDF::ln(15);

        $tbl = '
            <table cellspacing="0" cellpadding="1" border="1">
                <tr>
                    <th>Nº</th>
                    <th width="800px">Nome</th>
                    <th width="1500px" colspan="20">Mês</th>
                    <th width="400px">Assinatura</th>
                </tr>';

        $seq = 0;
        foreach ($oficina->alunos as $k => $aluno) {
            $seq++;

            $tbl .= '<tr>';
            $tbl .= '<td>'.$seq.'</td>';
            $tbl .= '<td>'.$aluno->nome.'</td>';

            for ($i = 0; $i < 20; $i++) {
                $tbl .= '<td></td>';
            }
            $tbl .= '<td></td>';
            $tbl .= '</tr>';

        }

        $tbl .= '</table>';

        PDF::writeHTML($tbl, true, false, false, false, '');

        PDF::Output(Str::slug($oficina->nome).'.pdf');
    }

    /**
	 * Show the form for creating a new resource.
	 * GET /oficinas/create
	 *
	 * @return Response
	 */
    public function anyForm()
    {
        $edit = DataEdit::source(new Turma());
        $edit->link("turmas", "Listagem", "TR")->back();
        $action = Input::all();

        $turma = [];
        if (isset($action['show'])) {
            $edit->link("turmas/alunos/".Input::get('show'), "Visualizar matriculados", "TR");
            $edit->link("turmas/chamada/".Input::get('show'), "Imprimir diário de classe", "TR");
            $turma = Turma::find(\Input::get('show'));
        }

        $edit->add('nome', 'Nome', 'text')->rule('required|min:4');
        $edit->add('ano', 'Ano', 'text')->rule('required|min:4');
        $edit->add('responsavel_id', 'Colaborador', 'select')->options(Colaborador::lists("nome", "id"));
        $edit->add('oficina_id', 'Oficina', 'select')->options([''=>'--- Escolha ----']+Oficina::lists("nome", "id"))->rule('required');
        $edit->add('local', 'Local', 'text');
        $edit->add('turno', 'Turno', 'select')->options(
            ['Manhã'   => 'Manhã',
                'Tarde'    => 'Tarde',
                'Noite'    => 'Noite',
                'Integral' => 'Integral'
            ]);
        $edit->add('dias', 'Dias da semana', 'text');
        $edit->add('data', 'Data', 'date')->format('d/m/Y', 'pt-BR');
        $edit->add('hora_inicio', 'Hora de início', 'text');
        $edit->add('hora_fim', 'Hora final', 'text');
        $edit->add('max_participantes', 'Máximo de participantes', 'text')->rule('required|integer');
        $edit->add('idade_minima', 'Idade mínima', 'text')->rule('integer');
        $edit->add('idade_maxima', 'Idade máxima', 'text')->rule('integer');
        $edit->add('obs_autorizacao', 'Observações na ficha de autorização', 'textarea');
        $title = 'Oficinas';



        return $edit->view('oficinas.form', compact('edit', 'title', 'turma'));

    }

    public function getAlunos($id = null)
    {
        $oficina = Turma::with('colaboradores', 'alunos')->findOrFail($id);

        return View::make('oficinas.alunos', compact('oficina'));
    }

}

