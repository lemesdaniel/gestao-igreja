<?php

class AgendaController extends \BaseController {

    /**
     * Display a listing of the resource.
     * GET /unidades
     *
     * @return Response
     */
    public function index() {

        $eventos = Agenda::all();

        return View::make('eventos.index', compact('eventos'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function form($action = 'add', $id = null) {
        if ($action == 'add') {
            $form = AutoForm::model(new Agenda)->make();
            return View::make('eventos.form', compact('form', 'action'));
        } else {
            if ($id != null) {
                $form = AutoForm::model(new Agenda, $id)->make();
                return View::make('eventos.form', compact('form', 'action'));
            } else {
                return View::make('eventos');
            }
        }
    }

    /**
     * Update the specified resource in storage.
     * PUT /colaboradores/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function save($action = 'add', $id = 0) {

        $rules = ['evento' => 'required|min:3', 'responsavel_id' => 'required'];
        $type = 'info';
        $message = '';
        
        $validator = Validator::make($data = Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        }
        
        unset($data['_token']);
        
        try {
            AutoForm::model(new Agenda,$id)->save($data);
            $message = 'Registro salvo com sucesso!';
            $type = 'success';
        } catch (Exception $ex) {
            $message = 'Erro ao salvar registro! '.$ex->getMessage();
            $type = 'danger';
        }        
        
        Session::flash('message', $message);
        Session::flash('type', $type);
        return Redirect::route('eventos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id = null) {

        $message = 'Operação não realizada, tente novamente!';
        $type = 'info';

        if ($id != null) {
            $evento = Agenda::findOrFail(Crypt::decrypt($id));

            try {
                $evento->delete();
                $message = 'Registro excluído com sucesso!';
                $type = 'success';
            } catch (Exception $e) {
                $message = 'Erro ao excluir registro!';
                $type = 'danger';
            }
        }
        
        Session::flash('message', $message);
        Session::flash('type', $type);

        $eventos = Agenda::all();
        return View::make('eventos.index', compact('eventos'));
    }

}
