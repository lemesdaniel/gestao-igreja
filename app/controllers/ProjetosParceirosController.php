<?php

class ProjetosParceirosController extends \BaseController
{

    /**
	 * Display a listing of the resource.
	 * GET /colaboradores
	 *
	 * @return Response
	 */
    public function getIndex()
    {

        $filter = DataFilter::source(new ProjetoParceiro);
        $filter->attributes(array('class' => 'form-inline'));
        $filter->add('nome', 'Projeto parceiro', 'text');
        $filter->submit('Buscar');
        $filter->reset('Limpar filtro');

        $grid = DataGrid::source($filter);
        $grid->attributes(array("class" => "table table-striped"));
        $grid->add('id', 'ID', true)->style("width:100px");
        $grid->add('nome', 'Projeto');
        $grid->edit('/pedagogico/projetos-parceiros/form', 'Ações', 'show|modify|delete');
        $grid->link('/pedagogico/projetos-parceiros/form', "Novo", "TR");
        $grid->orderBy('id', 'desc');
        $grid->paginate(10);

        $title = 'Projetos Parceiros';

        return View::make('padrao.index', compact('filter', 'grid', 'title'));
    }

    /**
	 * Show the form for creating a new resource.
	 * GET /oficinas/create
	 *
	 * @return Response
	 */
    public function anyForm()
    {

        $edit = DataEdit::source(new ProjetoParceiro);
        $edit->link("/pedagogico/projetos-parceiros", "Listagem", "TR")->back();
        $action = Input::all();
        // if(isset($action['show']))
        //     $edit->link("oficinas/alunos/".Input::get('show'),"Visualizar matriculados", "TR");

        $edit->add('nome', 'Nome', 'text')->rule('required');
        $edit->add('inicio', 'Início', 'date')->format('d/m/Y', 'pt-BR')->rule('required');
        $edit->add('duracao', 'Duração', 'text');
        $edit->add('termino', 'Término', 'date');
        $edit->add('relatorio', 'Relatório', 'textarea');
        $edit->add('material', 'Material', 'textarea');
        $edit->add('rh', 'RH', 'textarea');
        $edit->add('valor', 'Valor', 'text');
        for ($i = 1; $i <= 5; $i++) {
            $edit->add('foto'.$i, 'Foto ', 'image')->move('photos/')->preview(120, 80);
        }

        $title = 'Projetos Parceiros';

        return $edit->view('padrao.form', compact('edit', 'title'));

    }

}

