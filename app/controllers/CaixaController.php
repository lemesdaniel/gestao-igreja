<?php

class CaixaController extends \BaseController
{

    /**
	 * Display a listing of the resource.
	 * GET /colaboradores
	 *
	 * @return Response
	 */

    public function getIndex()
    {

        return View::make('caixa.list');

    }

    public function getEntrada()
    {

        return View::make('caixa.entrada');

    }


    public function postEntrada()
    {

        $dados = Input::all();
        $caixa = new Caixa();
        $caixa->recurso_id = $dados['recurso_id'];
        $caixa->obs = $dados['obs'];
        $caixa->valor = $dados['valor'];
        $caixa->conta_id = $dados['conta_id'];
        $caixa->usuario_id = $dados['user_id'];
        $caixa->tipo = $dados['tipo'];
        $caixa->save();

        return Redirect::to('/financeiro/caixa');

    }


    public function getSaida()
    {

        return View::make('caixa.saida');

    }


    public function postSaida()
    {

        $dados = Input::all();

        $caixa = new Caixa();
        $caixa->conta_id = $dados['conta_id'];
        $caixa->obs = $dados['obs'];
        $caixa->valor = $dados['valor'];
        $caixa->valor = -1 * $caixa->valor;
        $caixa->usuario_id = $dados['user_id'];
        $caixa->tipo = $dados['tipo'];
        $caixa->save();

        return Redirect::to('/financeiro/caixa');

    }
    
    /**
	 * Show the form for creating a new resource.
	 * GET /oficinas/create
	 *
	 * @return Response
	 */
    public function anyForm()
    {

        $edit = DataEdit::source(new Centro());
        $edit->link("financeiro/centro", "Listagem", "TR")->back();
        $action = Input::all();
        // if(isset($action['show']))
        //     $edit->link("oficinas/alunos/".Input::get('show'),"Visualizar matriculados", "TR");

        $edit->add('nome', 'Nome', 'text');
        $edit->add('obs', 'Observações', 'text');
        $title = 'Centro de custo';

        return $edit->view('padrao.form', compact('edit', 'title'));

    }

}

