<?php

class ColaboradoresController extends \BaseController {

    /**
     * Display a listing of the resource.
     * GET /colaboradores
     *
     * @return Response
     */
    public function getIndex() {

        $filter = DataFilter::source(Colaborador::with('funcoes'));
        $filter->attributes(array('class'=>'form-inline'));
        $filter->add('nome','Nome', 'text');
        $filter->submit('Buscar');
        $filter->reset('Limpar filtro');


        $grid = DataGrid::source($filter);
        $grid->attributes(array("class"=>"table table-striped"));
        $grid->add('id','ID', true)->style("width:100px");
        $grid->add('{{ Str::words($nome,30) }}','nome');
        $grid->add('{{ $funcoes->funcao }}','Funcao', 'funcao_id');

        $grid->edit('/colaboradores/form', 'Edit','show|modify|delete');
        $grid->link('/colaboradores/form',"Novo Colaborador", "TR");
        $grid->orderBy('nome');
        $grid->paginate(20);



        return  View::make('colaboradores.index', compact('filter', 'grid'));
    }

    /**
     * Show the form for creating a new resource.
     * GET /colaboradores/create
     *
     * @return Response
     */
    public function anyForm()
    {

        if (Request::isMethod('PATCH') || Request::isMethod('POST')) {
            if(!empty(Input::get('password'))) {
                Input::merge(array('password' => Hash::make(Input::get('password'))));
            }
            else {
                if(!empty(Input::get('update'))){
                    $Colaborador = Colaborador::findOrFail(Input::get('update'));
                    Input::merge(array('password' => $Colaborador->password));
                }
            }
        }

        $edit = DataEdit::source(new Colaborador());

        $edit->link("colaboradores", "Listagem", "TR")->back();
        $edit->add('nome','Nome', 'text')->rule('required|min:4');
        $edit->add('data_nascimento','Data de Nascimento','date')->format('d/m/Y', 'pt-BR');
        $edit->add('naturalidade','Naturalidade', 'text')->rule('required|min:4');
        $edit->add('telefone','Telefone', 'text');
        $edit->add('estado','Estado','select')->options(Estado::lists("nome", "sigla"));
        $edit->add('cidade','Cidade', 'text');
        $edit->add('bairro','Bairro', 'text');
        $edit->add('endereco','Endereço', 'text');
        $edit->add('numero','Número', 'text')->attributes(array('size'=>6));
        $edit->add('rg','RG', 'text');
        $edit->add('cpf','CPF', 'text');
        $edit->add('email','E-mail', 'text')->rule('email');
        $edit->add('password','Senha', 'password');
        $edit->add('funcao_id','Funcao','select')->options(Funcao::lists("funcao", "id"));
        $edit->add('admissao','Data de Admissão','date')->format('d/m/Y', 'pt-BR');
        $edit->add('demissao','Data de Demissão','date')->format('d/m/Y', 'pt-BR');

        return $edit->view('colaboradores.form', compact('edit'));
    }
}