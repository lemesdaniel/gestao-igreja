<?php

class CentroController extends \BaseController
{

    /**
	 * Display a listing of the resource.
	 * GET /colaboradores
	 *
	 * @return Response
	 */
    public function getIndex()
    {

        $filter = DataFilter::source(new Centro);
        $filter->attributes(array('class' => 'form-inline'));
        $filter->add('nome', 'fonte', 'text');
        $filter->submit('Buscar');
        $filter->reset('Limpar filtro');

        $grid = DataGrid::source($filter);
        $grid->attributes(array("class" => "table table-striped"));
        $grid->add('id', 'ID', true)->style("width:100px");
        $grid->add('nome', 'Nome');
        $grid->edit('/financeiro/centro/form', 'Ações', 'show|modify|delete');
        $grid->link('/financeiro/centro/form', "Novo", "TR");
        $grid->orderBy('id', 'desc');
        $grid->paginate(10);

        $title = 'Centro de custos';

        return View::make('padrao.index', compact('filter', 'grid', 'title'));
    }

    /**
	 * Show the form for creating a new resource.
	 * GET /oficinas/create
	 *
	 * @return Response
	 */
    public function anyForm()
    {

        $edit = DataEdit::source(new Centro());
        $edit->link("financeiro/centro", "Listagem", "TR")->back();
        $action = Input::all();
        // if(isset($action['show']))
        //     $edit->link("oficinas/alunos/".Input::get('show'),"Visualizar matriculados", "TR");

        $edit->add('nome', 'Nome', 'text');
        $edit->add('obs', 'Observações', 'text');
        $title = 'Centro de custo';

        return $edit->view('padrao.form', compact('edit', 'title'));

    }

}

