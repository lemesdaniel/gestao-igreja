<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 10/04/15
 * Time: 12:08
 */

class ProfessorController extends Controller
{
    /**
     * Mostra painel principal.
     *
     * @return void
     */
    public function dashboard()
    {
        return View::make('professor.index');

    }


    public function getLogin()
    {
        return View::make('professor.login.index');

    }

    public function logout()
    {
        Auth::professor()->logout();
        return Redirect::route('professor.getLogin');
    }


    public function login()
    {
        $input = Input::all();
        Auth::professor()->attempt(array(
            'email'     => $input['email'],
            'password'  => $input['password'],
        ));
        if (Auth::professor()->check()) {
            return Redirect::intended('/professor');
        } else {
            $err_msg = 'Usuário ou senha inválidos';

            return Redirect::to('professor/login')
                ->withInput(Input::except('password'))
                ->with('error', $err_msg);
        }

    }


    public function oficinas()
    {

        $filter = DataFilter::source(Turma::with('colaboradores', 'alunos', 'oficinas')->where('responsavel_id', \Auth::professor()->user()->id));
        $filter->attributes(array('class' => 'form-inline'));
        $filter->add('nome', 'Turma', 'text');
        $filter->submit('Buscar');
        $filter->reset('Limpar filtro');

        $grid = DataGrid::source($filter);
        $grid->attributes(array("class" => "table table-striped"));
        $grid->add('id', 'ID', true)->style("width:100px");
        $grid->add('{{ isset($oficinas->nome)?$oficinas->nome:""  }}', 'Oficina');
        $grid->add('{{ Str::words($nome,30) }}', 'Turma', true);
        $grid->add('local', 'Local', true);
        $grid->add('turno', 'Turno', true);
        $grid->add('{{ $alunos->count() }}', 'Alunos');
        $grid->edit('minhas-oficinas/turmas', 'Visualizar', 'show');
        $grid->orderBy('nome');
        $grid->paginate(20);

        $title = 'Oficinas';

        return View::make('professor.oficinas.index', compact('filter', 'grid', 'title'));
    }

    public function chamada($oficina)
    {
        $oficina = Turma::FindOrFail($oficina);

        PDF::SetPrintHeader(false);
        PDF::SetPrintFooter(false);
        PDF::SetMargins(6, 6, 6, true);

        PDF::AddPage();
        PDF::setPageOrientation('L');
        PDF::SetFont('times', '', 18);
        PDF::Multicell(0, 2, 'Oficina:  '.$oficina->nome, 0, 'C');
        PDF::SetFont('times', '', 11);

        PDF::ln(5);

        $colunas = '';

        for ($i = 0; $i < 31; $i++) {
            $colunas .= '<td></td>';
        }

        $tbl = '
            <table cellspacing="0" cellpadding="0" border="0">
                <tr style="text-align:center;">
                    <td><b>Monitor(a):</b> ' . $oficina->colaboradores->nome . '</td>
                    <td><b>Dia da semana:</b> ' . $oficina->dias . '</td>
                    <td><b>Turno:</b> ' . $oficina->turno . '</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
            </table>

            <table cellspacing="0" cellpadding="1" border="1">
                <tr>
                    <td width="900px" colspan="2" style="text-align:right;"><b>Mês &nbsp;</b></td>
                    <td width="1800px" colspan="29"></td>
                    <td width="60px" style="text-align:center;"><b>F</b></td>
                    <td width="60px" style="text-align:center;"><b>S</b></td>
                    <td width="400px"></td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:right;"><b>Data  &nbsp;</b></td>' . $colunas . '
                    <td style="text-align:center;"><b>Assinatura</b></td>
                </tr>
                <tr>
                    <td width="100px"><b>&nbsp;Nº</b></td>
                    <td width="800px"><b>&nbsp;Nome</b></td>' . $colunas . '
                    <td></td>
                </tr>';

        $seq = 0;
        $arrAlunos = array();

        foreach ($oficina->alunos as $k => $aluno) {
            if((!$aluno->pivot->cancelado) && (!in_array($aluno->id, $arrAlunos))) {
                $seq++;
                $arrAlunos[] = $aluno->id;
                $nomeAluno = str_replace('(Registro copiado)', '', $aluno->nome);

                $tbl .= '<tr>';
                $tbl .= '<td>&nbsp;'.$seq.'</td>';
                $tbl .= '<td>&nbsp;'.$nomeAluno.'</td>';
                $tbl .= $colunas;
                $tbl .= '<td></td>';
                $tbl .= '</tr>';
            }
        }

        $tbl .= '</table>';

        PDF::writeHTML($tbl, true, false, false, false, '');
        PDF::Output(Str::slug($oficina->nome).'.pdf');
    }


    public function chamadaVerso($id = null)
    {
        $dados =  Input::all();

        if (Request::isMethod('post')) {
            PDF::SetPrintHeader(false);
            PDF::SetPrintFooter(false);
            PDF::SetMargins(6, 6, 6, true);
            PDF::AddPage();
            PDF::setPageOrientation('L');
            PDF::SetFont('times', '', 12);
            PDF::SetFillColor(255, 255, 255);

            $tbl = '';

            for ($i = 0; $i < $dados['numFolhas']; $i++) {

                $tbl .= '
                    <table cellspacing="0" cellpadding="1" border="1">
                        <tr style="text-align:center; font-weight:bold;">
                            <th width="1600px">Observações</th>
                            <th width="1600px">Ocorrências</th>
                        </tr>';

                for ($j = 0; $j < 34; $j++) {
                        $tbl .= '<tr>';
                        $tbl .= '<td></td>';
                        $tbl .= '<td></td>';
                        $tbl .= '</tr>';
                }

                $tbl .= '</table>';
            }

            PDF::writeHTML($tbl, true, false, false, false, '');
            PDF::Output('chamadaVerso.pdf');

        }
        else {
            return View::make('professor.oficinas.chamadaVerso',compact('id'));
        }

    }


    public function show()
    {
        $id        = \Input::get('show');
        $turma = Turma::findOrFail($id);

        $edit = DataEdit::source(new Turma());
        $edit->link("professor/minhas-oficinas", "Listagem", "TR")->back();
        $action = Input::all();

        if (isset($action['show'])) {
            $edit->link("professor/minhas-oficinas/turmas/alunos/".Input::get('show'), "Visualizar todos alunos", "TR");
            $edit->link("professor/minhas-oficinas/turmas/matriculados/".Input::get('show'), "Visualizar matriculados", "TR");
            $edit->link("professor/minhas-oficinas/turmas/chamada/".Input::get('show'), "Imprimir diário de classe", "TR");
            $edit->link("professor/minhas-oficinas/turmas/chamadaVerso/".Input::get('show'), "Verso diário de classe", "TR");
            $edit->link("professor/minhas-oficinas/turmas/aulas/".Input::get('show'), "Lançar nova aula", "TR");
        }

        $edit->add('nome', 'Nome', 'text')->rule('required|min:4');
        $edit->add('responsavel_id', 'Colaborador', 'select')->options(Colaborador::lists("nome", "id"));
        $edit->add('oficina_id', 'Oficina', 'select')->options([''=>'--- Escolha ----']+Oficina::lists("nome", "id"))->rule('required');
        $edit->add('local', 'Local', 'text');
        $edit->add('turno', 'Turno', 'select')->options(
            ['Manhã'   => 'Manhã',
                'Tarde'    => 'Tarde',
                'Noite'    => 'Noite',
                'Integral' => 'Integral'
            ]);

        $edit->add('dias', 'Dias da semana', 'text');
        $edit->add('data', 'Data', 'date')->format('d/m/Y', 'pt-BR');
        $edit->add('hora_inicio', 'Hora de início', 'text');
        $edit->add('hora_fim', 'Hora final', 'text');
        $edit->add('max_participantes', 'Máximo de participantes', 'text')->rule('required|integer');
        $edit->add('idade_minima', 'Idade mínima', 'text')->rule('integer');
        $edit->add('idade_maxima', 'Idade máxima', 'text')->rule('integer');
        $edit->add('obs_autorizacao', 'Observações na ficha de autorização', 'textarea');
        $title = 'Oficinas';

        return $edit->view('professor.oficinas.show', compact('edit', 'title', 'id', 'turma'));
    }

    public function getAlunos($id = null)
    {
        $oficina = Turma::with('colaboradores', 'alunos')->findOrFail($id);

        return View::make('professor.oficinas.alunos', compact('oficina', 'id'));
    }

    public function getMatriculados($id = null)
    {
        $oficina = Turma::with('colaboradores', 'alunos')->findOrFail($id);

        return View::make('professor.oficinas.matriculados', compact('oficina', 'id'));
    }


    public function lancarAula($id = null)
    {
        $oficina = Turma::with('colaboradores', 'alunos')->findOrFail($id);

        return View::make('professor.oficinas.chamada', compact('oficina', 'id'));
    }


    public function showAula($id = null, $chamada = null)
    {

        $oficina = Turma::with('colaboradores', 'alunos')->findOrFail($id);
        $aula     = Aula::findOrFail($chamada);
        $lista     = array();

        foreach($aula->chamada as $a)
        {
            $lista[$a->id] = $a->pivot->presenca;
        }

        return View::make('professor.oficinas.chamada', compact('oficina', 'lista', 'id'));
    }



    public function lancarAulaPost($id = null)
    {
        $data = Input::all();

        $aula = Aula::firstOrNew(['data' => $data['data'],
                                  'descricao' => $data['descricao'],
                                  'colaborador_id' => Auth::professor()->user()->id,
                                  'turma_id' => $id,
                                    ]);
        $aula->save();

        foreach($data['alunos'] as $aluno)
        {
            DB::table('chamadas')->insert(
                ['aluno_id' => $aluno['id'],
                 'aula_id' => $aula->id,
                 'presenca' => isset($aluno['presenca'])
                ]);
            //$aula->chamada()->attach([$aluno['id'], ['presenca' => isset($aluno['presenca'])]]);

        }



        return Redirect::route('professor.index')->with('success', 'Aula cadastrada com sucesso.');
    }

    public function deleteAula($id)
    {


        $aula = Aula::findOrFail($id);
        $aula->chamada()->detach();
        $aula->delete();


        return Redirect::route('professor.index')->with('success', 'Aula deletada com sucesso.');
    }



}
