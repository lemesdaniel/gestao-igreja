<?php

class AdminController extends Controller
{

    /**
	 * Mostra painel principal.
	 *
	 * @return void
	 */
    public function dashboard()
    {


        $homens       = Aluno::where('sexo', 'Masculino')->count();
        $mulheres     = Aluno::where('sexo', 'Feminino')->count();
        $data['sexo'] = [$homens, $mulheres];
        $oficinas     = Turma::all();
        // dd($data);
        return View::make('admin.index')
            ->with('data', $data)
            ->with('oficinas', $oficinas);

    }

}

