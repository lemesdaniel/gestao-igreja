<?php

class ContaController extends \BaseController
{

    public function getIndex()
    {

        $filter = DataFilter::source(new Conta);
        $filter->attributes(array('class' => 'form-inline'));
        $filter->add('nome', 'fonte', 'text');
        $filter->submit('Buscar');
        $filter->reset('Limpar filtro');

        $grid = DataGrid::source($filter);
        $grid->attributes(array("class" => "table table-striped"));
        $grid->add('id', 'ID', true)->style("width:100px");
        $grid->add('nome', 'Nome');
        $grid->edit('/financeiro/conta/form', 'Ações', 'show|modify|delete');
        $grid->link('/financeiro/conta/form', "Novo", "TR");
        $grid->orderBy('id', 'desc');
        $grid->paginate(10);

        $title = 'Contas';

        return View::make('padrao.index', compact('filter', 'grid', 'title'));
    }

    public function anyForm()
    {

        $edit = DataEdit::source(new Conta());
        $edit->link("financeiro/conta", "Listagem", "TR")->back();
        $action = Input::all();
        // if(isset($action['show']))
        //     $edit->link("oficinas/alunos/".Input::get('show'),"Visualizar matriculados", "TR");

        $edit->add('nome', 'Nome', 'text');
        $title = 'Contas';

        return $edit->view('padrao.form', compact('edit', 'title'));

    }

}

