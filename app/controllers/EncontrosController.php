<?php

class EncontrosController extends \BaseController
{

    /**
	 * Display a listing of the resource.
	 * GET /colaboradores
	 *
	 * @return Response
	 */
    public function getIndex()
    {

        $filter = DataFilter::source(Encontro::with('responsavel'));
        $filter->attributes(array('class' => 'form-inline'));
        $filter->add('assunto', 'Assunto', 'text');
        $filter->submit('Buscar');
        $filter->reset('Limpar filtro');

        $grid = DataGrid::source($filter);
        $grid->attributes(array("class" => "table table-striped"));
        $grid->add('id', 'ID', true)->style("width:100px");
        $grid->add('{{ Str::words($assunto,30) }}', 'Assunto');
        $grid->add('{{ Str::words($responsavel->nome,30) }}', 'Responsável');
        $grid->edit('/pedagogico/encontros/form', 'Ações', 'show|modify|delete');
        $grid->link('/pedagogico/encontros/form', "Novo", "TR");
        $grid->orderBy('id', 'desc');
        $grid->paginate(10);

        $title = 'Encontro de Formação e qualificação';

        return View::make('padrao.index', compact('filter', 'grid', 'title'));
    }

    /**
	 * Show the form for creating a new resource.
	 * GET /oficinas/create
	 *
	 * @return Response
	 */
    public function anyForm()
    {

        $edit = DataEdit::source(new Encontro());
        $edit->link("pedagogico/encontros", "Listagem", "TR")->back();
        $action = Input::all();
        // if(isset($action['show']))
        //     $edit->link("oficinas/alunos/".Input::get('show'),"Visualizar matriculados", "TR");

        $edit->add('assunto', 'Assunto', 'text');
        $edit->add('responsavel_id', 'Colaborador', 'select')->options(Colaborador::lists("nome", "id"))->rule('required');
        $edit->add('data', 'Data', 'date')->format('d/m/Y', 'pt-BR')->rule('required');
        $edit->add('participantes', 'Participantes', 'textarea');
        $edit->add('palestrante', 'Palestrante/Convidado', 'text');
        $edit->add('horario', 'Hora', 'text');
        $edit->add('local', 'Local', 'text');

        $title = 'Encontro de Formação e qualificação';

        return $edit->view('padrao.form', compact('edit', 'title'));

    }

}

