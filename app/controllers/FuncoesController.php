<?php

class FuncoesController extends \BaseController {

    /**
     * Display a listing of the resource.
     * GET /colaboradores
     *
     * @return Response
     */
    public function getIndex() {

        // $funcoes = Funcoes::all();

        // return View::make('funcoes.index', compact('funcoes'));

        $filter = DataFilter::source(new Funcao);
        $filter->attributes(array('class'=>'form-inline'));
        $filter->add('funcao','Nome', 'text');
        $filter->submit('Buscar');
        $filter->reset('Limpar filtro');


        $grid = DataGrid::source($filter);
        $grid->attributes(array("class"=>"table table-striped"));
        $grid->add('id','ID', true)->style("width:100px");
        $grid->add('{{ Str::words($funcao,30) }}','Função');
        
        $grid->edit('/funcoes/form', 'Ações','show|modify|delete');

        $grid->link('/funcoes/form',"Nova função", "TR");
        $grid->orderBy('funcao');
        $grid->paginate(20);
        $title = 'Funções';

        return  View::make('padrao.index', compact('filter', 'grid', 'title'));


    }
    
    /**
     * Show the form for creating a new resource.
     * GET /colaboradores/create
     *
     * @return Response
     */
    public function anyForm($action = 'add', $id = null) {
        $edit = DataEdit::source(new Funcao());
        $edit->link("funcoes","Funcao", "TR")->back();
        $edit->add('funcao','Nome', 'text')->rule('required|min:4');
        $title = 'Funções';        
        return $edit->view('padrao.form', compact('edit', 'title'));

    }


}
