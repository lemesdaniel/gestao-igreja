<?php

class UnidadesController extends \BaseController {

    /**
     * Display a listing of the resource.
     * GET /unidades
     *
     * @return Response
     */
    public function index() {

        $unidades = Unidades::all();

        return View::make('unidades.index', compact('unidades'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function form($action = 'add', $id = null) {
        if ($action == 'add') {
            $form = AutoForm::model(new Unidades)->make();
            return View::make('unidades.form', compact('form', 'action'));
        } else {
            if ($id != null) {
                $form = AutoForm::model(new Unidades, $id)->make();
                return View::make('unidades.form', compact('form', 'action'));
            } else {
                return View::make('unidades');
            }
        }
    }

    /**
     * Update the specified resource in storage.
     * PUT /colaboradores/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function save($action = 'add', $id = 0) {

        $rules = ['unidade' => 'required|min:3', 'responsavel_id' => 'required'];

        $type = 'info';
        $data = Input::all();

        unset($data['_token']);
        $message = '';

        $validator = Validator::make($data = Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput(Input::all());
        }
        
        try {
            AutoForm::model(new Unidades,$id)->save($data);
            $message = 'Registro salvo com sucesso!';
            $type = 'success';
        } catch (Exception $ex) {
            $message = 'Erro ao salvar registro! '.$ex->getMessage();
            $type = 'danger';
        }        

        Session::flash('message', $message);
        Session::flash('type', $type);
        return Redirect::route('unidades');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id = null) {

        $message = 'Operação não realizada, tente novamente!';
        $type = 'info';

        if ($id != null) {
            $unidade = Unidades::findOrFail(Crypt::decrypt($id));

            try {
                $unidade->delete();
                $message = 'Registro excluído com sucesso!';
                $type = 'success';
            } catch (Exception $e) {
                $message = 'Erro ao excluir registro!';
                $type = 'danger';
            }
        }
        
        Session::flash('message', $message);
        Session::flash('type', $type);

        $unidades = Unidades::all();
        return View::make('unidades.index', compact('unidades'));
    }

}
