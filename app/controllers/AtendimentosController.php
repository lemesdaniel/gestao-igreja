<?php

class AtendimentosController extends \BaseController
{

    /**
	 * Display a listing of the resource.
	 * GET /colaboradores
	 *
	 * @return Response
	 */
    public function getIndex()
    {

        $filter = DataFilter::source(Atendimento::with('responsavel', 'aluno'));
        $filter->attributes(array('class' => 'form-inline'));
        $filter->add('nome', 'Aluno', 'text');
        $filter->submit('Buscar');
        $filter->reset('Limpar filtro');

        $grid = DataGrid::source($filter);
        $grid->attributes(array("class" => "table table-striped"));
        $grid->add('id', 'ID', true)->style("width:100px");
        $grid->add('{{ Str::words($aluno->nome,30) }}', 'Aluno');
        $grid->add('{{ Str::words($responsavel->nome,30) }}', 'Responsável');
        $grid->edit('/pedagogico/atendimentos/form', 'Ações', 'show|modify|delete');
        $grid->link('/pedagogico/atendimentos/form', "Novo atendimento", "TR");
        $grid->orderBy('id', 'desc');
        $grid->paginate(10);

        $title = 'Atendimentos';

        return View::make('padrao.index', compact('filter', 'grid', 'title'));
    }

    /**
	 * Show the form for creating a new resource.
	 * GET /oficinas/create
	 *
	 * @return Response
	 */
    public function anyForm()
    {

        $edit = DataEdit::source(new Atendimento());
        $edit->link("pedagogico/atendimentos", "Listagem", "TR")->back();
        $action = Input::all();
        // if(isset($action['show']))
        //     $edit->link("oficinas/alunos/".Input::get('show'),"Visualizar matriculados", "TR");

        $edit->add('aluno_id', 'Aluno', 'select')->options(Aluno::lists("nome", "id"))->rule('required');
        $edit->add('responsavel_id', 'Colaborador', 'select')->options(Colaborador::lists("nome", "id"))->rule('required');
        $edit->add('local', 'Local', 'text');
        $edit->add('turno', 'Turno', 'text');
        $edit->add('data', 'Data', 'date')->format('d/m/Y', 'pt-BR')->rule('required');
        $edit->add('horario', 'Hora', 'text');
        $edit->add('incidente', 'Incidente', 'textarea')->rule('required');
        $edit->add('encaminhamentos', 'Encaminhamentos', 'textarea');
        $edit->add('orientacoes', 'Orientações', 'textarea');
        $title = 'Atendimentos';

        return $edit->view('padrao.form', compact('edit', 'title'));

    }

}

