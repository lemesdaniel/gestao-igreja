<?php

class OficinasController extends \BaseController {

    /**
     * Display a listing of the resource.
     * GET /colaboradores
     *
     * @return Response
     */
    public function getIndex() {

        $filter = DataFilter::source(new Oficina);
        $filter->attributes(array('class'=>'form-inline'));
        $filter->add('nome','Nome', 'text');
        $filter->submit('Buscar');
        $filter->reset('Limpar filtro');


        $grid = DataGrid::source($filter);
        $grid->attributes(array("class"=>"table table-striped"));
        $grid->add('id','ID', true)->style("width:100px");
        $grid->add('{{ Str::words($nome,30) }}','nome');

        $grid->edit('/oficinas/cadastro', 'Ações','show|modify|delete');
        $grid->link('/oficinas/cadastro',"Novo", "TR");
        $grid->orderBy('nome');
        $grid->paginate(20);

        $title = 'Oficinas';

        return  View::make('padrao.index', compact('filter', 'grid', 'title'));
    }
    
    /**
     * Show the form for creating a new resource.
     * GET /colaboradores/create
     *
     * @return Response
     */
    public function anyCadastro()
    {

        $edit = DataEdit::source(new Oficina());
        $edit->link("oficinas", "Listagem", "TR")->back();

        $edit->add('nome','Nome', 'text')->rule('required|min:4');
        $edit->add('descricao','Descrição', 'textarea');

        $title = 'Oficinas';

        return $edit->view('padrao.form', compact('edit', 'title'));

    }

}