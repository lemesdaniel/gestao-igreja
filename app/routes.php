<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
 */

Route::get('/', array('as'      => 'login', 'uses'      => 'UsersController@login'));
Route::get('/login', array('as' => 'login', 'uses' => 'UsersController@login'));
Route::get('users/login', 'UsersController@login');
Route::post('users/login', 'UsersController@doLogin');


Route::get('users/confirm/{code}', 'UsersController@confirm');
Route::get('users/forgot_password', 'UsersController@forgotPassword');
Route::post('users/forgot_password', 'UsersController@doForgotPassword');
Route::get('users/reset_password/{token}', 'UsersController@resetPassword');
Route::post('users/reset_password', 'UsersController@doResetPassword');
Route::get('users/logout', ['as' => 'logout', 'uses' => 'UsersController@logout']);

Route::get('professor/login', array('as' => 'professor.getLogin', 'uses' => 'ProfessorController@getLogin'));
Route::post('professor/login', array('as' => 'professor.login', 'uses' => 'ProfessorController@login'));
Route::get('professor/logout', array('as' => 'professor.logout', 'uses' => 'ProfessorController@logout'));


Route::group(array('before' => 'professor', 'prefix' => 'professor'), function ()
{

    Route::get('/', array('as' => 'professor.index', 'uses' => 'ProfessorController@dashboard'));
    Route::get('minhas-oficinas', array('as' => 'professor.oficinas', 'uses' => 'ProfessorController@oficinas'));
    Route::any('minhas-oficinas/turmas', array('as' => 'professor.turmas', 'uses' => 'ProfessorController@show'));
    Route::any('minhas-oficinas/turmas/chamadaVerso/{id}', array('as' => 'professor.turmas', 'uses' => 'ProfessorController@chamadaVerso'));
    Route::get('minhas-oficinas/turmas/chamada/{id}', array('as' => 'professor.turmas', 'uses' => 'ProfessorController@chamada'));
    Route::get('minhas-oficinas/turmas/alunos/{id}', array('as' => 'professor.turmas', 'uses' => 'ProfessorController@getAlunos'));
    Route::get('minhas-oficinas/turmas/matriculados/{id}', array('as' => 'professor.turmas', 'uses' => 'ProfessorController@getMatriculados'));
    Route::get('minhas-oficinas/turmas/aulas/{id}', array('as' => 'professor.aulas', 'uses' => 'ProfessorController@lancarAula'));
    Route::post('minhas-oficinas/turmas/aulas/{id}', array('uses' => 'ProfessorController@lancarAulaPost'));
    Route::get('minhas-oficinas/turmas/delete/aulas/{aula}', array('as' => 'professor.deleteAula', 'uses' => 'ProfessorController@deleteAula'));
    Route::get('minhas-oficinas/turmas/{id}/aulas/{chamada}', array('as' => 'professor.showAula', 'uses' => 'ProfessorController@showAula'));
});

Route::group(array('before' => 'auth'), function ()
{
        //Route::get('api/oficina', array('as'=>'api.oficina', 'uses'=>'ApiController@getOficina'));
        //Route::get('api/caixa', array('as'=>'api.caixa', 'uses'=>'ApiController@getCaixa'));
        Route::controller('api', 'ApiController');
        Route::get('/admin', array('as' => 'admin', 'uses' => 'AdminController@dashboard'));
        Route::controller('colaboradores', 'ColaboradoresController');
        Route::controller('funcoes', 'FuncoesController');
        Route::controller('religiao', 'ReligioesController');
        Route::controller('etnias', 'EtniasController');
        Route::controller('escolas', 'EscolasController');
        Route::controller('estado-civil', 'EstadoCivilController');
        //Route::controller('alunos', 'AlunosController');
        // Alunos controller
        Route::get('alunos', array('as'=>'alunos', 'uses'=>'AlunosController@index'));
        Route::any('alunos/relatorio/{action?}', array('as'=>'alunos', 'uses'=>'AlunosController@report'));

        Route::any('alunos/relatorioAlunos/{action?}', array('as'=>'alunos', 'uses'=>'AlunosController@newReport'));
        Route::get('alunos/novo', array('as'=>'alunos.novo', 'uses'=>'AlunosController@novo'));
        Route::get('alunos/edit/{id}', array('as'=>'alunos.edit', 'uses'=>'AlunosController@getEdit'));
        Route::get('alunos/autorizacao/{id}', array('as'=>'alunos.autorizacao', 'uses'=>'AlunosController@autorizacao'));
        Route::get('alunos/copy/{id}', array('as'=>'alunos.copy', 'uses'=>'AlunosController@copy'));
        Route::get('alunos/delete/{id}', array('as'=>'alunos.delete', 'uses'=>'AlunosController@delete'));
        Route::post('alunos/save', array('as'=>'alunos.save', 'uses'=>'AlunosController@postSave'));

        Route::get('alunos/export/{id}', array('as'=>'alunos.export', 'uses'=>'AlunosController@getExport'));

        Route::controller('turmas', 'TurmasController');
        Route::controller('oficinas', 'OficinasController');
        Route::controller('relatorios', 'RelatoriosController');

        // menu pedagogico
        Route::group(array('prefix' => 'pedagogico'), function ()
        {
                Route::controller('atendimentos', 'AtendimentosController');
                Route::controller('orientacoes', 'OrientacoesController');
                Route::controller('projetos', 'ProjetosController');
                Route::controller('relatorio-diario', 'RelatorioDiarioController');
                Route::controller('encontros', 'EncontrosController');
                Route::controller('projetos-parceiros', 'ProjetosParceirosController');
        });

        // menu financeiro
        Route::group(array('prefix' => 'financeiro'), function ()
        {
            Route::controller('recurso', 'RecursosController');
            Route::controller('centro', 'CentroController');
            Route::controller('caixa', 'CaixaController');
            Route::controller('conta', 'ContaController');

        });

        // menu financeiro
        Route::group(array('prefix' => 'estoque'), function ()
        {
            Route::controller('produtos', 'ProdutosController');


        });

        // Serviços de Terceiros
        Route::get('/servicos_terceiros', array('as'                 => 'servicos_terceiros', 'uses'                 => 'ServicosTerceirosController@index'));
        Route::get('/servicos_terceiros/delete/{id}', array('as'     => 'excluir_servicos_terceiros', 'uses'     => 'ServicosTerceirosController@delete'));
        Route::get('/servicos_terceiros/{action}/{id?}', array('as'  => 'form_servicos_terceiros', 'uses'  => 'ServicosTerceirosController@form'));
        Route::post('/servicos_terceiros/{action}/{id?}', array('as' => 'salvar_servicos_terceiros', 'uses' => 'ServicosTerceirosController@save'));

        // Formas de pagamento
        Route::get('/formas_pagamento', array('as'                 => 'formas_pagamento', 'uses'                 => 'FormasPagamentosController@index'));
        Route::get('/formas_pagamento/delete/{id}', array('as'     => 'excluir_formas_pagamento', 'uses'     => 'FormasPagamentosController@delete'));
        Route::get('/formas_pagamento/{action}/{id?}', array('as'  => 'form_formas_pagamento', 'uses'  => 'FormasPagamentosController@form'));
        Route::post('/formas_pagamento/{action}/{id?}', array('as' => 'salvar_formas_pagamento', 'uses' => 'FormasPagamentosController@save'));

        // Unidades
        Route::get('/unidades', array('as'                 => 'unidades', 'uses'                 => 'UnidadesController@index'));
        Route::get('/unidades/delete/{id}', array('as'     => 'excluir_unidade', 'uses'     => 'UnidadesController@delete'));
        Route::get('/unidades/{action}/{id?}', array('as'  => 'form_unidade', 'uses'  => 'UnidadesController@form'));
        Route::post('/unidades/{action}/{id?}', array('as' => 'salvar_unidade', 'uses' => 'UnidadesController@save'));

        // Agenda (Eventos)
        Route::get('/eventos', array('as'                 => 'eventos', 'uses'                 => 'AgendaController@index'));
        Route::get('/eventos/delete/{id}', array('as'     => 'excluir_evento', 'uses'     => 'AgendaController@delete'));
        Route::get('/eventos/{action}/{id?}', array('as'  => 'form_evento', 'uses'  => 'AgendaController@form'));
        Route::post('/eventos/{action}/{id?}', array('as' => 'salvar_evento', 'uses' => 'AgendaController@save'));

    });

