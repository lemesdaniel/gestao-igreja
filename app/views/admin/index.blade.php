@extends('layout.admin')

@section('title')
  Gestão - Página Inicial
@stop

@section('content')
	<div class="row">
		<div class="col-sm-6">
			<h3>Cadastra por sexo</h3>
			<div id="alunos_por_sexo" style="height: 250px;"></div>
		</div>
		<div class="col-sm-6">
			<h3>Alunos por oficina</h3>
			<div id="alunos_por_oficina" style="height: 250px;"></div>
		</div>
	</div>


@stop

@section('js')
    <!-- Morris Charts JavaScript -->
    {{ HTML::script('assets/js/plugins/morris/raphael.min.js') }}
    {{ HTML::script('assets/js/plugins/morris/morris.min.js') }}
    {{ HTML::script('assets/js/plugins/morris/morris-data.js') }}
	<script>
	Morris.Donut({
	  element: 'alunos_por_sexo',
	  data: [
	    {label: "Masculino", value: {{ $data['sexo'][0] }} },
	    {label: "Feminino", value: {{ $data['sexo'][1] }} },
	  ]
	});

	Morris.Donut({
	  element: 'alunos_por_oficina',
	  data: [
	  	@foreach ($oficinas as $oficina)
	    	{label: "{{ $oficina->nome }}", value: {{ $oficina->alunos->count() }} },

	    @endforeach
	  ]
	});


	</script>
@stop