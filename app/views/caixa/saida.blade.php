@extends('layout.admin')

@section('css')

@stop

@section('js')
    {{ HTML::script('assets/js/vanilla-masker.min.js') }}
    {{ HTML::script('assets/js/caixa.js') }}

@stop

@section('content')
<div class="row">
	<h2>Saída</h2>
</div>
<div class="row" id="caixaSaida">
    {{ Form::open(array('action' => 'CaixaController@postSaida', 'method' => 'post')) }}
    {{ Form::hidden('tipo', 'S')}}
    {{ Form::hidden('user_id', Auth::admin()->user()->id)}}
    {{ BootstrapForm::select('oficina_id', [""=>"Selecione uma opção"] + Oficina::lists('nome','id'),null, array('label' => 'Oficina para onde o recurso será enviado')) }}
    {{ BootstrapForm::select('conta_id', [""=>"Selecione uma opção"] + Conta::lists('nome','id'), null,['label' => 'Conta', 'v-model'=>'conta_id', 'v-on'=>'change: getSaldo']) }}
    {{ BootstrapForm::select('centro_id', [""=>"Selecione uma opção"] + Centro::lists('nome','id'),null, array('label' => 'Centro de custos')) }}
    <div v-show="showSaldo">Saldo da conta selecionada: R$ @{{ saldo }}</div>
    {{ BootstrapForm::text('valor', '', ['label'=>'Digite o valor',  'class'=>"form-control currency", 'v-model' => 'valor', 'v-el' => 'valor' ]) }}
    {{ BootstrapForm::text('obs', '', array('label' => 'Observações:')) }}
    {{ BootstrapForm::submit('Salvar') }}
    {{ Form::close() }}
    
</div>    




@stop