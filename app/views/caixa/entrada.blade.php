@extends('layout.admin')

@section('css')

@stop

@section('js')
    {{ HTML::script('assets/js/vanilla-masker.min.js') }}
    {{ HTML::script('assets/js/caixa.js') }}

@stop

@section('content')
<div class="row">
	<h2>Entrada</h2>
</div>
<div class="row" id="caixaEntrada">
    {{ Form::open(array('action' => 'CaixaController@postEntrada', 'method' => 'post')) }}
    {{ Form::hidden('tipo', 'E')}}
    {{ Form::hidden('user_id', Auth::admin()->user()->id)}}
    {{ BootstrapForm::select('recurso_id', [""=>"Selecione uma opção"]+Recurso::lists('fonte','id'), null,  array('label' => 'Fonte do recurso')) }}
    {{ BootstrapForm::select('conta_id', [""=>"Selecione uma opção"]+Conta::lists('nome','id'), null , ['label' => 'Conta']) }}
    {{ BootstrapForm::text('obs', '', array('label' => 'Observações:')) }}
    {{ BootstrapForm::text('valor', '', ['label'=>'Digite o valor', 'v-model' => 'valor', 'v-el' => 'valor' ]) }}
    {{ BootstrapForm::submit('Submit', array('label' => 'Salvar')) }}
    {{ Form::close() }}
    
</div>    

@stop