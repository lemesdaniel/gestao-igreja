@extends('layout.admin')

@section('css')
    {{ HTML::style('//cdn.datatables.net/1.9.4/css/jquery.dataTables.css'); }}
    {{ HTML::style('//cdn.datatables.net/tabletools/2.2.0/css/dataTables.tableTools.css'); }}
@stop

@section('js')

    {{ HTML::script('//cdn.datatables.net/1.9.4/js/jquery.dataTables.min.js'); }}
    {{ HTML::script('//cdn.datatables.net/tabletools/2.2.0/js/dataTables.tableTools.min.js'); }}

    
    //bootbox.confirm(message, callback)
@stop
@section('content')
    {{ Datatable::table()
    ->addColumn('Tipo','conta', 'Oficina', 'Valor', 'Usuário', 'Ações')       // these are the column headings to be shown
    ->setUrl(url('api/caixa'))
    ->setOptions(
            array(
                'dom' =>"T<'clear'>lfrtip",
                "language" => array("url" => "//cdn.datatables.net/plug-ins/f2c75b7247b/i18n/Portuguese-Brasil.json"),
                'tableTools' => array(
                    "sSwfPath" => "/swf/copy_csv_xls_pdf.swf" ,
                    "aButtons" => array( "pdf", "xls")
                )
            )
        )
    ->render() }}



@stop