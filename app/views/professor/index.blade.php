@extends('layout.colaborador')

@section('title')
    Gestão - Página Inicial
@stop

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success" role="alert">
            <p>{{ Session::get('success') }}</p>
        </div>
    @endif

@stop

@section('js')
    <!-- Morris Charts JavaScript -->
    {{ HTML::script('assets/js/plugins/morris/raphael.min.js') }}
    {{ HTML::script('assets/js/plugins/morris/morris.min.js') }}
    {{ HTML::script('assets/js/plugins/morris/morris-data.js') }}

@stop