@extends('layout.colaborador')

@section('title')
    Oficina: {{ $oficina->nomeCompleto }}
@stop

@section('h1')
    {{ $oficina->nomeCompleto }}
@stop

@section('content')
    {{ Form::open() }}
    <div class="btn-toolbar" role="toolbar">

        <div class="pull-left">
            <h2></h2>
        </div>
        <div class="pull-right">
            <a href="/professor/minhas-oficinas" class="btn btn-default">Listagem</a>
            <a href="/professor/minhas-oficinas/turmas?show={{$id}}" class="btn btn-default">Oficina</a>
            <a href="/professor/minhas-oficinas/turmas/alunos/{{$id}}" class="btn btn-default">Todos alunos</a>
            <a href="/professor/minhas-oficinas/turmas/matriculados/{{$id}}" class="btn btn-default">Matriculados</a>
            <a href="/professor/minhas-oficinas/turmas/chamada/{{$id}}" class="btn btn-default" target="_blank">Imprimir di&aacute;rio de classe</a>
            <a href="/professor/minhas-oficinas/turmas/chamadaVerso/{{$id}}" class="btn btn-default">Verso di&aacute;rio de classe</a>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-xs-4">Responsável: <b>{{ $oficina->colaboradores->nome }}</b></div>
    </div>
    <div class="row">
        <div class="col-xs-4">Horário: <b>{{ $oficina->hora_inicio }} às {{ $oficina->hora_fim }} </b></div>
        <div class="col-xs-4">Turno: <b>{{ $oficina->turno }}</b></div>
    </div>

    <div class="row">
        <div class="col-xs-2">Data:</div>
        <div class="col-xs-8"><b>{{ Form::text('data') }} </b></div>
    </div>
    <div class="row">
        <div class="col-xs-2">Notas da Aula:</div>
        <div class="col-xs-8"><b>{{  Form::textarea('descricao') }}</b></div>
    </div>

    <hr>
    <h3>  Presenças</h3>
    Desmarque os alunos que não estavam presentes na aula.


    <table class="table table-hover">
        <tr>
            <td>
                <b> Nº</b>
            </td>
            <td>
                <b>Presença</b>
            </td>
            <td>
                <b>Nome</b>
            </td>
        </tr>
    @foreach ($oficina->alunos as $index => $aluno)
        <tr>
            <td>
                {{ $index + 1 }}.
            </td>
            <td>
                @if(isset($lista))
                    @if(isset($lista[$aluno->id]) and $lista[$aluno->id]==1) presente @else <span style="color:red"> ausente </span> @endif
                @else
                    @if(!$aluno->pivot->cancelado)
                        {{ Form::checkbox('alunos['.$index.'][presenca]', 1, true) }}
                    @endif
                @endif
            </td>
            <td>
                {{ Form::hidden('alunos['.$index.'][id]', $aluno->id) }}
                {{ $aluno->nome }}
                @if ($aluno->pivot->cancelado)
                    ** Matricula cancelada **
                @endif
            </td>
        </tr>
    @endforeach
    </table>
    @if(!isset($lista))
        <div class="row">
            {{ Form::submit('Salvar Dados') }}
        </div>
    @endif
    {{ Form::close() }}

    <script>
        $(document).ready(function(){
            $('input[name="data"').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR',
                todayBtn: 'linked',
                autoclose: true
            });
        });

    </script>
@stop
