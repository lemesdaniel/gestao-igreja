@extends('layout.colaborador')

@section('title')
    Oficina: {{ $oficina->nome }}
@stop

@section('h1')
    {{ $oficina->nome }}
@stop

@section('content')
    <div class="btn-toolbar" role="toolbar">
        <div class="pull-left">
            <h2></h2>
        </div>
        <div class="pull-right">
            <a href="/professor/minhas-oficinas" class="btn btn-default">Listagem</a>
            <a href="/professor/minhas-oficinas/turmas?show={{$id}}" class="btn btn-default">Oficina</a>
            <a href="/professor/minhas-oficinas/turmas/matriculados/{{$id}}" class="btn btn-default">Matriculados</a>
            <a href="/professor/minhas-oficinas/turmas/chamada/{{$id}}" class="btn btn-default" target="_blank">Imprimir di&aacute;rio de classe</a>
            <a href="/professor/minhas-oficinas/turmas/chamadaVerso/{{$id}}" class="btn btn-default">Verso di&aacute;rio de classe</a>
            <a href="/professor/minhas-oficinas/turmas/aulas/{{$id}}" class="btn btn-default">Lan&ccedil;ar nova aula</a>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-xs-4">Responsável: <b>{{ $oficina->colaboradores->nome }}</b></div>
    </div>
    <div class="row">
        <div class="col-xs-4">Horário: <b>{{ $oficina->hora_inicio }} às {{ $oficina->hora_fim }} </b></div>
        <div class="col-xs-4">Turno: <b>{{ $oficina->turno }}</b></div>
    </div>
    <hr>
    <div class="row">
        <div class="col-xs-1">
            <b> Nº</b>
        </div>
        <div class="col-xs-9">
            <b>Nome</b>
        </div>
        <div class="col-xs-2">
            <b>Código</b>
        </div>
    </div>
    @foreach ($oficina->alunos as $index => $aluno)
        <div class="row">
            <div class="col-xs-1">
                {{ $index + 1 }}
            </div>
            <div class="col-xs-9">
                {{ $aluno->nome }}
                @if ($aluno->pivot->cancelado)
                    ** Matricula cancelada **
                @endif
            </div>
            <div class="col-xs-2">
                {{ $aluno->id }}
            </div>
        </div>
    @endforeach

@stop
