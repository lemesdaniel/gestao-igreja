@extends('layout.colaborador')

@section('title')
    Oficina
@stop

@section('h1')
    Oficina
@stop

@section('content')

    <div class="row">
        <div class="form">
            <div class="btn-toolbar" role="toolbar">

                <div class="pull-left">
                    <h2></h2>
                </div>
                <div class="pull-right">
                    <a href="/professor/minhas-oficinas" class="btn btn-default">Listagem</a>
                    <a href="/professor/minhas-oficinas/turmas/alunos/{{$id}}" class="btn btn-default">Todos alunos</a>
                    <a href="/professor/minhas-oficinas/turmas/matriculados/{{$id}}" class="btn btn-default">Matriculados</a>
                    <a href="/professor/minhas-oficinas/turmas/chamada/{{$id}}" class="btn btn-default" target="_blank">Imprimir di&aacute;rio de classe</a>
                    <a href="/professor/minhas-oficinas/turmas/chamadaVerso/{{$id}}" class="btn btn-default">Verso di&aacute;rio de classe</a>
                    <a href="/professor/minhas-oficinas/turmas/aulas/{{$id}}" class="btn btn-default">Lan&ccedil;ar nova aula</a>
                </div>
            </div>
            <br />


        {{ $edit->message }}

        @if(!$edit->message)
            <div class="row">
                <div class="col-sm-4">
                    Nome: {{ $edit->field('nome') }}
                    <p class="bg-danger">{{ $edit->field('nome')->message }}</p>
                </div>
                <div class="col-sm-4">
                    Oficina: {{ $edit->field('oficina_id') }}
                    <p class="bg-danger">{{ $edit->field('oficina_id')->message }}</p>
                </div>
                <div class="col-sm-4">
                    Responsável: {{ $edit->field('responsavel_id') }}
                    <p class="bg-danger">{{ $edit->field('responsavel_id')->message }}</p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    Local: {{ $edit->field('local') }}
                    <p class="bg-danger">{{ $edit->field('local')->message }}</p>
                </div>
                <div class="col-sm-4">
                    Turno: {{ $edit->field('turno') }}
                    <p class="bg-danger">{{ $edit->field('turno')->message }}</p>
                </div>
                <div class="col-sm-4">
                    Dias: {{ $edit->field('dias') }}
                    <p class="bg-danger">{{ $edit->field('dias')->message }}</p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    Data: {{ $edit->field('data') }}
                    <p class="bg-danger">{{ $edit->field('data')->message }}</p>
                </div>
                <div class="col-sm-4">
                    Hora de início: {{ $edit->field('hora_inicio') }}
                    <p class="bg-danger">{{ $edit->field('hora_inicio')->message }}</p>
                </div>
                <div class="col-sm-4">
                    Hora final: {{ $edit->field('hora_fim') }}
                    <p class="bg-danger">{{ $edit->field('hora_fim')->message }}</p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    Máximo de participantes:  {{ $edit->field('max_participantes') }}
                    <p class="bg-danger">{{ $edit->field('max_participantes')->message }}</p>
                </div>

                <div class="col-sm-4">
                    Idade Mínima:  {{ $edit->field('idade_minima') }}
                    <p class="bg-danger">{{ $edit->field('idade_minima')->message }}</p>
                </div>

                <div class="col-sm-4">
                    Idade Máxima:  {{ $edit->field('idade_maxima') }}
                    <p class="bg-danger">{{ $edit->field('idade_maxima')->message }}</p>
                </div>
            </div>


        @endif


        {{ $edit->footer }}
    </div>

    <div class="row">
       <h3>Aulas lançadas</h3>
        <table class="table table-hover">
            <tr>
                <td>
                    <b>Data</b>
                </td>
                <td>
                    <b>Descrições</b>
                </td>
                <td>
                    {{--<b>Ações</b>--}}
                </td>
            </tr>
       @foreach($turma->aulas as $aula)
            <tr>
                <td>
                    {{ $aula->data }}
                </td>
                <td>
                     {{ $aula->descricao }}
                </td>
                <td>
                    <a href="/professor/minhas-oficinas/turmas/{{$aula->turma_id}}/aulas/{{$aula->id}}">
                        <span class="glyphicon glyphicon-eye-open"> </span>
                    </a>
                    <a href="/professor/minhas-oficinas/turmas/delete/aulas/{{$aula->id}}">
                        <span class="glyphicon glyphicon-trash"> </span>
                    </a>
                </td>
            </tr>
        @endforeach

        </table>
    </div>


@stop






