@extends('layout.colaborador')

@section('title')
    Projari - Verso Diário de classe
@stop

@section('h1')
    Impressão diário de classe - Verso da folha
@stop

@section('content')
    {{ Form::open(array('target' => '_blank')) }}
        <div class="btn-toolbar" role="toolbar">
            <div class="pull-left">
                <h2></h2>
            </div>
            <div class="pull-right">
                <a href="/professor/minhas-oficinas" class="btn btn-default">Listagem</a>
                <a href="/professor/minhas-oficinas/turmas?show={{$id}}" class="btn btn-default">Oficina</a>
                <a href="/professor/minhas-oficinas/turmas/alunos/{{$id}}" class="btn btn-default">Todos alunos</a>
                <a href="/professor/minhas-oficinas/turmas/matriculados/{{$id}}" class="btn btn-default">Matriculados</a>
                <a href="/professor/minhas-oficinas/turmas/chamada/{{$id}}" class="btn btn-default" target="_blank">Imprimir di&aacute;rio de classe</a>
                <a href="/professor/minhas-oficinas/turmas/aulas/{{$id}}" class="btn btn-default">Lan&ccedil;ar nova aula</a>
            </div>
        </div>

        <br>

        <div class="form-group col-md-6">
            <label for="oficina">Número de folhas:</label>
            <div class="input-group">
                {{ Form::number('numFolhas', null, ['class'=>'form-control']) }}
                <span class="input-group-btn">
                    {{ Form::submit('Imprimir') }}
                </span>
            </div>
        </div>
    {{ Form::close() }}

@stop
