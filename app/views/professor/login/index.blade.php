@extends('layout.login')

@section('title')
    Gestão - Login professor
@stop

@section('content')

    <div class="panel-heading">
        <h3 class="panel-title">Identifique-se - Portal do Professor</h3>
    </div>
    <div class="panel-body">

        <form role="form" method="POST" action="{{ route('professor.login') }}" accept-charset="UTF-8">
            <fieldset>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input class="form-control" tabindex="1" placeholder="Email" type="text" name="email" id="email" value="">
                </div>
                <div class="form-group">
                    <label for="password">
                        Senha
                    </label>
                    <input class="form-control" tabindex="2" placeholder="Senha" type="password" name="password" id="password">
                    <p class="help-block">
                        <a href="/users/forgot_password">(esqueci minha senha)</a>
                    </p>
                </div>
                <div class="checkbox">
                    <label for="remember">
                        <input tabindex="4" type="checkbox" name="remember" id="remember" value="1"> Continuar conectado
                    </label>
                </div>

                <div class="form-group">
                    <button tabindex="3" type="submit" class="btn btn-default">Entrar</button>
                </div>
            </fieldset>
        </form>
    </div>
@stop
