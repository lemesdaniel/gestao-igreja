@extends('layout.admin')

@section('title') 
    @if ($action == 'add')
    Gestão Projari - Nova Unidade
    @elseif ($action == 'edit')
    Gestão Projari - Editando Unidade
    @else ($action == 'delete')
    Gestão Projari - Excluindo Unidade
    @endif
@stop

@section('h1') 
    Unidades
@stop

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-content">
                <h4 class="page-header">
                    @if ($action == 'add')
                    Adicionando nova Unidade
                    @elseif ($action == 'edit')
                    Editando Unidade
                    @else ($action == 'delete')
                    Excluindo Unidade
                    @endif
                </h4>
                
                @if ( ! $errors->isEmpty() )
                @foreach ( $errors->all() as $error )
                <div class="alert alert-danger alert-dismissable">
                    <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                    {{ $error }}
                </div>
                @endforeach
                @endif
                
                {{ $form }}
            </div>
        </div>
    </div>
</div>

@stop
