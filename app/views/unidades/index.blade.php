@extends('layout.admin')

@section('title') 
Gestão - Unidades
@stop

@section('h1') 
Unidades
@stop

@section('content')

<div class="row">
    <div class="col-lg-12">
        @if (Session::has('message'))
        <div class="alert alert-{{ Session::get('type') }} alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            {{ Session::get('message') }}
        </div>
        @endif

        <div class="panel panel-default">
            <div class="panel-heading">
                Listagem de Unidades
            </div>

            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="{{ URL::to('/unidades/add') }}">
                                <span class="glyphicon glyphicon-plus"></span> Adicionar Unidade
                            </a>
                        </li>
                    </ul>                
                </div>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Unidade</th>
                                <th>Local</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($unidades as $unidade)
                                <tr data-href="{{ URL::to('/unidades/edit/'. Crypt::encrypt($unidade->id) )}}">
                                    <td>{{ $unidade->unidade }}</td>
                                    <td>{{ $unidade->local }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
                <div class="row">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="{{ URL::to('/unidades/add') }}">
                                <span class="glyphicon glyphicon-plus"></span> Adicionar Unidade
                            </a>
                        </li>
                    </ul>                
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
@stop
