@extends('layout.admin')

@section('title') 
  Colaboradores
@stop

@section('h1') 
    Colaboradores
@stop

@section('content')

    <div class="row">
        {{ $edit->header }}

        {{ $edit->message }}

        @if(!$edit->message)
            <div class="row">
                <div class="col-sm-4">
                    Nome: {{ $edit->field('nome') }}
                    <p class="bg-danger">{{ $edit->field('nome')->message }}</p>
                </div>
                <div class="col-sm-4">
                    Data de nascimento: {{ $edit->field('data_nascimento') }}
                    <p class="bg-danger">{{ $edit->field('data_nascimento')->message }}</p>
                </div>
                <div class="col-sm-4">
                    Naturalidade: {{ $edit->field('naturalidade') }}
                    <p class="bg-danger">{{ $edit->field('naturalidade')->message }}</p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    Telefone: {{ $edit->field('telefone') }}
                    <p class="bg-danger">{{ $edit->field('telefone')->message }}</p>
                </div>
                <div class="col-sm-4">
                    Estado: {{ $edit->field('estado') }}
                    <p class="bg-danger">{{ $edit->field('estado')->message }}</p>
                </div>
                <div class="col-sm-4">
                    Cidade: {{ $edit->field('cidade') }}
                    <p class="bg-danger">{{ $edit->field('cidade')->message }}</p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    Endereço: {{ $edit->field('endereco') }}
                    <p class="bg-danger">{{ $edit->field('endereco')->message }}</p>
                </div>
                <div class="col-sm-4">
                    Número: {{ $edit->field('numero') }}
                    <p class="bg-danger">{{ $edit->field('numero')->message }}</p>
                </div>
                <div class="col-sm-4">
                    Bairro: {{ $edit->field('bairro') }}
                    <p class="bg-danger">{{ $edit->field('bairro')->message }}</p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    RG:  {{ $edit->field('rg') }}
                    <p class="bg-danger">{{ $edit->field('rg')->message }}</p>
                </div>

                <div class="col-sm-4">
                    CPF:  {{ $edit->field('cpf') }}
                    <p class="bg-danger">{{ $edit->field('cpf')->message }}</p>
                </div>

                <div class="col-sm-4">
                    Função:  {{ $edit->field('funcao_id') }}
                    <p class="bg-danger">{{ $edit->field('funcao_id')->message }}</p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    Email:
                    {{ $edit->field('email') }}
                    <p class="bg-danger">{{ $edit->field('email')->message }}</p>
                </div>
                <div class="col-sm-6">
                    Senha:
                    {{ $edit->field('password') }}
                    <p class="bg-danger">{{ $edit->field('password')->message }}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    Data de admissão:
                    {{ $edit->field('admissao') }}
                    <p class="bg-danger">{{ $edit->field('admissao')->message }}</p>
                </div>
                <div class="col-sm-6">
                    Data de demissão:
                    {{ $edit->field('demissao') }}
                    <p class="bg-danger">{{ $edit->field('demissao')->message }}</p>
                </div>
            </div>
        @endif


        {{ $edit->footer }}
    </div>

@stop








