@extends('layout.admin')

@section('title') 
	Oficina: {{ $oficina->nome }}
@stop

@section('h1') 
	{{ $oficina->nome }}
@stop

@section('content')
	<div class="row">
		<div class="col-xs-1">Responsável:</div>
		<div class="col-xs-3"><b>{{ $oficina->colaboradores->nome }}</b></div>
	</div>
	<div class="row">
		<div class="col-xs-1">Horário:</div>
		<div class="col-xs-3"><b>{{ $oficina->hora_inicio }} às {{ $oficina->hora_fim }} </b></div>

		<div class="col-xs-1">Turno:</div>
		<div class="col-xs-3"><b>{{ $oficina->turno }}</b></div>
	</div>
	<hr>
	<div class="row">
		<div class="col-xs-1">
			<b> Nº</b>
		</div>
		<div class="col-xs-6">
		 	<b>Nome</b>
		</div>
	</div>
	@foreach ($oficina->alunos as $index => $aluno)
		<div class="row">
			<div class="col-xs-1">
				{{ $index + 1 }}
			</div>
			<div class="col-xs-6">
				{{ $aluno->nome }}
			</div>
		</div>
	@endforeach

@stop
