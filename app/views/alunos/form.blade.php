@extends('layout.admin')

@section('title')
    {{ $title }}
@stop

@section('h1')
    {{ $title }}
@stop

@section('js')

<script>


$(document).on("click", "a[data-id]", function(e) {
    e.preventDefault();
    var id = $(this).data("id");
    var curso = $(this).data("curso");
    var oficina = $(this).data("oficina");


    bootbox.prompt("Digite o motivo do cancelamento", function(result) {                
      if (result === null) {                                             
        bootbox.alert("Ação cancelada");
      } else {
        var data = { id: id, oficina: oficina, motivo: result };
        $.ajax({
            type: "POST",
            url: '/api/cancelaturma',
            data: data,
            success: function(data){
                bootbox.alert(data);  
            }
        });
      }
    });

});


$(document).on("click", "a[data-motivo]", function(e) {
    e.preventDefault();
    var motivo = $(this).data("motivo");
    bootbox.alert(motivo);    
});
    

</script>

@stop


@section('content')

<div class="row">
    <div class="well">
    <ul class="nav nav-tabs">
       <li class="active"><a href="#home" data-toggle="tab">Dados Básicos</a></li>
      <li><a href="#escolares" data-toggle="tab">Escolaridade/Saúde</a></li>
      <li><a href="#endereco" data-toggle="tab">Endereço/Documentos</a></li>
      <li><a href="#habitacao" data-toggle="tab">Habitação</a></li>
      <li><a href="#familia" data-toggle="tab">Família</a></li>
      <li><a href="#oficinas" data-toggle="tab">Oficinas</a></li>
      @if(isset($aluno))
      <li><a href="#matricula" data-toggle="tab">Oficinas matriculadas</a></li>
      @endif
      <li><a href="#outros" data-toggle="tab">Outros</a></li>
    </ul>
    @if(!isset($aluno))
        {{ Form::open(array('method' => 'post', 'files' => true, 'route' => 'alunos.save')) }}
    @else
        {{ Form::model($aluno, array('route' => 'alunos.save')) }}
        {{ Form::hidden('id') }}

    @endif
    <div id="myTabContent" class="tab-content">
      <div class="tab-pane active in" id="home">
            <div class="row">

                <div class="col-sm-8">
                    Nome: {{ Form::text('nome') }}
                    <p class="bg-danger"></p>

                    Data de Nascimento: {{ Form::text('nascimento', null, ['id'=>'nascimento']) }}
                    <p class="bg-danger"></p>

                    Sexo:
                    {{ Form::select('sexo', array('' => '-- selecione --', 'Masculino' => 'Masculino', 'Feminimo' => 'Feminino', 'Outros' => 'Outros')) }}
                    <p class="bg-danger"></p>

                    Estado Civil:  {{ Form::select('estado_civil_id', EstadoCivil::lists('nome','id')) }}

                    <p class="bg-danger"></p>
                    Etnia:
                    {{ Form::select('etnia_id', Etnia::lists('nome','id')) }}
                    <p class="bg-danger"></p>

                </div>

                <div class="col-sm-4">
                    Foto: {{ Form::file('foto') }}
                    <p class="bg-danger"></p>
                </div>
            </div> 

      </div>

        <div class="tab-pane fade" id="escolares">
            <div class="row">
                <div class="col-sm-4">
                    Escola: {{ Form::select('escola_id', Escola::lists('nome','id')) }}

                    <p class="bg-danger"></p>
                </div>

                <div class="col-sm-4">
                    Série: {{ Form::text('serie') }}
                    <p class="bg-danger"></p>
                </div>

                <div class="col-sm-4">
                    Turno: {{ Form::select('turno', ['' => '----', 'Manhã'   => 'Manhã',
                'Tarde'    => 'Tarde',
                'Noite'    => 'Noite',
                'Integral' => 'Integral'
            ]) }}
                    <p class="bg-danger"></p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    Possui carteira escolar?
                    {{ Form::select('carteira_escolar', ['' => '----', 'Sim' => 'Sim', 'Não' => 'Não']) }}

                    <p class="bg-danger"></p>
                </div>

                <div class="col-sm-4">
                    Como vai se deslocar:
                    {{ Form::select('deslocamento', ['' => '----', 'A pé' => 'A pé',
                                    'Onibus'             => 'Onibus',
                                    'Bicicleta'          => 'Bicicleta',
                                    'Transporte Escolar' => 'Transporte Escolar',
                                    'Outros'             => 'Outros'
                                ]) }}

                    <p class="bg-danger"></p>
                </div>
                <div class="col-sm-4">
                    Foi atendido por outra instituição ou Projeto Social?
                    {{ Form::select('ja_atendido', ['' => '----', 'Sim' => 'Sim', 'Não' => 'Não']) }}

                    <p class="bg-danger"></p>
                </div>

            </div>

            <div class="row">
                <div class="col-sm-4">
                    A carteira de vacinação esta em dia?
                    {{ Form::select('carteira_vacinacao', ['' => '----', 'Sim' => 'Sim', 'Não' => 'Não']) }}
                    <p class="bg-danger"></p>
                </div>

                <div class="col-sm-4">
                    Toma alguma medicação?
                    {{ Form::select('medicacao', ['' => '----', 'Sim' => 'Sim', 'Não' => 'Não']) }}
                    <p class="bg-danger"></p>
                </div>
                <div class="col-sm-4">
                    Qual medicação?
                    {{ Form::text('medicacao_nome') }}
                    <p class="bg-danger"></p>
                </div>

            </div>

            <div class="row">
                <div class="col-sm-12">
                    Atestado médico para atividades físicas (danças, esportes, artes marciais...)?
                    {{ Form::select('atestado_medico', ['' => '----', 'Sim' => 'Sim', 'Não' => 'Não']) }}
                    <p class="bg-danger"></p>
                </div>

            </div>

            <div class="row">
                <div class="col-sm-6">
                    Possui alergia?
                    {{ Form::select('alergia', ['' => '----', 'Sim' => 'Sim', 'Não' => 'Não']) }}
                    <p class="bg-danger"></p>
                </div>
                <div class="col-sm-6">
                    Qual alergia?
                    {{ Form::text('alergia_qual') }}
                    <p class="bg-danger"></p>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-12">
                    <b>Em caso de emergência chamar</b>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    Nome: {{ Form::text('emergencia_nome')  }}
                    <p class="bg-danger"></p>
                </div>
                <div class="col-sm-3">
                    Contato: {{ Form::text('emergencia_endereco') }}
                    <p class="bg-danger"></p>
                </div>
                <div class="col-sm-3">
                    Contato: {{ Form::text('emergencia_fone') }}
                    <p class="bg-danger"></p>
                </div>
                <div class="col-sm-3">
                    Parentesco: {{ Form::text('emergencia_parentesco') }}
                    <p class="bg-danger"></p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    Encaminhamento: {{ Form::text('encaminhamento') }}
                    <p class="bg-danger"></p>
                </div>

                <div class="col-sm-6">
                    Outro encaminhamento não listado: {{ Form::text('encaminhamento_outros') }}
                    <p class="bg-danger"></p>
                </div>

            </div>



            <div class="row">
                <div class="col-sm-12">
                    Alguma doença na família?

                    {{ Form::select('doenca_familia', ['' => '----', 'Sim' => 'Sim', 'Não' => 'Não'], null , ['id'=>'doenca_familia']) }}
                    <p class="bg-danger"></p>
                </div>
            </div>
            <div class="row" id="div_doenca_familia">
                <div class="col-sm-3">
                    Qual doença? {{ Form::text('doenca_qual') }}
                    <p class="bg-danger"></p>
                </div>
                <div class="col-sm-3">
                    Qual familiar? {{ Form::text('doenca_qual_familiar') }}
                    <p class="bg-danger"></p>
                </div>
                <div class="col-sm-3">
                    Em tratamento? {{ Form::text('doenca_tratamento') }}
                    <p class="bg-danger"></p>
                </div>
                <div class="col-sm-3">
                    Médico responsável? {{ Form::text('doenca_medico') }}
                    <p class="bg-danger"></p>
                </div>
            </div>



            <div class="row">
                <div class="col-sm-3">
                    Gestante na família?
                    {{ Form::select('gestante_familia', ['' => '----', 'Sim' => 'Sim', 'Não' => 'Não']) }}

                    <p class="bg-danger"></p>
                </div>
                <div class="col-sm-3">
                    Quem é a gestante?
                    {{ Form::text('gestante_quem') }}
                    <p class="bg-danger"></p>
                </div>
                <div class="col-sm-3">
                    Tempo de gestação?
                    {{ Form::text('gestante_mes') }}

                    <p class="bg-danger"></p>
                </div>
                <div class="col-sm-3">
                    Faz pré natal?
                    {{ Form::select('gestante_pre_natal', ['' => '----', 'Sim' => 'Sim', 'Não' => 'Não']) }}
                    <p class="bg-danger"></p>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-3">
                    Algum familiar com necessidade especial?
                    {{ Form::select('necessidade_especial', ['' => '----', 'Sim' => 'Sim', 'Não' => 'Não'], null, ['id'=>'necessidade_especial']) }}
                    <p class="bg-danger"></p>
                </div>
                <div id="div_necessidade_especial">
                    <div class="col-sm-3">
                        Qual?
                        {{ Form::select('necessidade_qual', ['Mental'        => 'Mental', 'Física'        => 'Física', 'Auditiva'        => 'Auditiva', 'Visual'        => 'Visual', 'Multipla'        => 'Multipla']) }}
                        <p class="bg-danger"></p>
                    </div>

                    <div class="col-sm-3">
                        Recebe acompanhamento médico?
                        {{ Form::select('necessidade_acompanhamento', ['' => '----', 'Sim' => 'Sim', 'Não' => 'Não']) }}

                        <p class="bg-danger"></p>
                    </div>
                    <div class="col-sm-3">
                        Recebe BPC?
                        {{ Form::select('necessidade_bpc', ['' => '----', 'Sim' => 'Sim', 'Não' => 'Não']) }}
                        <p class="bg-danger"></p>
                    </div>
                </div>

            </div>

        </div>


        <!-- nova tab -->
        <div class="tab-pane fade" id="endereco">
            <div class="row">
                <div class="col-sm-8">
                    Endereço: {{ Form::text('endereco') }}
                    <p class="bg-danger"></p>
                </div>
                <div class="col-sm-2">
                    Número: {{ Form::text('numero') }}
                    <p class="bg-danger"></p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    Bairro: {{ Form::text('bairro') }}
                    <p class="bg-danger"></p>
                </div>
                <div class="col-sm-4">
                    Telefone: {{ Form::text('fone') }}
                    <p class="bg-danger"></p>
                </div>

            </div>

            <div class="row">
                <div class="col-sm-4">
                    NIS:  {{ Form::text('nis') }}
                    <p class="bg-danger"></p>
                </div>

                <div class="col-sm-4">
                    RG: {{ Form::text('rg') }}
                    <p class="bg-danger"></p>

                </div>

                <div class="col-sm-4">

                </div>
            </div>

        </div>

      <div class="tab-pane fade" id="oficinas">
            <div class="row">
                <div class="col-sm-12">
                    
                    <table class="table">
                    <tr>
                        <th>#</th>
                        <th>Oficina</th>
                        <th>Vagas</th>
                    </tr>
                    @foreach($oficinas as $oficina)
                        
                        <tr>
                        <td>{{ Form::checkbox('turma[]', $oficina->id, false) }} </td>
                        <td>{{ $oficina->nomeCompleto }} </td>
                        <td class="text-right"><span class="vagas">{{ ($oficina->max_participantes - $oficina->alunos->count()) }} </span></td>    
                                
                        </tr>            
                        
                    @endforeach
                    </table>
                    <p class="bg-danger"></p>
                </div>

            </div>
      </div>
      @if(isset($aluno))
          <div class="tab-pane fade" id="matricula">
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table">
                        <tr>
                            <th>Oficina</th>
                            <th>Ação</th>
                        </tr>
                        @foreach($aluno->turmas as $oficina)
                        
                            
                            <tr>
                             <td>
                                {{ $oficina->nomeCompleto }}
                                @if($oficina->pivot->cancelado)
                                    <span class="text-danger"> ** CANCELADA **</span>
                                @endif
                             </td>
                             <td>
                             
                                 @if($oficina->pivot->cancelado)
                                    <a href="#" data-motivo="{{ $oficina->pivot->motivo }}" >Ver motivo</a> 
                                 @else
                                    <a href="#" data-curso="{{ $oficina->nome }}" 
                                    data-id="{{ $aluno->id }}" data-oficina="{{ $oficina->id }}" 
                                    >Cancelar Matrícula</a> 
                                @endif
                            
                            </td>

                            </tr>    
                            

                        @endforeach
                        
                        </table>
                    </div>

                </div>
          </div>
      @endif

      
      <div class="tab-pane fade" id="outros">
                      <div class="row">
                <div class="col-sm-6">
                    Há medidas de proteção à criança e/ou adolescente?
                    {{ Form::select('medida_protecao', ['' => '----', 'Sim' => 'Sim', 'Não' => 'Não']) }}
                    <p class="bg-danger"></p>
                </div>
                <div class="col-sm-6">
                    Conselheiro tutelar responsável
                    {{ Form::text('medida_conselheiro') }}
                    <p class="bg-danger"></p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                     Como conheceu o PROJARI?
                    {{ Form::select('conheceu', ['Amigos'     => 'Amigos',
                        'Escola'     => 'Escola',
                        'Igreja'     => 'Igreja',
                        'Jornais'    => 'Jornais',
                        'Familiares' => 'Familiares'
                        , 'Outros'   => 'Outros'
                    ]) }}
                    <p class="bg-danger"></p>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-12">
                     Você ou alguém da família já fez parte do?
                    {{ Form::select('ja_fez_parte',
                            [''            => 'Nenhum',
                            'PROJARI'      => 'PROJARI',
                            'Mutirão'      => 'Mutirão',
                            'Projeto Vida' => 'Projeto Vida',
                        ]) }}
                    <p class="bg-danger"></p>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-12">
                    Observações

                    {{ Form::textarea('obs') }}
                    <p class="bg-danger"></p>
                </div>

            </div>

      </div>




      <div class="tab-pane fade" id="familia">
            <div class="row">
                <div class="col-sm-12">
                    <b>Composição familiar</b>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-1">
                    Nº
                </div>
                <div class="col-sm-3">
                    Nome
                </div>
                <div class="col-sm-1">
                    Parentesco
                </div>
                <div class="col-sm-1">
                    Documento
                </div>
                <div class="col-sm-1">
                    Idade
                </div>
                <div class="col-sm-2">
                    Sexo
                </div>
                <div class="col-sm-1">
                    Escolaridade
                </div>
                <div class="col-sm-1">
                    Profissão
                </div>
                <div class="col-sm-1">
                    Salário
                </div>

            </div>
            @for($i = 1; $i <= 14; $i++ )
                <div class="row">
                    <div class="col-sm-1">
                        {{ $i }}
                    </div>
                    <div class="col-sm-3">
                        {{ Form::text('familiar_nome'.$i); }}
                        <p class="bg-danger"></p>
                    </div>
                    <div class="col-sm-1">
                        {{ Form::select('familiar_parentesco'.$i, ['Cônjuge'    => 'Cônjuge',
                                    'Pai'         => 'Pai',
                                    'Mãe'        => 'Mãe',
                                    'Irmão(a)'   => 'Irmão(a)',
                                    'Filho(a)'    => 'Filho(a)',
                                    'Tio(a)'      => 'Tio(a)',
                                    'Avó'        => 'Avó',
                                    'Avô'        => 'Avô',
                                    'Primo(a)'    => 'Primo(a)',
                                    'Próprio(a)' => 'Próprio(a)',
                                    'Outros'      => 'Outros',
                                ]) }}
                        <p class="bg-danger"></p>
                    </div>
                    <div class="col-sm-1">
                        {{ Form::text('familiar_documento'.$i) }}
                        <p class="bg-danger"></p>
                    </div>
                    <div class="col-sm-1">
                        {{ Form::text('familiar_idade'.$i) }}
                        <p class="bg-danger"></p>
                    </div>
                    <div class="col-sm-2">
                        {{ Form::text('familiar_sexo'.$i) }}
                        <p class="bg-danger"></p>
                    </div>
                    <div class="col-sm-1">
                        {{ Form::text('familiar_escolaridade'.$i) }}
                        <p class="bg-danger"></p>
                    </div>
                    <div class="col-sm-1">
                        {{ Form::text('familiar_profissao'.$i) }}
                        <p class="bg-danger"></p>
                    </div>
                    <div class="col-sm-1">
                        {{ Form::text('familiar_salario'.$i); }}
                        <p class="bg-danger"></p>
                    </div>

                </div>

            @endfor

            <div class="row">
                <div class="col-sm-6">
                    Situação do(a) responsável pela família? {{ Form::select('responsavel_situacao',  [
                                                                ''                  => 'Escolha',
                                                                'Empregado'         => 'Empregado',
                                                                'Desempregado'      => 'Desempregado',
                                                                'Aposentado'        => 'Aposentado',
                                                                'Trabalho Eventual' => 'Trabalho Eventual',
                                                            ]) }}
                    <p class="bg-danger"></p>
                </div>
                <div class="col-sm-6">
                    Qual atividade desenvolve? {{ Form::text('responsavel_atividade') }}
                    <p class="bg-danger"></p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    Inclusão em benefícios? {{ Form::select('beneficio', ['' => '----', 'Sim' => 'Sim', 'Não' => 'Não']) }}
                    <p class="bg-danger"></p>
                </div>
                <div class="col-sm-2">
                    Bolsa Família: {{ Form::text('bolsa_familia') }}
                    <p class="bg-danger"></p>
                </div>
                <div class="col-sm-2">
                    BPC: {{ Form::text('bpc') }}
                    <p class="bg-danger"></p>
                </div>
                <div class="col-sm-5">
                    <div class="col-sm-6">
                        Outra bolsa: {{ Form::text('bolsa_outros') }}
                        <p class="bg-danger"></p>
                    </div>
                    <div class="col-sm-6">
                        Valor da bolsa: {{ Form::text('bolsa_outros_valor') }}
                        <p class="bg-danger"></p>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-sm-6">
                    Recebe ou participa de algum Programa/Serviço/Benefício ou ação da SMAS? {{ Form::select('programa', ['' => '----', 'Sim' => 'Sim', 'Não' => 'Não']) }}
                    <p class="bg-danger"></p>
                </div>
                <div class="col-sm-6">
                    Qual? {{ Form::text('programa_qual') }}
                    <p class="bg-danger"></p>
                </div>
            </div>

      </div>
      <div class="tab-pane fade" id="habitacao">
          <div class="row">
                <div class="col-sm-3">
                    Casa
                    {{ Form::select('casa', ['' => '----', 'Própria' => 'Própria', 'Cedida' => 'Cedida', 'Alugada' => 'Alugada']) }}
                    <p class="bg-danger"></p>
                </div>
                <div class="col-sm-3">
                    Valor da casa
                    {{ Form::text('casa_valor') }}
                    <p class="bg-danger"></p>
                </div>
                <div class="col-sm-3">
                    Tipo
                    {{ Form::select('casa_tipo', ['' => '----', 'Alvenaria' => 'Alvenaria', 'Madeira' => 'Madeira', 'Mista' => 'Mista', 'Papelão/Lona' => 'Papelão/Lona']) }}
                    <p class="bg-danger"></p>
                </div>
                <div class="col-sm-3">
                    Número de cômodos
                    {{ Form::text('casa_comodos') }}
                    <p class="bg-danger"></p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    Situação da casa
                    {{ Form::select('casa_situacao', ['' => '----', 'Ótima' => 'Ótima', 'Boa' => 'Boa', 'Razoável'  => 'Razoável', 'Precárias' => 'Precárias']) }}
                    <p class="bg-danger"></p>
                </div>
                <div class="col-sm-3">
                    Energia elétrica
                    {{ Form::select('casa_energia', ['' => '----', 'Regularizada' => 'Regulariada', 'Não regularizada' => 'Não regularizada', 'Razoável' => 'Razoável', 'Precárias' => 'Precárias']) }}
                    <p class="bg-danger"></p>
                </div>
                <div class="col-sm-3">
                    Água
                    {{ Form::select('casa_agua', ['' => '----', 'Canalizada' => 'Canalizada', 'Não canalizada' => 'Não canalizada']) }}
                    <p class="bg-danger"></p>
                </div>
                <div class="col-sm-3">
                    Instalações Sanitárias
                    {{ Form::select('casa_sanidade',['' => '----','Sim' => 'Sim', 'Não' => 'Não']) }}
                    <p class="bg-danger"></p>
                </div>


            </div>

      </div>
        <button type="submit" class="btn btn-default btn-success"  aria-expanded="false"> Salvar dados </button>
    {{ Form::close() }}

  </div>


</div>


@stop

@section('js')
    <script>
            var doenca_familia = $('#doenca_familia').val();
            if(doenca_familia == 'Sim'){
                $('#div_doenca_familia').show();
            }else{
                $('#div_doenca_familia').hide();
            }
            $(document).on('change','#doenca_familia', function(e){
                var doenca_familia = $('#doenca_familia').val();
                if(doenca_familia == 'Sim'){
                    $('#div_doenca_familia').show();
                }else{
                    $('#div_doenca_familia').hide();
                }
            });

            var necessidade_especial = $('#necessidade_especial').val();
            if(necessidade_especial == 'Sim'){
                $('#div_necessidade_especial').show();
            }else{
                $('#div_necessidade_especial').hide();
            }
            $(document).on('change','#necessidade_especial', function(e){
                var necessidade_especial = $('#necessidade_especial').val();
                if(necessidade_especial == 'Sim'){
                    $('#div_necessidade_especial').show();
                }else{
                    $('#div_necessidade_especial').hide();
                }
            });

            $('#nascimento').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR',
                todayBtn: 'linked',
                autoclose: true
            });


    </script>
        <style>
            .vagas{
                color: red;
            }
        </style>
@stop