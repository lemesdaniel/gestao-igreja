@extends('layout.admin')

@section('css')
    {{ HTML::style('//cdn.datatables.net/1.9.4/css/jquery.dataTables.css'); }}
    {{ HTML::style('//cdn.datatables.net/tabletools/2.2.0/css/dataTables.tableTools.css'); }}
@stop

@section('js')
    {{ HTML::script('//cdn.datatables.net/1.9.4/js/jquery.dataTables.min.js'); }}
    {{ HTML::script('//cdn.datatables.net/tabletools/2.2.0/js/dataTables.tableTools.min.js'); }}
@stop

@section('title')
    {{ $title }}
@stop

@section('content')
    <div class="row">

        @if($dados)

        <div class="col-sm-12">
            <h3>Relatório de alunos matriculados em 2016</h3>
        </div>

        <div class="col-sm-offset-9 col-sm-3">
            <a href="relatorioAlunos/export">
                <i class="fa fa-file-excel-o"> Gerar em excel</i>
            </a>
        </div>

        <div class="col-sm-12">

            <h4 style="padding: 20px; font-weight: bold;">Total de alunos: {{ $numeroInscritos }}</h4>
            <h4 style="padding: 0 20px 0; font-weight: bold;">Total por sexo:</h4>

            <table style="margin-left: 20px; margin-bottom: 20px">
                <tr>
                    <td>Masculino: &nbsp;</td>
                    <td>{{ $masculino }}</td>
                </tr>
                <tr>
                    <td>Feminino: &nbsp;</td>
                    <td>{{ $feminino }}</td>
                </tr>
                <tr>
                    <td>Outros: &nbsp;</td>
                    <td>{{ $outros }}</td>
                </tr>
            </table>

            <table class="table">
                <?php $oficina = ''; ?>

                @foreach($dados as $a)
                    @if($a->oficina != $oficina)
                        @if($oficina != '')
                        <tr>
                            <td colspan="4">&nbsp;</td>
                        </tr>
                        @endif
                        <tr>
                            <td colspan="4"> Oficina: <b>{{ $a->oficina }}</b></td>
                        </tr>
                        <?php $oficina = $a->oficina; ?>
                        <tr>
                            <th></th>
                            <th>Cód.</th>
                            <th>Nome</th>
                            <th>Sexo</th>
                    @endif

                    @if(!empty($a->aluno))
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td>{{ $a->id }}</td>
                            <td>{{ $a->aluno }}</td>
                            <td>{{ $a->sexo }}</td>
                        </tr>
                    @endif
                @endforeach
            </table>

        </div>
            <div class="col-sm-12">
            <hr/>
                </div>

        @endif
    </div>

@stop