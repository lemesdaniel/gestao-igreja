@extends('layout.admin')

@section('css')
    {{ HTML::style('//cdn.datatables.net/1.9.4/css/jquery.dataTables.css'); }}
    {{ HTML::style('//cdn.datatables.net/tabletools/2.2.0/css/dataTables.tableTools.css'); }}
@stop

@section('js')
    {{ HTML::script('//cdn.datatables.net/1.9.4/js/jquery.dataTables.min.js'); }}
    {{ HTML::script('//cdn.datatables.net/tabletools/2.2.0/js/dataTables.tableTools.min.js'); }}
@stop

@section('title')
    {{ $title }}
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <form method="post" class="form-inline">
                {{--<div class="form-group">--}}
                    {{--<label for="nome">Nome:</label>--}}
                    {{--{{ Form::number('nome', null, ['placeholder'=>'']) }}--}}
                {{--</div>--}}

                <div class="col-md-12">
                    <div class="form-group">
                        <label for="oficina">Oficina:</label>
                        {{ Form::select('oficina', [''=>'Todas']+$oficinas) }}
                    </div>
                    <div class="form-group">
                        <label for="nome">Sexo:</label>
                        {{ Form::select('sexo', [''=>'Ambos','Masculino'=>'Masculino','Feminino'=>'Feminino']) }}
                    </div>
                    <div class="form-group">
                        <label for="nome">Turno:</label>
                        {{ Form::select('turno', [''=>'Todos','Manhã'=>'Manhã','Tarde'=>'Tarde', 'Noite'=>'Noite', 'Integral'=>'Integral']) }}
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-8 form-group">
                        <label for="nome">Idade:</label>
                        {{ Form::number('idade_i', null, ['placeholder'=>'mínima', 'class'=>'form-control']) }}
                        {{ Form::number('idade_s', null, ['placeholder'=>'máxima', 'class'=>'form-control']) }}
                        <button type="submit" class="btn btn-primary">Filtrar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        @if($result)
        <div class="col-sm-6" style="margin-top: 20px">
            Total de Resultados: {{ $count }}
        </div>

        <div class="col-sm-offset-5 col-sm-1" style="margin-top: 20px">
            <a href="relatorio/print{{$qString}}" target="_blank">
                <i class="fa fa-print"></i>
            </a>
            <a href="relatorio/export{{$qString}}">
                <i class="fa fa-file-excel-o"></i>
            </a>
        </div>

        <div class="col-sm-12">
            <table class="table">
            <?php
                $oficina_id = '';
                $turma_id  = '';
            ?>
            @foreach($result as $a)
                @if($a->oficina_id != $oficina_id)
                    <?php
                        $oficina_id = $a->oficina_id;
                    ?>
                    @if($oficina_id != '')
                    <tr>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                    @endif
                    <tr>
                        <td colspan="4"> Oficina: <b>{{ $a->oficina }}</b></td>
                    </tr>
                @endif


                @if($a->turma_id != $turma_id)
                        <?php
                            $turma_id = $a->turma_id;
                        ?>
                        @if($turma_id != '')
                        <tr>
                            <td colspan="4">&nbsp;</td>
                        </tr>
                        @endif

                        <tr>
                            <td colspan="2"> Turma: <b>{{ $a->turma . ' - ' . $a->turno }}</b></td>
                            <td colspan="2"> Colaborador: <b>{{ $a->colaborador }}</b></td>
                        </tr>
                        <tr>
                            <th>Código</th>
                            <th>Nome</th>
                            <th>Sexo</th>
                            <th>Idade</th>
                        </tr>
                @endif

                <tr>
                <td>{{ $a->aluno_id }}</td>
                <td>{{ $a->aluno }}</td>
                <td>{{ (!empty($a->sexo) ? $a->sexo : 'Feminino') }}</td>
                <td>{{ $a->idade }}</td>
                </tr>
            @endforeach
            </table>
        </div>
        <div class="col-sm-12">
            <hr/>
        </div>
        @else
            @if(Request::isMethod('post'))
                <div class="col-sm-12" style="margin-top: 30px;">
                    Nenhum resultado encontrado.
                </div>
            @endif
        @endif
    </div>
@stop