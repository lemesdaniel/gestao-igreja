@extends('layout.admin')

@section('title') 
    @if ($action == 'add')
    Gestão Projari - Nova Função
    @elseif ($action == 'edit')
    Gestão Projari - Editando Função
    @else ($action == 'delete')
    Gestão Projari - Excluindo Função
    @endif
@stop

@section('h1') 
    Funçoes
@stop

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-content">
                <h4 class="page-header">
                    @if ($action == 'add')
                    Adicionando nova função
                    @elseif ($action == 'edit')
                    Editando função
                    @else ($action == 'delete')
                    Excluindo função
                    @endif
                </h4>
                
                @if ( ! $errors->isEmpty() )
                @foreach ( $errors->all() as $error )
                <div class="alert alert-danger alert-dismissable">
                    <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                    {{ $error }}
                </div>
                @endforeach
                @endif
                
                {{ $form }}
            </div>
        </div>
    </div>
</div>

@stop
