@extends('layout.admin')

@section('title') 
	{{ $title }}
@stop

@section('h1') 
	{{ $title }}
@stop

@section('content')

<div class="row">
    {{ $filter }}
    <p>

        {{ $grid }}
        
        
    </p>
</div>

@stop
