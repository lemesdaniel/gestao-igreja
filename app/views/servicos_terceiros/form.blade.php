@extends('layout.admin')

@section('title') 
    @if ($action == 'add')
    Gestão Projari - Novo Serviço
    @elseif ($action == 'edit')
    Gestão Projari - Editando Serviço
    @else ($action == 'delete')
    Gestão Projari - Excluindo Serviço
    @endif
@stop

@section('h1') 
    Serviços de Terceiros
@stop

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-content">
                <h4 class="page-header">
                    @if ($action == 'add')
                    Adicionando novo Serviço
                    @elseif ($action == 'edit')
                    Editando Serviço
                    @else ($action == 'delete')
                    Excluindo Serviço
                    @endif
                </h4>
                
                @if ( ! $errors->isEmpty() )
                @foreach ( $errors->all() as $error )
                <div class="alert alert-danger alert-dismissable">
                    <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                    {{ $error }}
                </div>
                @endforeach
                @endif
                
                @if (Session::has('message'))
                <div class="alert alert-{{ Session::get('type') }} alert-dismissable">
                    <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                    {{ Session::get('message') }}
                </div>
                @endif

                {{ $form }}
            </div>
        </div>
    </div>
</div>

@stop
