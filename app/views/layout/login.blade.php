<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('title')</title>

    <!-- Bootstrap Core CSS -->
    {{ HTML::style('assets/css/bootstrap.min.css') }}

    <!-- MetisMenu CSS -->
    {{ HTML::style('assets/css/plugins/metisMenu/metisMenu.min.css') }}

    <!-- Timeline CSS -->
    {{ HTML::style('assets/css/plugins/timeline.css') }}

    <!-- Custom CSS -->
    {{ HTML::style('assets/css/sb-admin-2.css') }}

    <!-- Morris Charts CSS -->
    {{ HTML::style('assets/css/plugins/morris.css') }}

    <!-- Custom Fonts -->
    {{ HTML::style('assets/font-awesome-4.1.0/css/font-awesome.min.css') }}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="row text-center">
                        {{ HTML::image('assets/img/logo.png', '', ['style'=>'width:250px;']) }}
                    </div>    
                    @yield('content')
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery Version 1.11.0 -->
    {{ HTML::script('assets/js/jquery-2.1.1.min.js') }}

    <!-- Bootstrap Core JavaScript -->
    {{ HTML::script('assets/js/bootstrap.min.js') }}

    <!-- Metis Menu Plugin JavaScript -->
    {{ HTML::script('assets/js/plugins/metisMenu/metisMenu.min.js') }}

    <!-- Morris Charts JavaScript -->
    {{ HTML::script('assets/js/plugins/morris/raphael.min.js') }}
    {{ HTML::script('assets/js/plugins/morris/morris.min.js') }}
    {{ HTML::script('assets/js/plugins/morris/morris-data.js') }}

    <!-- Custom Theme JavaScript -->
    {{ HTML::script('assets/js/sb-admin-2.js') }}
</body>

</html>