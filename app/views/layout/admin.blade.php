@if (!Request::ajax())
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('title')</title>



    <!-- CSS -->

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

    <!-- MetisMenu CSS -->
    {{ HTML::style('assets/css/plugins/metisMenu/metisMenu.min.css') }}

    <!-- DataTables CSS -->
    {{ HTML::style('assets/css/plugins/dataTables.bootstrap.css') }}

    <!-- Timeline CSS -->
    {{ HTML::style('assets/css/plugins/timeline.css') }}

    <!-- Custom CSS -->
    {{ HTML::style('assets/css/sb-admin-2.css') }}

    <!-- Morris Charts CSS -->
    {{ HTML::style('assets/css/plugins/morris.css') }}

    <!-- Custom Fonts -->
    {{ HTML::style('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css') }}

    <!-- touchspin CSS -->
    {{ HTML::style('assets/css/jquery.bootstrap-touchspin.min.css') }}

    <!-- Datepicker CSS -->
    {{ HTML::style('assets/eternicode-bootstrap-datepicker/css/datepicker.css') }}
    {{ HTML::style('assets/eternicode-bootstrap-datepicker/css/datepicker3.css') }}

    <!-- Notify CSS -->
    {{ HTML::style('assets/bootstrap-notify/css/bootstrap-notify.css') }}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery Version 1.11.0 -->
    {{ HTML::script('assets/js/jquery-2.1.1.min.js') }}


    <!-- CSS pessoal -->
    {{ HTML::style('assets/css/style.css') }}

    @yield('css')


</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ route('admin') }}">{{ HTML::image('assets/img/logo.png', '', ['style'=>'width:150px;']) }}</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="{{ route('logout') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu"
                            >

                        <li>
                            <a class="{{{ Request::is('admin*') ? 'active ' : '' }}}" href="{{ route('admin') }}"><i class="fa fa-dashboard fa-fw"></i> Página Inicial</a>
                        </li>

                        <li class="{{{ Request::is('colaboradores*') || Request::is('funcoes*') || Request::is('estado-civil*') || Request::is('unidades*')  || Request::is('eventos*') || Request::is('formas_pagamento*') || Request::is('servicos_terceiros*') ? 'active ' : '' }}}">
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Cadastros básicos<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a class="{{{ Request::is('funcoes*') ? 'active ' : '' }}}" href="/funcoes"><i class="fa fa-bar-chart-o fa-fw"></i> Funções</a>
                                </li>
                                <li>
                                    <a class="{{{ Request::is('estado-civil*') ? 'active ' : '' }}}" href="/estado-civil"><i class="fa fa-bar-chart-o fa-fw"></i> Estado Civil</a>
                                </li>

                                <li>
                                    <a class="{{{ Request::is('formas_pagamento*') ? 'active ' : '' }}}" href="{{ route('formas_pagamento') }}"><i class="fa fa-bar-chart-o fa-fw"></i> Formas de Pagamento</a>
                                </li>
                                <li>
                                    <a class="{{{ Request::is('unidades*') ? 'active ' : '' }}}" href="{{ route('unidades') }}"><i class="fa fa-bar-chart-o fa-fw"></i> Unidades</a>
                                </li>
                                <li>
                                    <a class="{{{ Request::is('eventos*') ? 'active ' : '' }}}" href="{{ route('eventos') }}"><i class="fa fa-bar-chart-o fa-fw"></i> Agenda/Eventos</a>
                                </li>
                                <li>
                                    <a class="{{{ Request::is('colaboradores*') ? 'active ' : '' }}}" href="/colaboradores"><i class="fa fa-bar-chart-o fa-fw"></i> Colaboradores</a>
                                </li>
                                <li>
                                    <a class="{{{ Request::is('servicos_terceiros*') ? 'active ' : '' }}}" href="{{ route('servicos_terceiros') }}"><i class="fa fa-bar-chart-o fa-fw"></i> Serviços de Terceiros</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->

                        </li>



                        <li class="{{{ Request::is('alunos*') || Request::is('religiao*') ||  Request::is('etnias*') || Request::is('escolas*') || Request::is('atendimentos*') ? 'active ' : '' }}}">
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Alunos<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a class="{{{ Request::is('religiao*') ? 'active ' : '' }}}" href="/religiao"><i class="fa fa-bar-chart-o fa-fw"></i> Religiões</a>
                                </li>
                                <li>
                                    <a class="{{{ Request::is('etnias*') ? 'active ' : '' }}}" href="/etnias"><i class="fa fa-bar-chart-o fa-fw"></i> Etnias</a>
                                </li>
                                <li>
                                    <a class="{{{ Request::is('escolas*') ? 'active ' : '' }}}" href="/escolas"><i class="fa fa-bar-chart-o fa-fw"></i> Escolas</a>
                                </li>
                                <li>
                                    <a class="{{{ Request::is('alunos') ? 'active ' : '' }}}" href="/alunos"><i class="fa fa-bar-chart-o fa-fw"></i> Alunos</a>
                                </li>

                                <li>
                                    <a class="{{{ Request::is('alunos/relatorio') ? 'active ' : '' }}}" href="/alunos/relatorio"><i class="fa fa-bar-chart-o fa-fw"></i> Relatório de Alunos</a>
                                </li>

                                <li>
                                    <a class="{{{ Request::is('alunos/relatorioAlunos') ? 'active ' : '' }}}" href="/alunos/relatorioAlunos"><i class="fa fa-bar-chart-o fa-fw"></i> Matriculados 2016</a>
                                </li>

                            </ul>
                            <!-- /.nav-second-level -->

                        </li>


                        <li class="{{{  Request::is('pedagogico*')  ? 'active ' : '' }}}">
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Pedagógico<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a  href="/pedagogico/relatorio-diario"><i class="fa fa-bar-chart-o fa-fw"></i> Relatório Diário</a>
                                </li>
                                <li>
                                    <a class="{{{ Request::is('atendimentos*') ? 'active ' : '' }}}" href="/pedagogico/atendimentos"><i class="fa fa-bar-chart-o fa-fw"></i> Atendimentos</a>
                                </li>
                                <li>
                                    <a  href="/pedagogico/orientacoes"><i class="fa fa-bar-chart-o fa-fw"></i> Orientação pedagógica</a>
                                </li>
                                <li>
                                    <a  href="/pedagogico/projetos"><i class="fa fa-bar-chart-o fa-fw"></i> Projetos</a>
                                </li>
                                <li>
                                    <a  href="/pedagogico/encontros"><i class="fa fa-bar-chart-o fa-fw"></i> Encontros</a>
                                </li>
                                <li>
                                    <a  href="/pedagogico/projetos-parceiros"><i class="fa fa-bar-chart-o fa-fw"></i> Projetos parceiros</a>
                                </li>

                            </ul>
                            <!-- /.nav-second-level -->

                        </li>


                        <li class="{{{ Request::is('oficinas*') || Request::is('turmas*') ? 'active ' : '' }}}">
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Oficinas<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="/turmas/form"><i class="fa fa-bar-chart-o fa-fw"></i> Cadastro de Turmas</a>
                                </li>
                                <li>
                                    <a href="/turmas"><i class="fa fa-bar-chart-o fa-fw"></i>Listagem de Turmas</a>
                                </li>
                                <li>
                                    <a href="/oficinas/cadastro"><i class="fa fa-bar-chart-o fa-fw"></i> Cadastro de Oficina</a>
                                </li>
                                <li>
                                    <a href="/oficinas"><i class="fa fa-bar-chart-o fa-fw"></i>Listagem de Oficina</a>
                                </li>

                            </ul>
                            <!-- /.nav-second-level -->

                        </li>

                        <li class="{{{ Request::is('estoque*') ? 'active ' : '' }}}">
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Estoque<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="/estoque/produtos"><i class="fa fa-bar-chart-o fa-fw"></i> Produtos</a>
                                </li>
                            </ul>
                        </li>
                        <li class="{{{ Request::is('financeiro*') ? 'active ' : '' }}}">
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Financeiro<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="/financeiro/recurso"><i class="fa fa-bar-chart-o fa-fw"></i> Fonte de recursos</a>
                                </li>
                                <li>
                                    <a href="/financeiro/centro"><i class="fa fa-bar-chart-o fa-fw"></i> Centro de custos</a>
                                </li>
                                <li>
                                    <a href="/financeiro/conta/"><i class="fa fa-bar-chart-o fa-fw"></i> Contas</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Caixa</a>
                                    <ul class="nav nav-third-level">

                                        <li>
                                            <a href="/financeiro/caixa/"><i class="fa fa-bar-chart-o fa-fw"></i> Movimentação</a>
                                        </li>
                                        <li>
                                            <a href="/financeiro/caixa/entrada"><i class="fa fa-bar-chart-o fa-fw"></i> Entrada</a>
                                        </li>
                                        <li>
                                            <a href="/financeiro/caixa/saida"><i class="fa fa-bar-chart-o fa-fw"></i> Saída</a>
                                        </li>
                                    </ul>
                                </li>


                            </ul>
                        </li>


<!--                         <li class="{{{ Request::is('relatorios*')  ? 'active ' : '' }}}">
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Relatórios<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="/relatorios/visao-geral"><i class="fa fa-bar-chart-o fa-fw"></i> Visão Geral</a>
                                </li>

                            </ul>

                        </li>
 -->


                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">@yield('h1')</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <div id="ajax_content">
@endif
                @yield('content')
@if (!Request::ajax())
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
</body>
    <!-- JS -->


    <!-- Bootstrap Core JavaScript -->
    {{ HTML::script('assets/js/bootstrap.min.js') }}

    <!-- Metis Menu Plugin JavaScript -->
    {{ HTML::script('assets/js/plugins/metisMenu/metisMenu.min.js') }}

<!-- DataTables JavaScript -->
    {{ HTML::script('assets/js/plugins/dataTables/jquery.dataTables.js') }}
    {{ HTML::script('assets/js/plugins/dataTables/dataTables.bootstrap.js') }}

    <!-- Custom Theme JavaScript -->
    {{ HTML::script('assets/js/sb-admin-2.js') }}


    <!-- bootbox Plugin JavaScript -->
    {{ HTML::script('assets/js/bootbox.min.js') }}

    <!-- devoops Plugin JavaScript -->
    {{ HTML::script('assets/js/devoops.js') }}

    <!-- touchspin Plugin JavaScript -->
    {{ HTML::script('assets/js/jquery.bootstrap-touchspin.min.js') }}

    <!-- maskMoney Plugin JavaScript -->
    {{ HTML::script('assets/js/jquery.maskMoney.min.js') }}

    <!-- maskedinput Plugin JavaScript -->
    {{ HTML::script('assets/js/jquery.maskedinput.min.js') }}

    <!-- Notify Plugin JavaScript -->
    {{ HTML::script('assets/bootstrap-notify/js/bootstrap-notify.js') }}
    {{ HTML::script('http://cdnjs.cloudflare.com/ajax/libs/vue/0.11.10/vue.min.js') }}

    <!-- Datepicker Plugin JavaScript -->
    {{ HTML::script('assets/eternicode-bootstrap-datepicker/js/bootstrap-datepicker.js') }}
    {{ HTML::script('assets/eternicode-bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR.js') }}

    {{ Rapyd::head() }}

    {{ Rapyd::scripts() }}
    {{ HTML::script('assets/js/functions.js') }}
    @yield('js')

</html>
@endif