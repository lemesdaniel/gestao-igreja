@if (!Request::ajax())
    <!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>@yield('title')</title>



        <!-- CSS -->

        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

        <!-- MetisMenu CSS -->
        {{ HTML::style('assets/css/plugins/metisMenu/metisMenu.min.css') }}

        <!-- DataTables CSS -->
        {{ HTML::style('assets/css/plugins/dataTables.bootstrap.css') }}

        <!-- Timeline CSS -->
        {{ HTML::style('assets/css/plugins/timeline.css') }}

        <!-- Custom CSS -->
        {{ HTML::style('assets/css/sb-admin-2.css') }}

        <!-- Morris Charts CSS -->
        {{ HTML::style('assets/css/plugins/morris.css') }}

        <!-- Custom Fonts -->
        {{ HTML::style('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css') }}

        <!-- touchspin CSS -->
        {{ HTML::style('assets/css/jquery.bootstrap-touchspin.min.css') }}

        <!-- Datepicker CSS -->
        {{ HTML::style('assets/eternicode-bootstrap-datepicker/css/datepicker.css') }}
        {{ HTML::style('assets/eternicode-bootstrap-datepicker/css/datepicker3.css') }}

        <!-- Notify CSS -->
        {{ HTML::style('assets/bootstrap-notify/css/bootstrap-notify.css') }}

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- jQuery Version 1.11.0 -->
        {{ HTML::script('assets/js/jquery-2.1.1.min.js') }}


        <!-- CSS pessoal -->
        {{ HTML::style('assets/css/style.css') }}

        @yield('css')


    </head>

    <body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ route('admin') }}">{{ HTML::image('assets/img/logo.png', '', ['style'=>'width:150px;']) }}</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="{{ route('professor.logout') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu"
                            >

                        <li>
                            <a class="{{{ Request::is('instrutor*') ? 'active ' : '' }}}" href="{{ route('professor.oficinas') }}"><i class="fa fa-dashboard fa-fw"></i> Minhas oficinas</a>
                        </li>




                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">@yield('h1')</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <div id="ajax_content">
                @endif
                @yield('content')
                @if (!Request::ajax())
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    </body>
    <!-- JS -->


    <!-- Bootstrap Core JavaScript -->
    {{ HTML::script('assets/js/bootstrap.min.js') }}

    <!-- Metis Menu Plugin JavaScript -->
    {{ HTML::script('assets/js/plugins/metisMenu/metisMenu.min.js') }}

    <!-- DataTables JavaScript -->
    {{ HTML::script('assets/js/plugins/dataTables/jquery.dataTables.js') }}
    {{ HTML::script('assets/js/plugins/dataTables/dataTables.bootstrap.js') }}

    <!-- Custom Theme JavaScript -->
    {{ HTML::script('assets/js/sb-admin-2.js') }}


    <!-- bootbox Plugin JavaScript -->
    {{ HTML::script('assets/js/bootbox.min.js') }}

    <!-- devoops Plugin JavaScript -->
    {{ HTML::script('assets/js/devoops.js') }}

    <!-- touchspin Plugin JavaScript -->
    {{ HTML::script('assets/js/jquery.bootstrap-touchspin.min.js') }}

    <!-- maskMoney Plugin JavaScript -->
    {{ HTML::script('assets/js/jquery.maskMoney.min.js') }}

    <!-- maskedinput Plugin JavaScript -->
    {{ HTML::script('assets/js/jquery.maskedinput.min.js') }}

    <!-- Notify Plugin JavaScript -->
    {{ HTML::script('assets/bootstrap-notify/js/bootstrap-notify.js') }}

    <!-- Datepicker Plugin JavaScript -->
    {{ HTML::script('assets/eternicode-bootstrap-datepicker/js/bootstrap-datepicker.js') }}
    {{ HTML::script('assets/eternicode-bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR.js') }}

    {{ Rapyd::head() }}

    @yield('js')
    {{ Rapyd::scripts() }}
    {{ HTML::script('assets/js/functions.js') }}
    </html>
@endif