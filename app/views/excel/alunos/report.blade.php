<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <body>
        <h3 style="font-family: helvetica;">Relatório de Alunos</h3>

        <div class="col-sm-12" style=" font-family: helvetica;">
            Total de registros: {{ $count }}
        </div>

        <div class="col-sm-12">
            <table class="table" style=" font-family: helvetica;">
            <?php
                $oficina_id = '';
                $turma_id  = '';
            ?>
            @foreach($result as $a)
                @if($a->oficina_id != $oficina_id)
                    <?php
                        $oficina_id = $a->oficina_id;
                    ?>
                    @if($oficina_id != '')
                    <tr>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                    @endif
                    <tr>
                        <td colspan="4"> Oficina: <b>{{ $a->oficina }}</b></td>
                    </tr>
                @endif


                @if($a->turma_id != $turma_id)
                        <?php
                            $turma_id = $a->turma_id;
                        ?>
                        @if($turma_id != '')
                        <tr>
                            <td colspan="4">&nbsp;</td>
                        </tr>
                        @endif

                        <tr>
                            <td colspan="2"> Turma: <b>{{ $a->turma . ' - ' . $a->turno }}</b></td>
                            <td colspan="2"> Colaborador: <b>{{ $a->colaborador }}</b></td>
                        </tr>
                        <tr>
                            <td colspan="4">&nbsp;</td>
                        </tr>
                        <tr style="text-align: left;">
                            <td style="font-weight: bold;">Código</td>
                            <td style="font-weight: bold;">Nome</td>
                            <td style="font-weight: bold;">Sexo</td>
                            <td style="font-weight: bold;">Idade</td>
                        </tr>
                @endif

                <tr>
                <td>{{ $a->aluno_id }}</td>
                <td>{{ $a->aluno }}</td>
                <td>{{ (!empty($a->sexo) ? $a->sexo : 'Feminino') }}</td>
                <td>{{ $a->idade }}</td>
                </tr>
            @endforeach
            </table>
    </body>
</html>
