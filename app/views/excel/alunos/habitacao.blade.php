<html>
<meta charset="UTF-8">

	<table>
		<tr>
			<td>Casa</td>  
			<td> {{ $aluno->casa }} </td>
		</tr>
		<tr>
			<td>Valor</td>
			<td> {{ $aluno->casa_valor }} </td>
		</tr>
		<tr>
			<td>Tipo</td>
			<td>{{ $aluno->casa_tipo }}</td>
		</tr>
		<tr>
			<td>Número de cômodos</td>
			<td>{{ $aluno->casa_comodos }} </td>
		</tr>
		<tr>
			<td>Situação</td>
			<td> {{ $aluno->casa_situacao }} </td>
		</tr>
		<tr>
			<td>Energia Elétrica</td>
			<td> {{ $aluno->casa_energia }} </td>
		</tr>
		<tr>
			<td>Água</td>
			<td> {{ $aluno->casa_agua }} </td>
		</tr>
		<tr>
			<td>Instalações sanitárias</td>
			<td> {{ $aluno->casa_sanidade }} </td>
		</tr>

	</table>

</html>
                    