<html>
<meta charset="UTF-8">

	<table>
	<tr>
		<td><b>Escola</b></td>
		<td><b>Série</b></td>
		<td><b>Turno</b></td>
	</tr>

	<tr>
		<td> {{ $aluno->escola->nome or '' }} </td>
		<td> {{ $aluno->serie }} </td>
		<td> {{ $aluno->turno }} </td>
	</tr>

	<tr></tr>

	<tr>
		<td><b>Possui carteira escolar</b></td>
		<td><b>Como vai se deslocar</b></td>
		<td><b>Foi atendido por outra instituição ou projeto social?</b></td>
	</tr>
	<tr>
		<td>{{ $aluno->carteira_escolar }}</td>
		<td>{{ $aluno->deslocamento }}</td>
		<td>{{ $aluno->ja_atendido }}</td>
	</tr>
	<tr>
		<td>Carteira de vacinação em dia?</td>
		<td>Toma alguma medicação?</td>
		@if($aluno->medicacao=='Sim')
			<td>Qual medicação?</td>
		@endif
	</tr>
	<tr>
		<td>{{ $aluno->carteira_vacinacao }}</td>
		<td>{{ $aluno->medicacao}}</td>
		@if($aluno->medicacao=='Sim')
			<td>{{ $aluno->medicacao_nome }}</td>
		@endif
	</tr>

	<tr>
		<td>Atestado médico para atividades físicas?</td>
		<td>Possui alergia</td>
		@if($aluno->alergia=='Sim')
			<td>Qual alergia</td>	
		@endif	
	</tr>
	<tr>
		<td>{{ $aluno->atestado_medico }}</td>
		<td>{{ $aluno->alergia }} </td>
		@if($aluno->alergia=='Sim')
			<td>{{ $aluno->alergia_qual }}</td>	
		@endif	
	</tr>
	
	<tr></tr>

	<tr>
		<td><b>Em caso de emergência chamar</b></td>
	</tr>
	<tr>
		<td>Nome</td>
		<td>{{$aluno->emergencia_nome}}</td>
	</tr>
	<tr>
		<td>Contato</td>
		<td>{{$aluno->emergencia_endereco}}</td>
	</tr>
	<tr>
		<td>Telefone</td>
		<td>{{$aluno->emergencia_fone}}</td>
	</tr>
	<tr>
		<td>Parentesco</td>
		<td>{{ $aluno->emergencia_parentesco }}</td>
	</tr>

    <tr>
        <td>
            Encaminhamento 
        </td>
        <td>{{ $aluno->encaminhamento }}</td>
    <tr>    
        <td>
            Outro encaminhamento não listado
        </td>
        <td>
        	{{ $aluno->encaminhamento_outros }}
        </td>
    </tr>

    <tr>
        <td>
        	Alguma doença na família?
        </td>
        <td>
        	{{ $aluno->doenca_familia }}
        </td>
    </tr>

    @if($aluno->doenca_familia)
	    <tr>
	        <td>
	            Qual familiar? 
	        </td>
	        <td>
	        	{{ $aluno->doenca_qual_familiar }}
	        </td>
	    </tr>    
	    
	    <tr>
	        <td>
	            Em tratamento? 
	        </td>
	        <td>
	        	{{ $aluno->doenca_tratamento }}
	        </td>
	    </tr>    

	    <tr>    
	        <td>
	            Médico responsável? 
	        </td>
	        <td>
	        	{{ $aluno->doenca_medico }}
	        </td>
	    </tr>
    @endif


    <tr>
        <td>
            Gestante na família?
            {{ Form::select('gestante_familia', ['' => '----', 'Sim' => 'Sim', 'Não' => 'Não']) }}
        </td>
        <td>{{ $aluno->gestante_familia }}</td>
    </tr>
    @if($aluno->gestante_familia=='Sim')
	    <tr>
	        <td>Quem é a gestante?</td>
	        <td>{{ $aluno->gestante_quem }}</td>
	    </tr>    
	    <tr>    
	        <td>Tempo de gestação?</td>
	        <td>{{ $aluno->gestante_mes }}</td>
	    </tr>    
	    <tr>    
	        <td>Faz pré natal?</td>
	        <td>{{ $aluno->gestante_pre_natal }}</td>
	    </tr>
    @endif

    <tr>

        <td>
            Algum familiar com necessidade especial?
        </td>
        <td>{{ $aluno->necessidade_especial }}</td>
    </tr>    
        @if($aluno->necessidade_especial)
        	<tr>
			    <td>
	                Qual?
	            </td>
	            <td>
	            	{{ $aluno->necessidade_qual }}
	            </td>
            </tr>
        	<tr>
	            <td>
	                Recebe acompanhamento médico?
	            </td>
	            <td>    
	                {{ $aluno->necessidade_acompanhamento }}
	            </td>
	        </tr>
        	<tr>    
	            <td>
	                Recebe BPC?
	            </td>
	            <td>    
	                {{ $aluno->necessidade_bpc }}
	            </td>
	        </tr>
        @endif
    </tr>
    </table>

</html>