<html>
<meta charset="UTF-8">

            <h4 style="padding: 20px; font-weight: bold;">Total de alunos: {{ $numeroInscritos }}</h4>
            <h4 style="padding: 0 20px 0; font-weight: bold;">Total por sexo:</h4>

            <table style="margin-left: 20px; margin-bottom: 20px">
                <tr>
                    <td>Masculino: &nbsp;</td>
                    <td>{{ $masculino }}</td>
                </tr>
                <tr>
                    <td>Feminino: &nbsp;</td>
                    <td>{{ $feminino }}</td>
                </tr>
                <tr>
                    <td>Outros: &nbsp;</td>
                    <td>{{ $outros }}</td>
                </tr>
            </table>

            <table class="table">
                <?php $oficina = ''; ?>

                @foreach($dados as $a)
                    @if($a->oficina != $oficina)
                        @if($oficina != '')
                        <tr>
                            <td colspan="4">&nbsp;</td>
                        </tr>
                        @endif
                        <tr>
                            <td colspan="4"> Oficina: <b>{{ $a->oficina }}</b></td>
                        </tr>
                        <?php $oficina = $a->oficina; ?>
                        <tr>
                            <th></th>
                            <th>Cód.</th>
                            <th>Nome</th>
                            <th>Sexo</th>
                    @endif

                    @if(!empty($a->aluno))
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td>{{ $a->id }}</td>
                            <td>{{ $a->aluno }}</td>
                            <td>{{ $a->sexo }}</td>
                        </tr>
                    @endif
                @endforeach
            </table>


</html>
