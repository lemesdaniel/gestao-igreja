<html>
<meta charset="UTF-8">

     <table>
          <tr>
               <td>Há medidas de proteção à criança e/ou adolescente?</td>  
               <td> {{ $aluno->medida_protecao }} </td>
          </tr>
          <tr>
               <td>Conselheiro tutelar responsável</td>
               <td> {{ $aluno->medida_conselheiro }} </td>
          </tr>
          <tr>
               <td>Como conheceu o PROJARI?</td>
               <td>{{ $aluno->conheceu }}</td>
          </tr>
          <tr>
               <td>Você ou alguém da família já fez parte do?</td>
               <td>{{ $aluno->ja_fez_parte }} </td>
          </tr>
          <tr>
               <td>Observações</td>
               <td> {{ $aluno->obs }} </td>
          </tr>
     </table>

</html>
                    