<html>
<meta charset="UTF-8">

	<table>
		<tr>
			<td>Composição Familiar</td>  
		</tr>
		<tr></tr>

		<tr>
			<td><b>Nome</b></td>
			<td><b>Parentesco</b></td>
			<td><b>Documento</b></td>
			<td><b>Idade</b></td>
			<td><b>Sexo</b></td>
			<td><b>Escolaridade</b></td>
			<td><b>Profissão</b></td>
			<td><b>Salário</b></td>
		</tr>
		
		@if($aluno->familiar_nome1))
			<tr>
				<td>{{ $aluno->familiar_nome1 }}</td>
				<td>{{ $aluno->familiar_parentesco1 }}</td>
				<td>{{ $aluno->familiar_documento1 }}</td>
				<td>{{ $aluno->familiar_idade1 }}</td>
				<td>{{ $aluno->familiar_sexo1 }}</td>
				<td>{{ $aluno->familiar_escolaridade1 }}</td>
				<td>{{ $aluno->familiar_profissao1 }}</td>
				<td>{{ $aluno->familiar_salario1 }}</td>
			</tr>
		@endif
		@if($aluno->familiar_nome2))
			<tr>
				<td>{{ $aluno->familiar_nome2 }}</td>
				<td>{{ $aluno->familiar_parentesco2 }}</td>
				<td>{{ $aluno->familiar_documento2 }}</td>
				<td>{{ $aluno->familiar_idade2 }}</td>
				<td>{{ $aluno->familiar_sexo2 }}</td>
				<td>{{ $aluno->familiar_escolaridade2 }}</td>
				<td>{{ $aluno->familiar_profissao2 }}</td>
				<td>{{ $aluno->familiar_salario2 }}</td>
			</tr>
		@endif	

		@if($aluno->familiar_nome3))
			<tr>
				<td>{{ $aluno->familiar_nome3 }}</td>
				<td>{{ $aluno->familiar_parentesco3 }}</td>
				<td>{{ $aluno->familiar_documento3 }}</td>
				<td>{{ $aluno->familiar_idade3 }}</td>
				<td>{{ $aluno->familiar_sexo3 }}</td>
				<td>{{ $aluno->familiar_escolaridade3 }}</td>
				<td>{{ $aluno->familiar_profissao3 }}</td>
				<td>{{ $aluno->familiar_salario3 }}</td>
			</tr>
		@endif

		@if($aluno->familiar_nome4))
			<tr>
				<td>{{ $aluno->familiar_nome4 }}</td>
				<td>{{ $aluno->familiar_parentesco4 }}</td>
				<td>{{ $aluno->familiar_documento4 }}</td>
				<td>{{ $aluno->familiar_idade4 }}</td>
				<td>{{ $aluno->familiar_sexo4 }}</td>
				<td>{{ $aluno->familiar_escolaridade4 }}</td>
				<td>{{ $aluno->familiar_profissao4 }}</td>
				<td>{{ $aluno->familiar_salario4 }}</td>
			</tr>
		@endif

		@if($aluno->familiar_nome5))
			<tr>
				<td>{{ $aluno->familiar_nome5 }}</td>
				<td>{{ $aluno->familiar_parentesco5 }}</td>
				<td>{{ $aluno->familiar_documento5 }}</td>
				<td>{{ $aluno->familiar_idade5 }}</td>
				<td>{{ $aluno->familiar_sexo5 }}</td>
				<td>{{ $aluno->familiar_escolaridade5 }}</td>
				<td>{{ $aluno->familiar_profissao5 }}</td>
				<td>{{ $aluno->familiar_salario5 }}</td>
			</tr>
		@endif

		@if($aluno->familiar_nome6))
			<tr>
				<td>{{ $aluno->familiar_nome6 }}</td>
				<td>{{ $aluno->familiar_parentesco6 }}</td>
				<td>{{ $aluno->familiar_documento6 }}</td>
				<td>{{ $aluno->familiar_idade6 }}</td>
				<td>{{ $aluno->familiar_sexo6 }}</td>
				<td>{{ $aluno->familiar_escolaridade6 }}</td>
				<td>{{ $aluno->familiar_profissao6 }}</td>
				<td>{{ $aluno->familiar_salario6 }}</td>
			</tr>
		@endif

		@if($aluno->familiar_nome7))
			<tr>
				<td>{{ $aluno->familiar_nome7 }}</td>
				<td>{{ $aluno->familiar_parentesco7 }}</td>
				<td>{{ $aluno->familiar_documento7 }}</td>
				<td>{{ $aluno->familiar_idade7 }}</td>
				<td>{{ $aluno->familiar_sexo7 }}</td>
				<td>{{ $aluno->familiar_escolaridade7 }}</td>
				<td>{{ $aluno->familiar_profissao7 }}</td>
				<td>{{ $aluno->familiar_salario7 }}</td>
			</tr>
		@endif

		@if($aluno->familiar_nome8))
			<tr>
				<td>{{ $aluno->familiar_nome8 }}</td>
				<td>{{ $aluno->familiar_parentesco8 }}</td>
				<td>{{ $aluno->familiar_documento8 }}</td>
				<td>{{ $aluno->familiar_idade8 }}</td>
				<td>{{ $aluno->familiar_sexo8 }}</td>
				<td>{{ $aluno->familiar_escolaridade8 }}</td>
				<td>{{ $aluno->familiar_profissao8 }}</td>
				<td>{{ $aluno->familiar_salario8 }}</td>
			</tr>
		@endif

		@if($aluno->familiar_nome9))
			<tr>
				<td>{{ $aluno->familiar_nome9 }}</td>
				<td>{{ $aluno->familiar_parentesco9 }}</td>
				<td>{{ $aluno->familiar_documento9 }}</td>
				<td>{{ $aluno->familiar_idade9 }}</td>
				<td>{{ $aluno->familiar_sexo9 }}</td>
				<td>{{ $aluno->familiar_escolaridade9 }}</td>
				<td>{{ $aluno->familiar_profissao9 }}</td>
				<td>{{ $aluno->familiar_salario9 }}</td>
			</tr>
		@endif

		@if($aluno->familiar_nome10))
			<tr>
				<td>{{ $aluno->familiar_nome10 }}</td>
				<td>{{ $aluno->familiar_parentesco10 }}</td>
				<td>{{ $aluno->familiar_documento10 }}</td>
				<td>{{ $aluno->familiar_idade10 }}</td>
				<td>{{ $aluno->familiar_sexo10 }}</td>
				<td>{{ $aluno->familiar_escolaridade10 }}</td>
				<td>{{ $aluno->familiar_profissao10 }}</td>
				<td>{{ $aluno->familiar_salario10 }}</td>
			</tr>
		@endif

		@if($aluno->familiar_nome11))
			<tr>
				<td>{{ $aluno->familiar_nome11 }}</td>
				<td>{{ $aluno->familiar_parentesco11 }}</td>
				<td>{{ $aluno->familiar_documento11 }}</td>
				<td>{{ $aluno->familiar_idade11 }}</td>
				<td>{{ $aluno->familiar_sexo11 }}</td>
				<td>{{ $aluno->familiar_escolaridade11 }}</td>
				<td>{{ $aluno->familiar_profissao11 }}</td>
				<td>{{ $aluno->familiar_salario11 }}</td>
			</tr>
		@endif

		@if($aluno->familiar_nome12))
			<tr>
				<td>{{ $aluno->familiar_nome12 }}</td>
				<td>{{ $aluno->familiar_parentesco12 }}</td>
				<td>{{ $aluno->familiar_documento12 }}</td>
				<td>{{ $aluno->familiar_idade12 }}</td>
				<td>{{ $aluno->familiar_sexo12 }}</td>
				<td>{{ $aluno->familiar_escolaridade12 }}</td>
				<td>{{ $aluno->familiar_profissao12 }}</td>
				<td>{{ $aluno->familiar_salario12 }}</td>
			</tr>
		@endif

		@if($aluno->familiar_nome13))
			<tr>
				<td>{{ $aluno->familiar_nome13 }}</td>
				<td>{{ $aluno->familiar_parentesco13 }}</td>
				<td>{{ $aluno->familiar_documento13 }}</td>
				<td>{{ $aluno->familiar_idade13 }}</td>
				<td>{{ $aluno->familiar_sexo13 }}</td>
				<td>{{ $aluno->familiar_escolaridade13 }}</td>
				<td>{{ $aluno->familiar_profissao13 }}</td>
				<td>{{ $aluno->familiar_salario13 }}</td>
			</tr>
		@endif

		@if($aluno->familiar_nome14))
			<tr>
				<td>{{ $aluno->familiar_nome14 }}</td>
				<td>{{ $aluno->familiar_parentesco14 }}</td>
				<td>{{ $aluno->familiar_documento14 }}</td>
				<td>{{ $aluno->familiar_idade14 }}</td>
				<td>{{ $aluno->familiar_sexo14 }}</td>
				<td>{{ $aluno->familiar_escolaridade14 }}</td>
				<td>{{ $aluno->familiar_profissao14 }}</td>
				<td>{{ $aluno->familiar_salario14 }}</td>
			</tr>
		@endif

		<tr></tr>

		<tr>
            <td>Situação do(a) responsável pela família?</td>
            <td> {{ $aluno->responsavel_situacao }}</td>
        </tr>
        <tr>
        	<td>Qual atividade desenvolve? </td>
        	<td>{{ $aluno->responsavel_atividade }}</td>
        </tr>    
		<tr>
			<td>Inclusão em benefícios?</td>
			<td>{{ $aluno->beneficio }}</td>
		</tr>        
		<tr>
			<td>Bolsa Família</td>
			<td> {{ $aluno->bolsa_familia }} </td>
		</tr>        
        <tr>
        	<td>BPC</td>
        	<td>{{ $aluno->bpc }}</td>
        </tr>   
        <tr>
        	<td>Outra Bolsa</td>
        	<td>{{ $aluno->bolsa_outros }}</td>
        </tr>
        <tr>
        	<td>Valor bolsa</td>
        	<td>{{ $aluno->bolsa_outros_valor }}</td>
        </tr>
        <tr>
        	<td>Recebe ou participa de algum Programa/Serviço/Benefício ou ação da SMAS?</td>
        	<td>{{ $aluno->programa }}</td>
        	@if($aluno->programa)
	        	<td>Qual programa?</td>
	        	<td> {{ $aluno->programa_qual }} </td>
        	@endif
        </tr>
	</table>

</html>
                    