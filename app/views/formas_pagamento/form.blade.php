@extends('layout.admin')

@section('title') 
    @if ($action == 'add')
    Gestão Projari - Nova Forma de Pagamento
    @elseif ($action == 'edit')
    Gestão Projari - Editando Forma de Pagamento
    @else ($action == 'delete')
    Gestão Projari - Excluindo Forma de Pagamento
    @endif
@stop

@section('h1') 
    Formas de Pagamento
@stop

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-content">
                <h4 class="page-header">
                    @if ($action == 'add')
                    Adicionando nova Forma de Pagamento
                    @elseif ($action == 'edit')
                    Editando Forma de Pagamento
                    @else ($action == 'delete')
                    Excluindo Forma de Pagamento
                    @endif
                </h4>
                
                @if ( ! $errors->isEmpty() )
                @foreach ( $errors->all() as $error )
                <div class="alert alert-danger alert-dismissable">
                    <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                    {{ $error }}
                </div>
                @endforeach
                @endif
                
                {{ $form }}
            </div>
        </div>
    </div>
</div>

@stop
