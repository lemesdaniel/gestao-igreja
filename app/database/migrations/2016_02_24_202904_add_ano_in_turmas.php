<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAnoInTurmas extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('turmas', function(Blueprint $table)
		{
			$table->integer('ano')->default(2015);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('turmas', function(Blueprint $table)
		{
			$table->dropColumn('ano');
		});
	}

}
