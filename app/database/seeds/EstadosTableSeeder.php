<?php

class EstadosTableSeeder extends Seeder {

  public function run()
  {
    DB::select(DB::raw("INSERT INTO `estados` (`id`, `nome`, `sigla`, `pais_id`, `created_at`, `updated_at`) VALUES
        (1, 'Acre', 'AC', 1, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP ),
        (2, 'Alagoas', 'AL', 1, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP ),
        (3, 'Amazonas', 'AM', 1, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP ),
        (4, 'Amapá', 'AP', 1, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP ),
        (5, 'Bahia', 'BA', 1, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP ),
        (6, 'Ceará', 'CE', 1, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP ),
        (7, 'Distrito Federal', 'DF', 1, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP ),
        (8, 'Espírito Santo', 'ES', 1, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP ),
        (9, 'Goiás', 'GO', 1, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP ),
        (10, 'Maranhão', 'MA', 1, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP ),
        (11, 'Minas Gerais', 'MG', 1, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP ),
        (12, 'Mato Grosso do Sul', 'MS', 1, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP ),
        (13, 'Mato Grosso', 'MT', 1, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP ),
        (14, 'Pará', 'PA', 1, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP ),
        (15, 'Paraíba', 'PB', 1, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP ),
        (16, 'Pernambuco', 'PE', 1, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP ),
        (17, 'Piauí', 'PI', 1, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP ),
        (18, 'Paraná', 'PR', 1, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP ),
        (19, 'Rio de Janeiro', 'RJ', 1, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP ),
        (20, 'Rio Grande do Norte', 'RN', 1, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP ),
        (21, 'Rondônia', 'RO', 1, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP ),
        (22, 'Roraima', 'RR', 1, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP ),
        (23, 'Rio Grande do Sul', 'RS', 1, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP ),
        (24, 'Santa Catarina', 'SC', 1, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP ),
        (25, 'Sergipe', 'SE', 1, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP ),
        (26, 'São Paulo', 'SP', 1, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP ),
        (27, 'Tocantins', 'TO', 1, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP)"));

    $this->command->info('Estados cadastrados!');
  }
}