<?php

return array(

    'multi' => array(
        'admin' => array(
            'driver' => 'eloquent',
            'model' => 'User'
        ),
        'professor' => array(
            'driver' => 'eloquent',
            'model' => 'Colaborador'
        )
    ),

    'reminder' => array(

        'email' => 'emails.auth.reminder',

        'table' => 'password_reminders',

        'expire' => 60,

    ),

);

//return array(
//	'driver' => 'eloquent',
//	'model' => 'User',
//	'table' => 'users',
//	'reminder' => array(
//
//		'email' => 'emails.auth.reminder',
//
//		'table' => 'password_reminders',
//
//		'expire' => 60,
//
//	),
//
//);
