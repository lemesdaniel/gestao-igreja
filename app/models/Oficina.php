<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 29/03/15
 * Time: 13:24
 */

class Oficina extends \Eloquent{
    protected $table = 'oficinas';
    protected $fillable = [];

    public function turmas()
    {
        return $this->hasMany('Turma');
    }

    public function alunos()
    {
        return $this->turmas->hasManyThrough('Aluno', 'Turma', 'oficina_id', 'turma_id');

    }

}