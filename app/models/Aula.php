<?php

class Aula extends \Eloquent
{
    protected $table = 'aulas';
    protected $guarded = array('id');

    public function colaborador()
    {
        return $this->belongsTo('Colaborador', 'colaborador_id');
    }

    public function turma()
    {
        //return $this->belongsTo('User', 'local_key', 'parent_key');
        return $this->belongsTo('Turma', 'turma_id');
    }

    public function getDates()
    {
        return array('created_at', 'data');
    }

    public function getDataAttribute()
    {
        return \Carbon::createFromFormat('Y-m-d', $this->attributes['data'])->format('d/m/Y');
    }

    public function setDataAttribute($value)
    {
        if(empty($value))
            return $value;

        $this->attributes['data'] = \Carbon::createFromFormat('d/m/Y', $value)->toDateTimeString();
    }

    public function chamada()
    {
        return $this->belongsToMany('Aluno','chamadas', 'aula_id', 'aluno_id')->withPivot('presenca');;
    }


}

