<?php

class Caixa extends \Eloquent
{
    protected $table = 'caixa';
    protected $guarded = array('id');

    public function oficina()
    {
        return $this->belongsTo('Oficina', 'oficina_id');
    }

    public function conta()
    {
        return $this->belongsTo('Conta', 'conta_id');
    }

    public function usuario()
    {
        return $this->belongsTo('User', 'usuario_id');
    }

    public function recurso()
    {
        //return $this->belongsTo('User', 'local_key', 'parent_key');
        return $this->belongsTo('Recurso', 'recurso_id');
    }

    public function setValorAttribute($value)
    {
        
        if(empty($value))
            return $value;
        $valor = str_replace(",", ".", str_replace(".", "", $value));

        $this->attributes['valor'] = $valor;
    }


    public function getDates()
    {
        return array('created_at');
    }

    

}

