<?php

class FormasPagamentos extends \Eloquent {
    protected $table = 'formas_pagamentos';

    public $fillable = [ 'forma' ];
    
     public function form_tabs() {
        return [
            'dados_cadastrais' => 'Dados Cadastrais',
        ];
    }

    public function fields_grid() {
        return [];
    }

    public function structure() {
        return [
            DT::Text('forma', [
                'Label' => 'Forma de Pagamento',
                'TabPage' => 'dados_cadastrais',
                'Size' => 100,
                'Line' => '1',
                'Column' => '1',
                'TabIndex' => 1,
                'Required' => true,
            ]),
        ];
    }
       
        
        
}