<?php

class Encontro extends \Eloquent
{
    protected $fillable = [];

    public function responsavel()
    {
        return $this->belongsTo('Colaborador', 'responsavel_id');
    }
}

