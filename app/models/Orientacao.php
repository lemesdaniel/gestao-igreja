<?php

class Orientacao extends \Eloquent
{
    protected $table    = 'orientacoes';
    protected $fillable = [];

    public function responsavel()
    {
        return $this->belongsTo('Colaborador', 'responsavel_id');
    }
}

