<?php

class Turma extends \Eloquent
{
    protected $fillable = [];

    public function colaboradores()
    {
        return $this->belongsTo('Colaborador', 'responsavel_id');
    }

    public function oficinas()
    {
        return $this->belongsTo('Oficina', 'oficina_id');
    }

    public function getNomeCompletoAttribute()
    {
        return (isset($this->oficinas->nome)?$this->oficinas->nome . ' - '   : '') .
                    $this->attributes['nome'] . ' - ' .
                    $this->attributes['turno'] . ' - ' .
                    substr($this->attributes['hora_inicio'], 0, 5) . ' às ' . substr($this->attributes['hora_fim'], 0, 5) . ' - idade: ' .
                    $this->attributes['idade_minima'] . ' ate ' . $this->attributes['idade_maxima'] .
                    ' - local: ' . $this->attributes['local'] . ' - dias: ' . $this->attributes['dias']

                    ;
    }
    public function alunos()
    {
        return $this->belongsToMany('Aluno', 'aluno_turma', 'turma_id', 'aluno_id')->orderBy("alunos.nome")->withPivot('motivo', 'cancelado');

    }

    public function aulas()
    {
        return $this->hasMany('Aula')->orderBy('data');
    }


}

