<?php

class Aluno extends \Eloquent
{
    protected $table = 'alunos';
    protected $guarded = array('id');

    public function estadoCivil()
    {
        return $this->belongsTo('EstadoCivil', 'estado_civil_id');
    }

    public function etnia()
    {
        //return $this->belongsTo('User', 'local_key', 'parent_key');
        return $this->belongsTo('Etnia', 'etnia_id');
    }

    public function getDates()
    {
        return array('created_at', 'nascimento');
    }

    public function getNascimentoAttribute()
    {
        return \Carbon::createFromFormat('Y-m-d', $this->attributes['nascimento'])->format('d/m/Y');
    }

    public function setNascimentoAttribute($value)
    {
        if(empty($value))
            return $value;

        $this->attributes['nascimento'] = \Carbon::createFromFormat('d/m/Y', $value)->toDateTimeString();
    }

    public function getIdadeAttribute()
    {

        return (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $this->attributes['nascimento']))
                ? \Carbon::createFromFormat('Y-m-d', $this->attributes['nascimento'])->age
                : '';
    }

    public function religiao()
    {
        return $this->belongsTo('Religiao', 'religiao_id');
    }

    public function escola()
    {
        return $this->belongsTo('Escola', 'escola_id');
    }

    public function turmas()
    {
        return $this->belongsToMany('Turma', 'aluno_turma', 'aluno_id', 'turma_id')->withPivot('motivo', 'cancelado');
    }


}

