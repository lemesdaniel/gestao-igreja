<?php

class Atendimento extends \Eloquent {
	protected $fillable = [];

	public function responsavel()
    {
        return $this->belongsTo('Colaborador', 'responsavel_id');
    }

	public function aluno()
    {
        return $this->belongsTo('Aluno', 'aluno_id');
    }
}