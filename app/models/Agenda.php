<?php

class Agenda extends \Eloquent {
    protected $table = 'agenda';

    public $fillable = [ 'data_evento',
                         'evento',
                         'local',
                         'horario',
                         'responsavel_id',
                         'material',
                         'transporte'];
    
     public function form_tabs() {
        return [
            'dados_cadastrais' => 'Dados Cadastrais',
        ];
    }

    public function fields_grid() {
        return [];
    }

    public function structure() {
        return [
            DT::Date('data_evento', [
                'Label' => 'Data do evento',
                'TabPage' => 'dados_cadastrais',
                'Line' => '1',
                'Column' => '1',
                'TabIndex' => 1,
            ]),
            DT::Text('evento', [
                'Label' => 'Evento',
                'Size' => 150,
                'TabPage' => 'dados_cadastrais',
                'Line' => '1',
                'Column' => '2',
                'TabIndex' => 2,
                'Required' => true,
            ]),
            DT::Text('local', [
                'Label' => 'Local',
                'Size' => 100,
                'TabPage' => 'dados_cadastrais',
                'Line' => '2',
                'Column' => '1',
                'TabIndex' => 3,
            ]),
            DT::Text('horario', [
                'Label' => 'Hora',
                'Size' => 30,
                'TabPage' => 'dados_cadastrais',
                'Line' => '2',
                'Column' => '2',
                'TabIndex' => 3,
            ]),
            DT::Responsavel('responsavel_id', [
                'Label' => 'Responsável',
                'Size' => 20,
                'TabPage' => 'dados_cadastrais',
                'Line' => '2',
                'Column' => '3',
                'TabIndex' => 4,
            ]),
            DT::TextArea('material', [
                'Label' => 'Material Necessário',
                'TabPage' => 'dados_cadastrais',
                'Line' => '3',
                'Column' => '1',
                'Size' => 150,
                'TabIndex' => 5,
            ]),
            DT::TextArea('transporte', [
                'Label' => 'Tipo de Transporte',
                'TabPage' => 'dados_cadastrais',
                'Line' => '3',
                'Column' => '2',
                'Size' => 150,
                'TabIndex' => 6,
            ]),
        ];
    }
       
        
        
}