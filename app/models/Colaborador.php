<?php
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;


class Colaborador extends Eloquent implements UserInterface, RemindableInterface
{
    use UserTrait, RemindableTrait;

    protected $table = 'colaboradores';
    protected $hidden = array('password');
    protected $guarded = array('password');

    public $fillable = [ 'nome',
                         'data_nascimento',
                         'naturalidade',
                         'telefone',
                         'estado',
                         'cidade',
                         'bairro',
                         'endereco',
                         'numero',
                         'rg',
                         'cpf',
                         'email',
                         'funcao_id',
                         'admissao',
                         'demissao'];
    
    public function funcoes()
    {
        return $this->belongsTo('Funcao', 'funcao_id');
    }

    public function estados()
    {
        //return $this->belongsTo('User', 'local_key', 'parent_key');
        return $this->belongsTo('Estado', 'estado', 'sigla');
    }   
        
        
}