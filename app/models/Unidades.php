<?php

class Unidades extends \Eloquent {
    protected $table = 'unidades';

    public $fillable = [ 'unidade',
                         'local',
                         'responsavel_id',
                         'telefone',
                         'estado',
                         'cidade',
                         'bairro',
                         'endereco',
                         'numero' ];
    
     public function form_tabs() {
        return [
            'dados_cadastrais' => 'Dados Cadastrais',
        ];
    }

    public function fields_grid() {
        return [];
    }

    public function structure() {
        return [
            DT::Text('unidade', [
                'Label' => 'Unidade',
                'Size' => 150,
                'TabPage' => 'dados_cadastrais',
                'Line' => '1',
                'Column' => '1',
                'TabIndex' => 1,
                'Required' => true,
            ]),
            DT::Text('local', [
                'Label' => 'Local',
                'Size' => 100,
                'TabPage' => 'dados_cadastrais',
                'Line' => '1',
                'Column' => '2',
                'TabIndex' => 2,
            ]),
            DT::Responsavel('responsavel_id', [
                'Label' => 'Responsável',
                'Size' => 20,
                'TabPage' => 'dados_cadastrais',
                'Line' => '2',
                'Column' => '1',
                'TabIndex' => 3,
            ]),
            DT::Phone('telefone', [
                'Label' => 'Telefone',
                'TabPage' => 'dados_cadastrais',
                'Line' => '2',
                'Column' => '2',
                'TabIndex' => 4,
            ]),
            DT::States('estado', [
                'Label' => 'Estado',
                'TabPage' => 'dados_cadastrais',
                'Line' => '3',
                'Column' => '1',
                'TabIndex' => 5,
            ]),
            DT::Text('cidade', [
                'Label' => 'Cidade',
                'TabPage' => 'dados_cadastrais',
                'Line' => '3',
                'Column' => '2',
                'Size' => 150,
                'TabIndex' => 6,
            ]),
            DT::Text('bairro', [
                'Label' => 'Bairro',
                'TabPage' => 'dados_cadastrais',
                'Line' => '3',
                'Column' => '3',
                'Size' => 50,
                'TabIndex' => 7,
            ]),
            DT::Text('endereco', [
                'Label' => 'Endere&ccedil;o',
                'Size' => 150,
                'TabPage' => 'dados_cadastrais',
                'Line' => '4',
                'Column' => '1',
                'TabIndex' => 8,
            ]),
            DT::Text('numero', [
                'Label' => 'N&uacute;mero',
                'TabPage' => 'dados_cadastrais',
                'Line' => '4',
                'Column' => '2',
                'Size' => 10,
                'TabIndex' => 9,
            ]),
        ];
    }
       
        
        
}