<?php

class Funcao extends \Eloquent {
    protected $table = 'funcoes';

    public $fillable = [ 'funcao' ];

    public function colaboradores()
    {
        return $this->hasMany('Colaboradores');
    }
    
}