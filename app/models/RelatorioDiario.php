<?php

class RelatorioDiario extends \Eloquent
{
    protected $table    = 'relatorio_diario';
    protected $fillable = [];

    public function responsavel()
    {
        return $this->belongsTo('Colaborador', 'responsavel_id');
    }

    public function oficina()
    {
        return $this->belongsTo('Turma', 'oficina_id');
    }
}

