<?php

class Projeto extends \Eloquent
{
    protected $fillable = [];

    public function responsavel()
    {
        return $this->belongsTo('Colaborador', 'responsavel_id');
    }

    public function oficinas()
    {
        return $this->belongsToMany('Oficina', 'oficina_projeto', 'projeto_id', 'oficina_id');
    }
}

