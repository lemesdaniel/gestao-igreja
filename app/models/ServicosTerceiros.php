<?php

class ServicosTerceiros extends \Eloquent {
    
    protected $table = 'servicos_terceiros';

    public $fillable = [ 'nome_empresa',
                         'data_servico',
                         'valor',
                         'telefone',
                         'email',
                         'nota_fiscal',
                         'formas_pagamentos_id',
                         'orcamento'];
    
     public function form_tabs() {
        return [
            'dados_cadastrais' => 'Dados Cadastrais',
        ];
    }

    public function fields_grid() {
        return [];
    }

    public function structure() {
        return [
            DT::Text('nome_empresa', [
                'Label' => 'Nome Empresa',
                'Size' => 150,
                'TabPage' => 'dados_cadastrais',
                'Line' => '1',
                'Column' => '1',
                'TabIndex' => 1,
                'Required' => true,
            ]),
            DT::Date('data_servico', [
                'Label' => 'Data do Serviço',
                'TabPage' => 'dados_cadastrais',
                'Line' => '1',
                'Column' => '2',
                'TabIndex' => 2,
            ]),
            DT::Money('valor', [
                'Label' => 'Valor do Serviço',
                'TabPage' => 'dados_cadastrais',
                'Size' => 11,
                'Line' => '1',
                'Column' => '3',
                'TabIndex' => 3,
            ]),
            DT::Phone('telefone', [
                'Label' => 'Telefone',
                'TabPage' => 'dados_cadastrais',
                'Line' => '2',
                'Column' => '1',
                'TabIndex' => 4,
            ]),
            DT::Email('email', [
                'Label' => 'E-mail',
                'Size' => 150,
                'TabPage' => 'dados_cadastrais',
                'Line' => '2',
                'Column' => '2',
                'TabIndex' => 5,
            ]),
            DT::Text('nota_fiscal', [
                'Label' => 'Nota Fiscal',
                'Size' => 100,
                'TabPage' => 'dados_cadastrais',
                'Line' => '3',
                'Column' => '1',
                'TabIndex' => 6,
            ]),
            DT::FormasPagamento('formas_pagamentos_id', [
                'Label' => 'Forma de Pagamento',
                'Size' => 20,
                'TabPage' => 'dados_cadastrais',
                'Line' => '3',
                'Column' => '2',
                'TabIndex' => 7,
            ]),
            DT::Text('orcamento', [
                'Label' => 'Orçamento',
                'Size' => 100,
                'TabPage' => 'dados_cadastrais',
                'Line' => '3',
                'Column' => '3',
                'TabIndex' => 8,
            ]),
        ];
    }
}