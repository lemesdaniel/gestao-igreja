function loadpage(url, el, dataType) {

    if (dataType == undefined) {
        dataType = 'html';
    }
    
    $.ajax({
        mimeType: 'text/html; charset=utf-8', // ! Need set mimeType only when run from local file
        url: url,
        method: 'get',
        type: 'get',
        success: function (data) {
            $(el).html(data);
            events();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        },
        dataType: dataType,
        async: false
    });
}

function danger(msg) {
    $('.top-right').notify({
        type: 'danger',
        message: {
            text: msg
        }
    }).show();
}

function success(msg) {
    $('.top-right').notify({
        type: 'success',
        message: {
            text: msg
        }
    }).show();
}

function info(msg) {
    $('.top-right').notify({
        type: 'info',
        message: {
            text: msg
        }
    }).show();
}

function deleteRegister(urldelete, url, dataType) {

    if (dataType == undefined) {
        dataType = 'html';
    }

    bootbox.dialog({
        message: "Deseja excluir esse registro?",
        title: "Exclusão de registro",
        buttons: {
            success: {
                label: "Excluir",
                className: "btn-primary",
                callback: function (result) {
                    if (result) {
                        $.ajax({
                            mimeType: 'text/html; charset=utf-8',
                            url: urldelete,
                            method: 'get',
                            type: 'get',
                            success: function (data) {
                                loadpage(url, '#ajax_content');
                                success('Excluído com sucesso');
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                console.log(errorThrown);
                            },
                            dataType: dataType,
                            async: false
                        });
                    }
                }
            },
            main: {
                label: "Cancelar",
                className: "btn-info",
                callback: function () {
                }
            }
        }
    });
}

var events = function () {
    $(".date").datepicker({
        format: "dd/mm/yyyy",
        todayBtn: "linked",
        language: "pt-BR",
        autoclose: true,
        todayHighlight: true
    });
    $("[rel=tooltip]").tooltip({
        placement: "auto"
    });
    $("[rel=modal]").modal({
        backdrop: true
    });
    $('.money').maskMoney({
        allowNegative: true,
        thousands: '.',
        decimal: ',',
        affixesStay: false
    });
    $("#AFBtnDelete").on("click", function () {
        urldelete = $(this).data('delete');
        url = $(this).data('redirect');
        deleteRegister(urldelete, url);
    });
    
    $('#dataTables-example').dataTable();

    $('tr').on("click", function() {
        if($(this).data('href') !== undefined){
            document.location = $(this).data('href');
        }
    });
}

$(document).ready(function () {
    events();
})
