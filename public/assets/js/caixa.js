Vue.config.debug = true;

var entrada = new Vue({
  el: '#caixaEntrada',
  data:{
    valor: '',
    conta_id: '',

  },
  attached: function(){

    VMasker(this.$$.valor).maskMoney({
      precision: 2, 
      separator: ',', 
      delimiter: '.', 
    });
  }
});


var saida = new Vue({
  el: '#caixaSaida',
  data:{
    valor: '',
    conta_id: '',
    showSaldo: false,
    saldo: 0
  },
  attached: function(){
    
    VMasker(this.$$.valor).maskMoney({
      precision: 2, 
      separator: ',', 
      delimiter: '.', 
    });
  },
  methods: {
    getSaldo: function(e){
      var self = this;
      jQuery.get( "/api/saldo/"+this.conta_id, function( data ) {
        self.saldo = data.saldo;     
        self.showSaldo = true;
        
      });
    }
  }
});