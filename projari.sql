-- MySQL dump 10.13  Distrib 5.5.38, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: projari
-- ------------------------------------------------------
-- Server version	5.5.38-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `agenda`
--

DROP TABLE IF EXISTS `agenda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agenda` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_evento` date DEFAULT NULL,
  `evento` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `local` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `horario` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `responsavel_id` int(10) unsigned NOT NULL,
  `material` text COLLATE utf8_unicode_ci NOT NULL,
  `transporte` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `agenda_responsavel_id_foreign` (`responsavel_id`),
  CONSTRAINT `agenda_responsavel_id_foreign` FOREIGN KEY (`responsavel_id`) REFERENCES `colaboradores` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agenda`
--

LOCK TABLES `agenda` WRITE;
/*!40000 ALTER TABLE `agenda` DISABLE KEYS */;
INSERT INTO `agenda` VALUES (1,'2014-12-07','Treinamento Sistema Infoprojari','Projari','10',1,'computador','','2014-12-08 16:33:11','2014-12-08 16:45:07'),(2,'2014-12-20','Batizado e troca de corda','Sede do PROJARI','09h às 12h',7,'kjfdkjfhakjfhakjdfhkjdfh','kjdfhkjadfhkjshdfkjhsdfkjhskdfh','2014-12-08 21:26:35','2015-01-12 23:00:29');
/*!40000 ALTER TABLE `agenda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aluno_oficina`
--

DROP TABLE IF EXISTS `aluno_oficina`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aluno_oficina` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `aluno_id` int(10) unsigned NOT NULL,
  `oficina_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `aluno_oficina_aluno_id_index` (`aluno_id`),
  KEY `aluno_oficina_oficina_id_index` (`oficina_id`),
  CONSTRAINT `aluno_oficina_oficina_id_foreign` FOREIGN KEY (`oficina_id`) REFERENCES `oficinas` (`id`) ON DELETE CASCADE,
  CONSTRAINT `aluno_oficina_aluno_id_foreign` FOREIGN KEY (`aluno_id`) REFERENCES `alunos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=135 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aluno_oficina`
--

LOCK TABLES `aluno_oficina` WRITE;
/*!40000 ALTER TABLE `aluno_oficina` DISABLE KEYS */;
INSERT INTO `aluno_oficina` VALUES (5,7,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(6,8,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(9,10,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(13,9,2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(16,5,3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(17,12,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(18,13,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(19,13,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(20,15,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(21,16,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(22,16,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(23,17,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(24,17,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(25,18,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(26,18,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(27,19,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(28,19,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(29,20,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(30,20,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(31,21,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(32,21,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(33,22,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(34,22,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(35,23,3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(37,24,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(38,24,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(41,26,3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(47,32,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(48,32,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(49,32,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(60,39,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(61,40,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(64,43,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(65,45,2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(66,42,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(67,42,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(69,47,2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(70,47,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(76,50,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(77,51,3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(78,52,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(85,58,3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(86,54,2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(87,54,3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(88,53,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(89,53,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(91,46,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(95,25,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(99,55,3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(101,49,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(102,60,3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(104,61,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(106,36,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(107,36,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(111,63,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(112,66,2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(113,64,2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(114,62,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(118,67,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(119,70,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(120,71,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(121,72,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(123,48,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(124,30,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(128,73,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(129,73,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(130,74,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(131,74,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(132,75,9,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(133,78,17,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(134,29,4,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `aluno_oficina` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alunos`
--

DROP TABLE IF EXISTS `alunos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alunos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `nascimento` date NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rg` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `numero` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `bairro` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `fone` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `nis` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `estado_civel_id` int(11) DEFAULT NULL,
  `etnia_id` int(11) DEFAULT NULL,
  `religiao_id` int(11) DEFAULT NULL,
  `escola_id` int(11) DEFAULT NULL,
  `naturalidade` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `serie` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `turno` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `carteira_escolar` enum('Sim','Não') COLLATE utf8_unicode_ci NOT NULL,
  `deslocamento` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ja_atendido` enum('Sim','Não') COLLATE utf8_unicode_ci NOT NULL,
  `carteira_vacinacao` enum('Sim','Não') COLLATE utf8_unicode_ci NOT NULL,
  `medicacao` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `medicao_nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `emergencia_nome` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `emergencia_endereco` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `emergencia_fone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `emergencia_parentesco` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `encaminhamento` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `encaminhamento_outros` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_nome1` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_parentesco1` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_documento1` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_idade1` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_sexo1` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_escolaridade1` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_profissao1` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_salario1` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_nome2` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_parentesco2` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_documento2` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_idade2` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_sexo2` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_escolaridade2` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_profissao2` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_salario2` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_nome3` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_parentesco3` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_documento3` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_idade3` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_sexo3` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_escolaridade3` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_profissao3` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_salario3` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_nome4` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_parentesco4` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_documento4` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_idade4` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_sexo4` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_escolaridade4` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_profissao4` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_salario4` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_nome5` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_parentesco5` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_documento5` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_idade5` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_sexo5` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_escolaridade5` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_profissao5` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_salario5` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_nome6` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_parentesco6` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_documento6` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_idade6` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_sexo6` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_escolaridade6` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_profissao6` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_salario6` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_nome7` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_parentesco7` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_documento7` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_idade7` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_sexo7` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_escolaridade7` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_profissao7` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_salario7` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_nome8` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_parentesco8` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_documento8` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_idade8` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_sexo8` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_escolaridade8` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_profissao8` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_salario8` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_nome9` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_parentesco9` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_documento9` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_idade9` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_sexo9` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_escolaridade9` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_profissao9` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_salario9` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_nome10` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_parentesco10` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_documento10` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_idade10` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_sexo10` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_escolaridade10` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_profissao10` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_salario10` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_nome11` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_parentesco11` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_documento11` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_idade11` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_sexo11` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_escolaridade11` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_profissao11` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_salario11` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_nome12` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_parentesco12` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_documento12` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_idade12` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_sexo12` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_escolaridade12` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_profissao12` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_salario12` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_nome13` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_parentesco13` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_documento13` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_idade13` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_sexo13` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_escolaridade13` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_profissao13` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_salario13` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_nome14` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_parentesco14` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_documento14` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_idade14` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_sexo14` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_escolaridade14` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_profissao14` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `familiar_salario14` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `responsavel_situacao` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `responsavel_atividade` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `beneficio` enum('Sim','Não') COLLATE utf8_unicode_ci NOT NULL,
  `bolsa_familia` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `bpc` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `bolsa_outros` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `bolsa_outros_valor` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `programa` enum('Sim','Não') COLLATE utf8_unicode_ci NOT NULL,
  `programa_qual` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `doenca_familia` enum('Sim','Não') COLLATE utf8_unicode_ci NOT NULL,
  `doenca_qual` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `doenca_qual_familiar` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `doenca_tratamento` enum('Sim','Não') COLLATE utf8_unicode_ci NOT NULL,
  `doenca_medico` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `atestado_medico` enum('Sim','Não') COLLATE utf8_unicode_ci NOT NULL,
  `alergia` enum('Sim','Não') COLLATE utf8_unicode_ci NOT NULL,
  `alergia_qual` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `gestante_familia` enum('Sim','Não') COLLATE utf8_unicode_ci NOT NULL,
  `gestante_quem` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `gestante_mes` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `gestante_pre_natal` enum('Sim','Não') COLLATE utf8_unicode_ci NOT NULL,
  `necessidade_especial` enum('Sim','Não') COLLATE utf8_unicode_ci NOT NULL,
  `necessidade_qual` enum('Mental','Física','Auditiva','Visual','Multipla') COLLATE utf8_unicode_ci NOT NULL,
  `necessidade_acompanhamento` enum('Sim','Não') COLLATE utf8_unicode_ci NOT NULL,
  `necessidade_bpc` enum('Sim','Não') COLLATE utf8_unicode_ci NOT NULL,
  `medida_protecao` enum('Sim','Não') COLLATE utf8_unicode_ci NOT NULL,
  `medida_conselheiro` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `casa` enum('Própria','Cedida','Irregular','Alugada') COLLATE utf8_unicode_ci NOT NULL,
  `casa_valor` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `casa_tipo` enum('Alvenaria','Madeira','Mista','Papelão/Lona') COLLATE utf8_unicode_ci NOT NULL,
  `casa_comodos` int(11) NOT NULL,
  `casa_situacao` enum('Ótima','Boa','Razoável','Precárias') COLLATE utf8_unicode_ci NOT NULL,
  `casa_energia` enum('Regularizada','Não regularizada','Não possui') COLLATE utf8_unicode_ci NOT NULL,
  `casa_agua` enum('Canalizada','Não canalizada') COLLATE utf8_unicode_ci NOT NULL,
  `casa_sanidade` enum('Sim','Não') COLLATE utf8_unicode_ci NOT NULL,
  `data` date NOT NULL,
  `obs` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `conheceu` enum('Amigos','Escola','Igreja','Jornais','Familiares','Outros') COLLATE utf8_unicode_ci NOT NULL,
  `sexo` enum('Outros','Masculino','Feminino') COLLATE utf8_unicode_ci NOT NULL,
  `ja_fez_parte` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alunos`
--

LOCK TABLES `alunos` WRITE;
/*!40000 ALTER TABLE `alunos` DISABLE KEYS */;
INSERT INTO `alunos` VALUES (5,'Jhiullia Alexia Menezes ','2004-02-01','799.jpg','6125715596','Rua Noel Guarani','421','Jardim dos Lagos','86477808','',NULL,2,1,4,'Porto Alegre','6 ano','Manhã','Sim','Onibus','Não','Sim','Não','','Jaqueline Menezes','51-85080490','51-80133227','Mãe','Outros','','Jaqueline dos Santos Menezes','Mãe','003.758.380/82','32','Feminino','Ensino Superior incompleto','Professora/ Pedagoga','1.400,00','','','','','Feminino','','','','Claiton Jeovani Capiotti Bom','Padrasto','676.048.030/91','41','Masculino','Ensino Médio completo','Motorista','2.000,00','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','Empregado','Motorista','Não','','','','','Não','','Não','','','Sim','','Não','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',5,'Boa','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-09 20:10:55','2015-01-12 18:05:28','Amigos','Outros',''),(7,'Pamela dos Santos Koszeniewski','1999-01-22','','6112923617','Av. Urias Lugon','576','Columbia City','51-93948438','',NULL,2,9,23,'Guaíba','2º ano','Manhã','Sim','A pé','Não','Sim','Não','','Marilda de Fatima Lemos dos Santos','51-92368397','','Mãe','Outros','','Marilda de Fatima Lemos dos Santos','Mãe','4055180642','47','Feminino','5ª','Diarista','1100,00','Ricardo Clair Koszeniewski','Pai','','50','Masculino','E. M. completo','Pedreito','1000,00','Gabriela dos Santos Koszeniewski','Irmã','','25','Feminino','Superior incompleto','Aux. Admin','1600,00','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','Trabalho Eventual','Pedreiro','Não','','','','','Não','','Não','','','Não','','Não','Sim','Pó','Sim','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',5,'Boa','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-12 14:55:34','2015-01-12 14:55:34','Amigos','Outros',''),(8,'Florentina Quadrado Vicente','0000-00-00','','5018021781','Rua João Zeferino','75','Bom Fim Novo','51-98839422','',NULL,2,1,36,'Guaíba','','Manhã','Não','A pé','Não','Não','Sim','','Luciana Assis','','51-99926253','Filha','Outros','','Juliano Quadrado Vicente','Filho','','32','Masculino','Ensino Médio Incompleto','Ajudante','','Florentina Quadrado Vicente','própria','5018021781','64','Feminino','Ensino fundamental incompleto','Aposentada','1600,00','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','Aposentado','','Não','','','','','Não','','Sim','Diabetes e colesterol','Própria','Sim','Dr. Jorge','Não','Não','','Não','','','Sim','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',7,'Razoável','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-12 16:30:17','2015-01-12 16:33:33','Amigos','Outros',''),(9,'Bruna de Oliveira Souza','2001-02-17','','','Rua Pedro Caldas ','217','Bom Fim Novo','5198498561','',NULL,2,1,1,'São Jeronimo','8 ano','Tarde','Não','A pé','Não','Sim','Não','','Maria Helena de Oliveira Souza','5198732735','','Mãe','Outros','','Maria Helena de Oliveira Souza','Mãe','463.075.750/34','46','Feminino','Ensino Médio Completo','Auxiliar de Cozinha','1.200,00','','','','','Feminino','','','','Pâmella Luisa de Oliveira Souza','Irmã','','18','Feminino','Ensino Fundamental incompleto','Atendente','1.100,00','Helen de Oliveira Souza','Irmã','','9','Feminino','Ensino Fundamental incompleto','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','Empregado','Auxiliar de Cozinha','Não','','','','','Não','','Não','','','Não','','Não','Sim','Perfumes','Não','','','Sim','Não','Mental','Não','Não','Não','','Alugada','350,00','Alvenaria',8,'Ótima','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-12 16:52:08','2015-01-12 17:45:49','Amigos','Outros',''),(10,'Maria Helena de Oliveira Souza','0000-00-00','','5038700811','Rua Pedro Caldas ','217','Bom Fim Novo','51-98732735','12220599177',NULL,2,2,36,'São Jeronimo','','Manhã','Não','A pé','Não','Sim','Não','','Pâmella','51-96720017','','Filha','Outros','','Pâmella Luisa de Oliveira Souza','Filha','','18','Feminino','Ensino Fundamental incompleto','','1.100,00','Hellen de Oliveira Souza','Filha','','9','Feminino','Ensino Fundamental incomlpeto','','','Bruna de Oliveira Souza','Filha','','13','Feminino','Ensino Fundamental incompleto','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','','','','','Masculino','','','','Empregado','Auxiliar de Cozinha','Não','','','','','Não','','Não','','','Sim','','Não','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Alugada','350,00','Alvenaria',8,'Ótima','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-12 17:04:01','2015-01-12 17:08:42','Amigos','Outros',''),(12,'Bianca Sampaio Vasconcelos','1999-06-12','','3110438698','Rua C Carolina Paz','20','Bom Fim Velho','51-80548062','',NULL,2,1,23,'Guaíba','2 ano do ensino médio','Manhã','Sim','A pé','Não','Sim','Não','','Celi Rodrigues Sampaio','51-34022698','51-95184530','Vó','Outros','','Nubia Rejane Rodrigues Sampaio','Mãe','','','Feminino','','','','Dagomar de Oliveira Vasconcelos','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','','Não','','','','','Não','','Não','','','Não','','Não','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',5,'Boa','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-12 17:57:00','2015-01-12 17:57:00','Amigos','Outros',''),(13,'Katia Suzana Pereira Oliveira','1972-09-06','','2055203638','Rua Rio Grande do Norte','604','Coronel Nassuca','51-30680087 ou 51-82072342','',NULL,2,1,36,'Guaíba','','Manhã','Não','Outros','Não','Sim','Não','','Leandro Oliveira','51-81707571','','Marido','Outros','','Leandro Oliveira','Marido','657592300/34','43','Masculino','Ensino Médio Completo','Tecnico Eletronica','1.500,00','Guilherme Pereira Oliveira','Filho','','14','Masculino','Ensino Médio Incompleto','Estudante','','Giuliano Pereira Oliveira','Filho','','4','Masculino','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado',' TécnicoEletronica','Não','','','','','Não','','Não','','','Não','','Não','Não','','Não','','','Não','Sim','Mental','Sim','Não','Não','','Própria','','Alvenaria',6,'Boa','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-12 18:53:14','2015-01-12 18:53:14','Amigos','Outros',''),(15,'Zaida Marisa Lima da Silveira','0000-00-00','','6042993417','Av. Vitória Casa Grande','497','Alegria','51-34011752- 93296647','',NULL,2,2,36,'Guaíba','','Manhã','Não','Onibus','Não','Sim','Sim','','Jose Renato Silveira','34011752','','Esposo','CRAS','','Zaida Marisa Lima da Silveira','Própria','6042993417','58','Feminino','Ensino Médio Completo','Aposentada','1606,02','Jose Renato Silveira','Esposo','','60','Masculino','Ensino fundamental incompleto','Autonomo','700,00','Jilma Fernanda Lima da Silveira','Filha','','21','Feminino','Ensino superior incompleto','Estagio','400,00','Renata Lima Monique da Silveira','Filha','','23','Feminino','Ensino Superior Incompleto','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Trabalho Eventual','Autônomo ','Sim','','','Bolsa Enem e bolsista Social','','Sim','Cras','Sim','Pressão alta e coração faquinho','a própria','Sim','Dr. Simone Ovalhe','Não','Sim','Anestesia e contraste','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',8,'Boa','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-12 19:03:02','2015-01-12 19:03:02','Amigos','Outros',''),(16,'Niquele da Cruz Silva','1988-11-26','','4108143001','Rua P','932','São Francisco','51-97079436','',NULL,2,1,36,'Charqueadas','','Manhã','Não','A pé','Não','Sim','Não','','Edgar Vargas Fonceca','51-99240536','','Marido','Outros','','Edgar Vargas Fonseca','Marido','','24','Masculino','Ensino Fundamental incompleto','Auxiliar de Pedreiro','800,00','Ana Luisa Silva Vargas','Filha','','6','Feminino','Ensino Fundamental incomlpeto','Estudante','','Erica Mariana Silva Vargas','Filha','','1ano 5mese','Feminino','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','Auxiliar de Pedreiro','Sim','137,00','','','','Não','','Não','','','Não','','Não','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',4,'Boa','Não regularizada','Canalizada','Sim','0000-00-00','','2015-01-12 19:06:15','2015-01-12 19:06:15','Amigos','Outros',''),(17,'Aline de Fátima Correa da Silva','1996-04-10','','7110891558','Rua Estância Velha','361','São Francisco','51-97946684','',NULL,2,1,36,'Cruz Alta','','Manhã','Não','A pé','Não','Sim','Não','','Vera Lúcia Correa','51-95849111','','Mãe','Outros','Voluntariamente','Aline de Fátima Correa da Silva','Própria','7110891558','18','Feminino','Ensino Médio Incompleto','Operraadora de Caixa','890,00','Rafaella Correa Borges','Filha','','3','Feminino','','','','Rogério Lucas Borges Forte','Marido','','28','Masculino','Ensino Fundamental incompleto','Ferreiro','900,00','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','Ferreiro','Sim','70,00','','','','Não','','Não','','','Sim','','Não','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',6,'Razoável','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-12 19:13:32','2015-01-12 19:13:32','Amigos','Outros',''),(18,'Roseli dos Santos Bortoli','0000-00-00','','636.477.969/72','Rua 27L','4','Nova Guaiba','51-34032137','',NULL,2,1,36,'Ponte Serrada','','Manhã','Não','A pé','Não','Sim','Sim','','Jacir Bortoli','51-98515001','','Marido','Outros','','Adriano Bortoli','Filho','','26','Masculino','Ensino Superior incompleto','','4.000,00','Lucas Bortoli','Filho','','20','Masculino','Ensino Médio Completo/ Técnico Nutrição','','','Jacir Bortoli','Marido','','52','Masculino','Ensino Médio completo','Aposentado','2.000,00','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Aposentado','','Não','','','','','Não','','Não','','','Não','','Não','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',6,'Boa','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-12 19:19:24','2015-01-12 19:19:24','Amigos','Outros',''),(19,'Lusiane Francisca Dias Almeida','1975-03-01','','8071488418','Maneca Paiva','134','Vila Nova','51-97319559','12453046138',NULL,2,1,36,'Guaíba','','Manhã','Não','Onibus','Não','Sim','Não','','Marlei Dias Almeida','51-95861895','','Irmã','Outros','','Lusiane Francisca Dias Almeida','Própria','8071488418','39','Feminino','Ensino Fundamental Completo','do lar','900,00','Ricardo Almeida Jobim','Filho','','6','Masculino','1º EF','','','Rodrigo Almeida Jobim','Filho','','6','Masculino','1º EF','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Desempregado','Aux. Padaria','Sim','238,00','','','','Não','','Não','','','Sim','','Não','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Alugada','600,00','Mista',7,'Razoável','Regularizada','Canalizada','Sim','0000-00-00','Esta no seguro desemprego','2015-01-12 19:22:30','2015-01-12 19:22:30','Amigos','Outros',''),(20,'Clair Borba Armesto','0000-00-00','','6023321109','Rua Colombo Santos Marques','591','Columbia City','51-34012829 ou 51-98889414','',NULL,2,1,36,'São Lourenço do Sul','','Manhã','Não','A pé','Não','Sim','Sim','','João Pedro Armesto','51-34012829','','Marido','Outros','','Maicon Armesto','Filho','','30','Masculino','Ensino Médio Completo','','900,00','João Pedro Armesto','Marido','','64','Masculino','Ensino Fundamental incomlpeto','Aposentado','780,00','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Aposentado','','Não','','','','','Não','','Não','','','Não','','Não','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',5,'Razoável','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-12 19:32:55','2015-01-12 19:32:55','Amigos','Outros',''),(21,'Joice Mineia Fontoura Pacheco','1986-06-09','','8093653098','Ramão Correa','144','Bom Fim Novo','51-82205728','',NULL,2,1,36,'Guaíba','','Manhã','Não','A pé','Não','Sim','Não','','Claudete de Fátima Martins Fontoura','51-30554846','','Mãe','Outros','Voluntariamente','Joice Mineia Fontoura Pacheco','Própria','8093653098','28','Feminino','Ensino Médio Incompleto','do lar','','João Pedro Fontoura Pacheco','Filho','','05','Masculino','1º EF','','','Leandro da Silva Pacheco','Esposo','','31','Masculino','Ensino Fundamental incompleto','Motorista','1400,00','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','Motorista','Não','','','','','Não','','Sim','asma/ problema renal','a própria/filho','Sim','dr. Virgilio/ Dr. Anelise','Não','Sim','','Não','','','Não','Não','Mental','Sim','Não','Não','','Própria','','Alvenaria',6,'Ótima','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-12 19:33:37','2015-01-12 19:33:37','Amigos','Outros',''),(22,'Denise Magri Munhoz Freitas','1979-07-31','','9071187026','Rua João Longuá','466','Bom Fim Novo','51-34032085- 98432781','',NULL,2,1,36,'Guaíba','','Manhã','Não','A pé','Não','Sim','Sim','','Alvanora da costa Freitas','51-34032085','','Sogra','Outros','Voluntariamente','Denise Magri Munhoz Freitas','Própria','9071187026','35','Feminino','Ensino Médio Completo','Aux. Doença','935,00','Mateus Munhoz Freitas','Filho','','10','Masculino','5º EF','','','Luiza Munhoz Freitas','Filha','','05','Feminino','Pré','','','Zevani da Costa Freitas','Esposo','','39','Masculino','Ensino Médio Incompleto','Aux. Doença','1850,00','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Não','','','','','Não','','Sim','depressão/ esquisofrenia','a própria/esposo','Sim','Dr. Godoy/ Dr. Jeferson','Não','Não','','Não','','','Não','Sim','Mental','Sim','Não','Não','','Própria','','Alvenaria',6,'Ótima','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-12 19:43:36','2015-01-12 19:43:36','Amigos','Outros',''),(23,'Luiza Munhoz Freitas','2010-01-06','','098046015520101000780320061898','Rua João Longuá','466','Bom Fim Novo','51-34032085- 98432781','',NULL,2,1,1,'Guaíba','pré','Tarde','Não','A pé','Não','Sim','Sim','','Alvanora da costa Freitas','51-34032085','','Sogra','Outros','Voluntariamente','Denise Magri Munhoz Freitas','Mãe','9071187026','35','Feminino','Ensino Médio Completo','Aux. Doença','935,00','Mateus Munhoz Freitas','Filho','','10','Masculino','5º EF','','','Luiza Munhoz Freitas','Própria','','05','Feminino','Pré','','','Zevani da Costa Freitas','Esposo','','39','Masculino','Ensino Médio Incompleto','Aux. Doença','1850,00','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Não','','','','','Não','','Sim','depressão/ esquisofrenia','a própria/esposo','Sim','Dr. Godoy/ Dr. Jeferson','Não','Não','','Não','','','Não','Sim','Mental','Sim','Não','Não','','Própria','','Alvenaria',6,'Ótima','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-12 19:43:52','2015-01-12 19:46:08','Amigos','Outros',''),(24,'Ângela Fátima Pacheco Borba','0000-00-00','','7095461261','Rua Antonio Martins','238','Bom Fim Velho','51-99799275','',NULL,2,1,36,'Tapes','','Manhã','Não','A pé','Não','Sim','Sim','','Paulo Renato da Fonseca Borba','51-95724193','','Marido','Outros','','Paulo Renato da Fonseca Borba','Marido','','49','Masculino','Ensino Fundamental incompleto','Operador de CNC','2.000,00','Amauri Pacheco Borba','Filho','','26','Masculino','Ensino Médio Completo','','','Renata Pacheco Borba','Filha','','23','Feminino','Ensino Médio completo','Telemark','780,00','Elaine da Cunha Pacheco','Mãe','','75','Feminino','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','Operador de CNC','Sim','','','Auxilio doença da Mãe','700,00','Não','','Não','','','Não','','Não','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Madeira',5,'Precárias','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-12 19:49:42','2015-01-12 19:50:56','Amigos','Outros',''),(25,'Elis Regina Patricio dos Santos','1970-04-08','','1041713015','Rua Bruno Link','226','Parque do Nolli','51-98163511','',NULL,5,2,36,'Tapes','','Manhã','Não','A pé','Não','Sim','Sim','','Carlos Santos','51-96990083','','Esposo','CRAS','','Elis Regina Patricio dos Santos','Própria','1041713015','43','Feminino','Ensino Médio Completo','Aposentada','1300,00','Carlos Santos','esposo','','42','Masculino','E. M. completo','Autonomo','3000,00','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Trabalho Eventual','barbeiro','Não','','','','','Sim','','Sim','','','Sim','','Não','Sim','perfume, tempero...','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',8,'Ótima','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-12 19:53:12','2015-01-12 19:53:12','Amigos','Outros',''),(26,'Luisa Pires Czeczelski','2003-05-15','','1116810365','Rua Tio Nico','345','Bom Fim Novo','51-30553609','',NULL,2,1,1,'Porto Alegre','7 ano','Tarde','Não','A pé','Não','Sim','Não','','Simone Pires Czeczelski','51-81805783','51-34010028','Mãe','Outros','','Simone Pires Czeczelski','Mãe','','42','Feminino','Superior Completo','Pedagoga/ Presta consultorias','','Luis Carlos Czeczelski da Silveira','Pai','','39','Masculino','Superior Completo','Engenheiro de Produção','','Felipe Pires Czeczelski','Irmão','','3','Masculino','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','Engenheiro de Produção','Não','','','','','Não','','Não','','','Não','','Não','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',6,'Ótima','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-12 20:12:00','2015-01-12 20:12:00','Amigos','Outros',''),(27,'Anny De Avila Vicente','2008-02-17','','098046015520081000701180059584','João Salazar','629','Bom Fim Novo','51-98350477','',NULL,2,1,1,'Guaíba','2º ano','Tarde','Não','A pé','Não','Sim','Não','','Bruna de Avila Claro Trescastro','51-98350477','51-98839422','Mãe  avó','CRAS','','Bruna de Avila Claro Trescastro','mãe','7106386035','26','Feminino','6°ano','','','Rodrigo Coelho Broeges','padrasto','','31','Masculino','1ºano médio','auxiliar serviços gerais','800','Bryan de Avila Vicente','irmão','','10','Masculino','5° serie','','','Anthony de Avila Borges','imão','','1','Masculino','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','auxiliar de serviços gerais','Sim','182,00','','','','Não','','Não','','','Não','','Não','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Alugada','450,00','Madeira',5,'Razoável','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-12 20:14:38','2015-01-12 20:24:04','Amigos','Outros',''),(29,'Adriano Barbosa Machado','1994-05-11','','4115713077','Rua Igrejinha','186','São Francisco','51-97189153','',NULL,1,1,36,'Guaíba','','Manhã','Sim','A pé','Não','Sim','Não','','Ana Maria Barbosa Machado e Adriana Barbosa Machado','51-95181207','51-97918133','Mãe e Irmã','Outros','','Ana Maria Barbosa Machado','Mãe','','60','Feminino','fundamental incompleto','dona de casa','','Adriana Barbosa Machado','Cônjuge','','37','Feminino','Ensino médio completo','auxiliar de produção','788,00','Camila Barbosa Alves','Cônjuge','','8','Feminino','cursando fundamental','','','Brian Barbosa Alves','Cônjuge','','11','Masculino','cursando fundamental','','','Kevin Barbosa Alves','Cônjuge','','12','Masculino','cursando fundamental','','','Douglas da Silva Machado','Cônjuge','','11','Masculino','cursando fundamental','','','Wesleyn da Silva Machado','Cônjuge','','13','Masculino','cursando','','','','Cônjuge','','','','','','','','Cônjuge','','','','','','','','Cônjuge','','','','','','','','Cônjuge','','','','','','','','Cônjuge','','','','','','','','Cônjuge','','','','','','','','Cônjuge','','','','','','','Desempregado','','Sim','','','dona de casa ','','Não','','Não','','','Não','','Não','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',10,'Ótima','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-12 20:32:44','2015-02-08 10:51:24','Amigos','Outros',''),(30,'Joselaine dos Santos Quadros ','1996-08-12','','7094816341','Travessa 5','81','Nova Guaíba 2','51-98173786','',NULL,2,1,2,'Guaíba ','1 Ano do ensino médio','Noite','Sim','Onibus','Não','Sim','Não','','Vitor Mateus Rodrigues Machado','51-95067077','','Marido','Outros','','Vitor Mateus Rodrigues Machado  ','Marido','','20','Masculino','1 ano incopleto','auxiliar de produção','1.038,00','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','','Não','','','','','Não','','Não','','','Não','','Não','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Cedida','','Alvenaria',5,'Boa','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-12 20:41:05','2015-01-30 18:51:00','Amigos','Outros',''),(31,'Rodrigo Feijo Jardim','2001-05-09','','84946741020','Rua x5','676','Morada da Colina','51-98578043','',NULL,1,1,19,'Guaíba','9°ANO','Manhã','Não','A pé','Não','Sim','Não','','Anajara Cristina Feijo Jardim','51-80427821','51-99156542','Mãe e Pai','Outros','','Anajara Cristina Feijo Jardim','Mãe','565.455.330/53','40','Feminino','Ensino Médio completo','','','Edson Feijo Jardim','Pai','','40','Masculino','Ensino Médio completo','Militar','','Luis Gustavo Feijo Jardim','imão','','20','Masculino','Ensino Médio incompleto','Militar','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','Militar','Não','','','','','Não','','Não','','','Sim','','Não','Sim','Corante','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',7,'Boa','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-12 20:51:11','2015-01-13 02:41:29','Amigos','Outros',''),(32,'Janaina Lindermann Doria ','1976-10-07','','7066423414','Rua João Longua ','518','Bom Fim Novo','51-97484627','12586841567',NULL,2,1,36,'Porto Alegre','','Manhã','Não','A pé','Não','Sim','Sim','','Laura Lindermann Doria','51-34912003','','Mãe','Outros','','Patrik Doria Custódio','Filho','9127555556','9','Masculino','Ensino Fundamental incompleto','Estudante','','Murilo Doria Custódio','Filho','','3','Masculino','','','','José Edmar Freitas Custódio','Marido','','43','Masculino','Ensino Fundamental incompleto','Motorista','1.800,00','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','Motorista','Não','','','','','Não','','Não','','','Não','','Não','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Mista',5,'Ótima','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-12 20:52:43','2015-01-12 21:02:56','Amigos','Outros',''),(33,'Almeli Alvarenga Peixoto','2009-06-29','','1016696369','Rua José Curto','20','Bom Fim Novo','51-97546788','aposentada',NULL,2,1,36,'Camaquã','','Manhã','Não','A pé','Não','Sim','Não','','Neli da Silva Peixoto','51-98336881','','cunhado','Outros','','Nildo Alvarenga peixoto','irmão','','67','Masculino','Analfabeto','Funcinário Público','788,00','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Aposentado','','Não','','','','','Não','','Não','','','Não','','Não','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',4,'Ótima','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-12 21:03:19','2015-01-13 02:05:04','Amigos','Outros',''),(35,'Evelin Rafaela Pereira Lucas','2001-07-14','','1124009679','Avenida João Salazar','575','Bom Fim','51-95024585','',NULL,2,1,1,'Guaíba','8 ano','Manhã','Não','A pé','Não','Sim','Não','','Marivane Azeredo Pereira','51-95024585','','Mãe','Outros','','Marivane Azeredo Pereira','Mãe','','32','Feminino','Ensino Fundamental incompleto','Do lar','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Desempregado','','Sim','','','Pensão','780,00','Não','','Não','','','Não','','Não','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',6,'Razoável','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-12 21:32:55','2015-01-13 02:02:29','Amigos','Outros',''),(36,'Celia Oni Dornelles Marques ','0000-00-00','','2052574841','Rua Seu Onofre','220','Bom Fim Novo','51-34032124','',NULL,2,1,36,'Camaquã','','Manhã','Não','A pé','Não','Sim','Não','','Luciane Ribeiro Marques Silva','51-98540258','','Filha','Outros','','Alex Bruno Marques ','Filho','','18','Masculino','Ensino Médio Completo','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Aposentado','','Sim','','','Pensionista','2000,00','Não','','Não','','','Não','','Não','Sim','Amoxicilina','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',6,'Boa','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-12 22:08:11','2015-01-23 21:13:11','Amigos','Outros',''),(37,'Juliana Lopes Ourique','1999-11-11','','1125842201','Rua 13','1289','Pedras Brancas','51-95568657','',NULL,2,1,24,'Guaíba','1 Ano do ensino médio','Manhã','Sim','Onibus','Não','Sim','Não','','Elizabeth Lopes Martins','51-95789801','','Mãe','Outros','','Elizabeth Lopes Martins','Mãe','2066418738','53','Feminino','Ensino Fundamental incompleto','Autonoma','1.200,00','Luísa Lopes Ourique','Irmã','3125842363','12','Feminino','Ensino Fundamental incomlpeto','Estudante','','Gerson Luis Ourique','Pai','','50','Masculino','Ensino Fundamental incompleto','Motorista','1.700,00','Sabrina Lopes Ourique','Irmã','','24','Feminino','Ensino Médio completo','Funcionária Pública','1.900,00','Fernanda Lopes Ourique','Irmã','','18','Feminino','Magistério','Professora','1.200,00','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','Motorista','Não','','','','','Não','','Não','','','Não','','Não','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',6,'Ótima','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-12 22:22:31','2015-01-13 01:55:30','Amigos','Outros',''),(38,'Luísa Lopes Ourique','2002-07-08','','3125842363','Rua 13','1289','Pedras Brancas','51-96299857','',NULL,2,1,10,'Guaíba','7ano','Tarde','Não','Onibus','Não','Sim','Não','','Elizabeth Lopes Martins','51-95789801','','Mãe','Outros','','Elizabeth Lopes Martins','Mãe','2066418738','53','Feminino','Ensino Fundamental incompleto','Autonoma','1.200,00','Juliana Lopes Ourique','Irmã','3125842363','15','Feminino','Ensino Médio Incompleto','Estudante','','Gerson Luis Ourique','Pai','','50','Masculino','Ensino Fundamental incompleto','Motorista','1.700,00','Sabrina Lopes Ourique','Irmã','','24','Feminino','Ensino Médio completo','Funcionária Pública','1.900,00','Fernanda Lopes Ourique','Irmã','','18','Feminino','Magistério','Professora','1.200,00','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','Motorista','Não','','','','','Não','','Não','','','Não','','Não','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',6,'Ótima','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-12 22:22:41','2015-01-13 01:53:10','Amigos','Outros',''),(39,'Érika Scheltz Brites  ','2001-07-04','','49.321','Rua travessa A1','45','IP','51-84545642','',NULL,2,1,24,'Guaíba','9°ano ','Manhã','Não','Outros','Não','Sim','Não','','Andre Brites','51-84545642','51-93677701','Pai','Outros','','Andre Brites','pai','9074679391','33','Masculino','ensino médio','vendedor','1000,00','Patricia Peres','Mãe','01273603010','30','Feminino','ensino médio','auxiliar administrativo','900,00','logan Brites','Irmão','','2 ano','Masculino','','','','Eloah Brites','Irma','','8 m~eses','Feminino','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','vendedor','Não','','','','','Não','','Não','','','Sim','','Não','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Madeira',4,'Boa','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-13 18:42:06','2015-01-13 18:42:06','Amigos','Outros',''),(40,'Evelyn Rosa da Silva','2012-02-27','','6121591736','Rua B','88','Nova guaiba','51-96656995','',NULL,2,1,1,'Guaíba','6° ano','Manhã','Não','A pé','Não','Sim','Não','','Janaina Luccas de Rosa, Claiton Silva da Silva','51-96656995','51-95525918','Mãe e Pai','Outros','','Janaina Luccas de Rosa','Mãe','112\'325607','29','Feminino','eja','dona de casa','','Marcos Roberto da Silva','Padrasto','','35','Masculino','8°ano','pedreiro','788,00','Andrei Lopes da Silva','Irmão','','5 ano','Masculino','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','pedreiro','Não','','','','788','Não','','Não','','','Não','','Não','Não','','Não','','','Não','Não','Mental','Não','Não','Sim','','Própria','','Madeira',5,'Precárias','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-13 19:04:58','2015-01-13 19:04:58','Amigos','Outros',''),(41,'Janaina Luccas da Rosa','2012-01-25','','1121325607','Rua B','88','Nova guaiba','51-96656995','',NULL,2,1,1,'Guaíba','6° ano','Manhã','Não','A pé','Não','Sim','Não','','Janaina Luccas de Rosa, Claiton Silva da Silva','51-96656995','51-95525918','Mãe e Pai','Outros','','Evelyn Rosa da Silva','filha','6121591736','11 ano','Feminino','6° ano','','','Marcos Roberto da Silva','marido','','35','Masculino','8°ano','pedreiro','788,00','Andrei Lopes da Silva','filho','','5 ano','Masculino','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','pedreiro','Não','','','','788','Não','','Não','','','Não','','Não','Sim','musquito','Não','','','Não','Não','Mental','Não','Não','Sim','','Própria','','Madeira',5,'Precárias','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-13 19:05:05','2015-01-13 19:13:20','Amigos','Outros',''),(42,'Ana Paula Subtil','1976-06-22','','1052572821','rua borge de medeiros ','320','loteamento neiwa','51-99417666','',NULL,2,1,36,'',' ','Manhã','Não','Outros','Não','Sim','Não','','Rozaria Maria Da Silva','51-97777825','51-98649452','Mãe','Outros','','Roberto Carlos Subtil Da silveira','Marido','','38','Masculino','Ensino Médio Completo','policial militar','2,000','Douglas Subtil Silveira','Filho','','15','Masculino','2° ano','','','Arthur Subtil Silveira','Filho','','6','Masculino','1 ano','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','bombeiro','Não','','','','','Sim','','Sim','depreção','Marido','Sim','Jéferson ','Sim','Não','','Não','','','Sim','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',5,'Boa','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-13 19:27:01','2015-01-13 19:52:37','Amigos','Outros',''),(43,'Shaiane Lopes da Silva','2005-04-09','','255-F','Rua B','88','Nova guaiba','51-96656995','',NULL,2,1,1,'Guaíba','5° ano','Manhã','Não','A pé','Não','Sim','Não','','Marcos Roberto Martins da Silva','51-95525918','','Pai','Outros','','Lucas','padrasto','','','Masculino','','','','Alessandra da Silva Lopes','mãe','','','Feminino','','dona de casa','','Andriely Lopes da Silva','irmã','','11','Feminino','6°','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Desempregado','','Sim','sim','','','180,00','Não','','Não','','','Não','','Não','Sim','chocolate','Não','','','Sim','Não','Mental','Sim','Não','Não','','Própria','','Madeira',4,'Precárias','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-13 19:37:01','2015-01-13 19:37:01','Amigos','Outros',''),(44,'Katheleen Oliveira da Silva','2003-06-11','','285-F','Rua B','86','Nova guaiba','51-','',NULL,1,1,1,'Guaíba','6° ano','Manhã','Não','A pé','Não','Sim','Não','','','','','','CRAS','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Sim','','','','','Sim','','Sim','','','Sim','','Sim','Sim','','Sim','','','Sim','Sim','Mental','Sim','Sim','Sim','','Própria','','Alvenaria',0,'Ótima','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-13 19:44:08','2015-01-13 19:44:08','Amigos','Outros',''),(45,'Melina Duarte de Oliveira','1998-08-11','','3121037562','Rua Pedro Caldas ','184','Bom Fim Novo','51-80340497','',NULL,2,1,23,'Porto Alegre','2 ano do ensino médio','Manhã','Sim','A pé','Não','Sim','Não','','Katia Leote Duarte','51-91517238','','Mãe','Outros','','Katia Leote Duarte','Mãe','','46','Feminino','Ensino Médio Completo','Vendedora','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','vende material de costrução','Não','','','','','Não','','Não','','','Não','','Não','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',5,'Boa','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-13 19:46:00','2015-01-13 19:46:00','Amigos','Outros',''),(46,'Aline Martim santos','1999-12-05','','9123993033','Rua X7','41','colina','51-34912457','',NULL,5,1,35,'Porto Alegre','2° ano médio','Manhã','Não','Onibus','Não','Sim','Não','','Nelson Francisco dos Santos','51-95170697','','Pai','CRAS','','Nelson Francisco dos Santos','Pai','1031880386','50','Masculino','ensino médio completo','funcinario publico','2.500,00','Adelia verginia Martins Santos','Mãe','','50','Feminino','ensino médio completo','dona de casa','','karine Martins Santos','Irmã','','15','Feminino','2° médio','','','Francine Martins Santos','Irmã','','17  ','Feminino','ensino médio completo','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','funcinario publico','Não','','','','2.500,00','Não','','Não','','','Não','','Sim','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',6,'Boa','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-13 20:38:29','2015-01-16 20:26:12','Amigos','Outros',''),(47,'Dionatan Rodrigues Oliveira','1991-10-14','','01368558062','Rua C','','Nova guaiba','51-81579378','',NULL,2,1,36,'Canoas','','Manhã','Não','A pé','Não','Sim','Não','','Nair de Lourdes','51-95833187','','Mãe','Outros','','','Mãe','','','Masculino','','Domestica','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','','Não','','','','','Não','','Não','','','Não','','Sim','Sim','penicilina','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',6,'Razoável','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-13 21:48:15','2015-01-13 21:48:15','Amigos','Outros',''),(48,'Vitor Mateus Rodrigues Machado','1994-11-20','','5111909718','Travessa 5','81','Nova Guaíba 2','51-95067077','',NULL,2,1,36,'Guaíba ','','Manhã','Não','A pé','Não','Sim','Não','','joselaine do Santos Quadro','51-98173786','','esposa ','Outros','','Joselaine do Santo Quadros','esposa','7094816341','18','Feminino','cursando ensino médio','Joven aprendiz','443,38','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','','Não','','','','','Não','','Não','','','Não','','Não','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Cedida','','Alvenaria',5,'Razoável','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-13 21:51:51','2015-01-30 18:49:46','Amigos','Outros',''),(49,'Giovana Nunes Garcia','2010-06-23','','0980460155 2010 100079 262 006','Rua seu onofri','164','Bom Fim Novo','51-34031684','',NULL,2,1,3,'Guaíba','jardim','Manhã','Não','A pé','Não','Sim','Não','','Neida Terezinha Nunes','51-34031684','','avó','CRAS','','Gelso Garcia ','pai','','34','Masculino','Ensino Médio Completo','vigilante','900','Viviane Nunes','Mãe','4083487381','31','Feminino','ensino médio completo','recepcionista','2000,00','Francisco Nunes Garcia','Irmão','','7 ano','Masculino','2° ano','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','cegurança','Não','','','','','Não','','Sim','síndrome do panico  ','mãe','Sim','Dr. Clarice','Sim','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',5,'Razoável','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-16 14:25:17','2015-01-19 18:46:35','Amigos','Outros',''),(50,'Eleci Morais de Oliveira','0000-00-00','','','Rua 31','100','Pedras Brancas','51-96276542','',NULL,2,1,36,'Guaíba','','Manhã','Não','Onibus','Não','Sim','Não','','Rosiane Morais de Oliveira','','','filha','Outros','','Nelsy Drosdowski de Oliveira','Marido','','60','Masculino','primario','comercial','1000,00','Matheus Morais de Oliveira','filho','','18','Masculino','6°ano','mecanico','1.500,00','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','comercio','Não','','','','','Não','','Não','','','Não','','Não','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',4,'Boa','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-16 19:00:38','2015-01-16 19:00:38','Amigos','Outros',''),(51,'Nycolly Iwanikiw da Rosa','0000-00-00','','8125876791','Rua projetada A','','Noli','51-98714149','',NULL,2,1,10,'Guaíba','7°ano','Tarde','Não','A pé','Não','Sim','Não','','Daniela Iwanikiw da Rosa, Kele Cristina Iwanikiw da Rosa','51-99021557','51-98600936','Mãe e tia dinda','Outros','','Daniela Iwanikiw da Rosa','mãe','','33','Feminino','1°médio','gerente comercial','1.300,00','Maicon da Silva Teixeira','padrasto','','28','Masculino','','analista financeiro','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','gerente comercial, analista financeiro','Não','','','','','Não','','Não','','','Não','','Sim','Não','','Não','','','Sim','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',5,'Ótima','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-16 19:25:47','2015-01-16 19:25:47','Amigos','Outros',''),(52,'Natália Taquatia Medeiros','2000-09-07','','8109874019','Rua B','192','Vera Cruz','51-82075666','',NULL,2,1,2,'Guaiba','2 ano do ensino médio','Noite','Não','Outros','Não','Sim','Não','','Jacineli Silva Taquatia','51-81793803','51-95543344','Mãe','Outros','','Jacineli Silva Taquatia','Mãe','','36','Feminino','Ensino Médio Completo','','','Marcio de Rosso Medeiros','Pai','','41','Masculino','Ensino Médio Completo','Tecnico em istrumentação','3.000.00','Marceli Taquatia Medeiros','Irmã','','16','Feminino','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','cuida das manutenção das Maquinas','Não','','','','','Não','','Não','','','Não','','Sim','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',5,'Ótima','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-16 19:37:40','2015-01-16 19:37:40','Amigos','Outros',''),(53,'Celiane Vieira Oreste','2011-09-26','','1087014278','Rua projetada D','75','Parck Noli','51-97673762','',NULL,2,2,36,'Guaíba','','Manhã','Não','A pé','Não','Sim','Não','','Jair Stracciony Oreste','51-95810585','','marido','Outros','','Jair Stracciony Oreste','Marido','','33','Masculino','cursando superior','axis tente de materiais ','2000,000','Maryna Vieira Oreste','filha','','8','Feminino','3°ano','','','Sthefany Vieira Oreste','filha','','5','Feminino','pré','','','Pedro Vieira Oreste','filho','','1 ano','Masculino','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','axis tente de materiais ','Não','','','','','Não','','Não','','','Não','','Não','Sim','dicoflenato','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Madeira',3,'Precárias','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-16 19:47:44','2015-01-16 20:06:53','Amigos','Outros',''),(54,'Maryna Vieira Orestes','2006-12-04','','58.025','Rua projetada D','75','Parck Noli','51-97673762','',NULL,2,2,3,'Guaíba','','Tarde','Não','A pé','Não','Sim','Não','','Jair Stracciony Oreste Celiane, Vieira Orestes','51-95810585','51-97673762','Pai e Mãe','Outros','','Jair Stracciony Oreste','Marido','','33','Masculino','cursando superior','axis tente de materiais ','2000,000','Celiane Vieira Orestes','Mãe','','30','Feminino','ensimédio','dona do lar','','Sthefany Vieira Orestes','imã','','5','Feminino','pré','','','Pedro Vieira Orestes','irmão','','1 ano','Masculino','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','axis tente de materiais ','Não','','','','','Não','','Não','','','Não','','Não','Sim','dicoflenato','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Madeira',3,'Precárias','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-16 19:48:41','2015-01-16 20:06:11','Amigos','Outros',''),(55,'Érika Pires de Medeiros Sutela','2007-01-10','','-58.167','Rua Henrique Levandowski projetada D','155','Parque Noli','51-95477922','',NULL,2,1,3,'Porto Alegre','3 ANO','Tarde','Não','A pé','Não','Sim','Não','','Valquiria Pires De Medeiros','51-95477922','','Mãe','Outros','','Valquiria Pires De Medeiros','Mãe','9054207148','38','Feminino','Ensino Superior incompleto','','','Analias Sutelo Da Silva ','Pai','747.983.320-20','36','Masculino','Ensino Superior incomlpeto','serralheiro','1700.00','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','corta chapa','Não','','','','','Não','','Não','','','Não','','Sim','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Mista',10,'Boa','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-16 19:50:45','2015-01-19 18:44:53','Amigos','Outros',''),(58,'Sthefany Vieira Orestes','2010-01-18','','098046015520101000780780061944','Rua projetada D','75','Parck Noli','51-97673762','',NULL,2,2,3,'Guaíba','','Tarde','Não','A pé','Não','Sim','Não','','Jair Stracciony Oreste Celiane, Vieira Orestes','51-95810585','51-97673762','Pai e Mãe','Outros','','Jair Stracciony Oreste','Marido','','33','Masculino','cursando superior','axis tente de materiais ','2000,000','Celiane Vieira Orestes','Mãe','','30','Feminino','ensimédio','dona do lar','','Maryna Vieira Orestes','irmã','','8','Feminino','3° ano','','','Pedro Vieira Orestes','irmão','','1 ano','Masculino','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','axis tente de materiais ','Não','','','','','Não','','Não','','','Não','','Não','Sim','dicoflenato','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Madeira',3,'Precárias','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-16 19:58:32','2015-01-16 20:04:51','Amigos','Outros',''),(60,'Ericka Carvalho Sonemann','2009-03-16','','098040015520091000742980060964','Rua joão zeferino','74','Bom Fim Novo','51-99258821','',NULL,2,1,1,'Guaíba','1°ano','Tarde','Não','Outros','Não','Sim','Sim','','Aline Deleski Carvalho, Alexandre Sonimann Correia','51-99258821','51-98247555','Mãe e pai','Outros','','Aline Deleski Carvalho','Mãe','006.262.420-29','33','Feminino','2°completo','dona do lar','',' Alexandre Sonimann Correia','Pai','737.419.300-91','38','Masculino','1°incompleto','motorista de cagas perigosas','2000,00','Erick Carvalho Sonimann Correia','Irmão','098046011552005100058286005615297','10','Masculino','4°ano','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','motorista de cagas perigosas','Não','','','','','Não','','Não','','','Não','','Sim','Sim','','Não','','','Não','Não','Mental','Não','Não','Sim','','Própria','','Alvenaria',4,'Boa','Regularizada','Não canalizada','Sim','0000-00-00','','2015-01-19 21:09:37','2015-01-19 21:09:37','Amigos','Outros',''),(61,'Nadine Bortolotti de Freitass','1992-08-26','','3117780613','Rua Pedro Caldas ','','Bom Fim Novo','97055040','',NULL,1,1,1,'Guaiba','8°ano','Manhã','Não','A pé','Não','Sim','Não','','Zuleica Bortolotti','97055040','','Mãe','Outros','','Zuleica Bortolotti','Mãe','1066427806','39','Feminino','2°completo','balconista','','Evelin Bortolotti','Irmã','','','Feminino','2 ANO','','','Vinicio Bortolotti','Irmão','','21','Feminino','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','balconista','Sim','148,00','','','','Não','','Não','','','','','Sim','Não','','Não','','','Não','Não','Mental','Não','Não','Sim','','Própria','','Alvenaria',5,'Boa','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-23 18:49:06','2015-01-23 18:49:06','Amigos','Outros',''),(62,'Sheila Silveira Kerbes','1999-08-06','','2124230984','capororoca','350','Pedras Brancas','99987318','165.83488.45-1',NULL,2,1,10,'Alvorada','1 ano do Ensino Médio','Noite','Sim','Onibus','Não','Sim','Não','','Iara Silveira Kerbes','51-98108132','34041134','Mãe','Outros','','Iara Silveira Kerbes','Mãe','2086597321','49','Feminino','Ensino Fundamental incompleto','','','Davi Silveira Kerbes','Irmã','2106973338','25','Masculino','ensino fundamenta completo','frentista','784.000','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','frentista','Sim','148,00','','','','Não','','','','','','','Sim','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Madeira',6,'Boa','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-23 20:15:40','2015-01-26 20:11:24','Amigos','Outros',''),(63,'Paula Dos Santos Correa','1982-09-16','','8071052768','Rua espirito santo','275','pack 35','31140068´93223791','',NULL,2,2,1,'Guaíba','','Manhã','Não','Outros','Não','Sim','Não','','Antonio Carlos Correia','91530707','81817796','Pai e Mãe','Outros','','Antonio Carlos Correa','Pai','','56','Masculino','1grau icopleto','reprecentante comercial','1.500.00','Maria Clair Dos Santos Correa','Mãe','','52','Feminino','1 grau icompleto','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','vendedor','Não','','','','','Não','','Não','','','','','Sim','Sim','grama,insetos...','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',6,'Boa','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-26 18:52:28','2015-01-26 18:55:56','Amigos','Outros',''),(64,'Mirella Kologeski de Deus','2006-07-20','','8121412566','Rua Campo Grande','151','Santa Rita','51-99222154','',NULL,2,1,24,'Guaíba','3°ano','Tarde','Não','Transporte Escolar','Sim','Sim','Não','','Taina Regina Kologeski deDeus, Ernesto Kolofgski','51-99222154','51-99813627','Mãe e avô','Outros','','Taina Regina Kologeski deDeus','Mãe','8066427702','44','Feminino','2°completo','Agente de atendimento ','1,300','Claudiomiro Silva de Deus','pai','','46','Masculino','2°incompleto','Preparador de maquina','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','mãe Agente de atendimento, pai Preparador de maquina','Não','','','','','Não','','Sim','Ernia na coluna','mãe e pai','Não','Dr. Roberto Samara','Sim','Sim','picada de inseto, local fechado ','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',4,'Boa','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-26 18:55:58','2015-01-26 19:07:02','Amigos','Outros',''),(66,'Taina Regina Kologeski de Deus','1970-03-07','','8066427702','Rua Campo Grande','151','Santa Rita','51-99222154','',NULL,2,1,36,'Guaíba','','Manhã','Não','Onibus','Não','Sim','Não','','Ernesto Kologeski','51-99813627','','Pai','Outros','','Mirella Kologeski de Deus','Filha','8121412566','8','Feminino','3°ano','','','Claudiomiro Silva de Deus','pai','','46','Masculino','2°incompleto','Preparador de maquina','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','mãe Agente de atendimento, pai Preparador de maquina','Não','','','','','Não','','Sim','Ernia na coluna','mãe e pai','Não','Dr. Roberto Samara','Sim','Sim','Reniti','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',4,'Boa','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-26 18:56:39','2015-01-26 19:04:53','Amigos','Outros',''),(67,'Claudia Negreiros Gerlack','1971-06-05','','5058285015','Rua C8 ','177','colina','51-80434120','',NULL,2,1,36,'Osório   ','','Manhã','Não','Onibus','Sim','Sim','Não','','João Carlos Gerlack','51-99874443','','esposo','Outros','','João Carlos Gerlack','Marido','','50','Masculino','médio completo','fizcal do meio ambiente','784,00','Vinicios Negreiros Gerlack','Filho','','24','','médio completo','tele markiting','784,00','Jesica Negreiros Gerlack','filha','','26','Feminino','médio completo','','','João Vitor Negreiros Gerlack','filho','','13','Masculino','9°ano','','','Delcia da Silva Negreiros','mãe','','74','Feminino','fundamental commpleto','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','fizcal do meio ambiente','Não','','','','','Não','','Sim','Avc','Mãe','Sim','neurologista DR.Matheus clinico geral Dr.Maria Helena ','Não','Sim','remédio buscopam','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',4,'Razoável','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-26 20:31:35','2015-01-26 20:33:50','Amigos','Outros',''),(70,'Manuela Kerbes Pereira ','2009-11-24','','A-77 folha 227 número 61.793','Rua 5 ','39','Nova Guaíba II','51-80185857','',NULL,2,2,36,'Porto Alegre','','Manhã','Não','A pé','Não','Sim','Não','','Sheila ou Mirian','51-99987318 ','51-80185857','Tia e Mãe','Outros','','Miriam Kerbes Pereira','Mãe','026.401.010-80','23','Feminino','Ensino Médio incompleto','Atendente de padaria','780,00','Pedro Vitor Kerbes Pereira','Irmão','','3','','','','','Jeferson Luis Oswaldi Pereira','Pai','','29','','Ensino Fundamental completo','Limpeza de Onibus','780,00','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','Limpeza de ônibus','Sim','148,00','','','','Não','','Não','','','','','Não','Não','','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',5,'Razoável','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-26 20:37:50','2015-01-26 20:37:50','Familiares','Outros',''),(71,'Emily Giovanna Santos','2015-01-26','','040.058.570-78','Avenida Zeferino da Boa Vista','495','Bom Fim','51-30554735','',NULL,2,1,3,'Guaíba','6 ano','Manhã','Não','A pé','Não','Sim','Sim','','Rozenilda Santos ','51-98271625','','Mãe','Outros','','Rozenilda Santos','Mãe','1075705002','37','Feminino','Ensino Médio Completo e Técnico','Técnico de Enfermagem','1.400,00','Moacir da Silva Santos','Pai','','35','Masculino','Ensino Médio Completo','Auxiliar de Produção','950.00','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','Auxiliar de Produção','Não','','','','','Não','','','','','','','Não','Sim','Picada de formiga','Não','','','Não','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',4,'Razoável','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-26 22:05:37','2015-01-26 22:05:37','Amigos','Outros',''),(72,'Dionata Pereira de Azambuja ','2003-12-12','','9117782913','Rua c','175','Nova Guaiba','51-97414135','',NULL,1,2,1,'Guaiba','6°ano','Manhã','Não','A pé','Não','Sim','Não','','Eloisa da Silva Pereira','51-97414135','','mãe','Outros','','Eloisa da Silva Pereira','mãe','969.077.620.72 RG:8083486939','34','Feminino','Segundo Grau ','terapeuta ','784,00','Carlos Eduardo Pereira Azambuja','irmão','','16','Masculino','9°ano','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','terapeuta ','Não','','','','','Sim','','Não','','','','','Sim','Não','','Não','','','Sim','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',4,'Razoável','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-26 22:06:16','2015-01-26 22:06:16','Escola','Outros',''),(73,'Elisabete Klein Pires','0000-00-00','','1056156126','Avenida Zeferino da Boa Vista','354','Bom Fim Novo','51-34033331','',NULL,2,1,1,'Guaíba','','Manhã','Não','A pé','Não','Sim','Não','','Claudete Rebeiro','51-34021540','','Irmã','Outros','','Roberto Escacel Marques','Marido','','53','Masculino','Ensino Médio incompleto','Motorista','1000.00','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','Motorista','Sim','','','Pensão','700,00','Não','','Não','','','','','Sim','Não','','Sim','','','Sim','Não','Mental','Não','Não','Não','','Própria','','Alvenaria',6,'Boa','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-30 19:11:47','2015-01-30 19:46:10','Amigos','Outros',''),(74,'Vanessa Klein Marques','1990-12-10','','1100489151','AV. zeferino boa vista','354','Bom fim Novo','51-95792588','',NULL,2,1,36,'Guaíba','','Manhã','Não','A pé','Não','Sim','Sim','','Elisabete Klein, Douglas Souza ','51-98195721','51-96039533','mãe e namorado','CRAS','','Douglas Souz','Namorado','','23','Masculino','fundamental completo','mertalurgico','900,00','Nicole Marques Antunes','filha','','07','Feminino','3°ano','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','mertalugico','Não','','','','','Não','','Não','','','','','Não','Não','','Não','','','Sim','Não','Mental','Não','Não','Não','','Própria','','Madeira',4,'Razoável','Regularizada','Canalizada','Sim','0000-00-00','','2015-01-30 19:39:36','2015-01-30 19:39:36','Amigos','Outros',''),(75,'Diogo da Silva Pereira','1983-07-24','','3067453989','Rua natal','482','Santa Rita','96064369','',NULL,1,1,0,'Porto Alegre','','Manhã','Não','Bicicleta','Não','Não','Sim','','MARIA LÚCIA J. DA SILVA','91230657','','MAMÃE','CRAS','','JULIANE TAVARES PRESTES','ESPOSA','6091095734','30 ANOS','Feminino','2ºGRAU COMPLETO','ATENDENTE DE RESTAURANTE','20$','EMILLY VITÓRIA P. PEREIRA','FILHA','','11 ANOS','Outros','7º ANO','ESTUDANTE','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','MONITOR DE PROJETOS SOCIAIS','Não','','','','','Sim','','','','','','','Não','Não','','Não','','','Sim','Não','Mental','Sim','Sim','Sim','','Cedida','','Alvenaria',0,'Razoável','Regularizada','Canalizada','Sim','0000-00-00','','2015-02-06 14:23:29','2015-02-06 14:23:29','Amigos','Masculino',''),(78,'Maria Luiza da Siva Oliveira','1976-11-19','','7066423554','Rua Cidreira','45','São Francisco','51-30553588','',NULL,1,1,0,'Catuípe','','Noite','Não','Outros','Não','Sim','Sim','','José Henrique','51-81913879','','conjuge','Outros','','Matheus Oliveira Marques','filho','','','Masculino','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Empregado','serv.gerais','Sim','n','','PROUNI','integral','Não','','Não','','','','','Não','Não','','Não','','','Sim','Não','Mental','Sim','Sim','Não','','Própria','','Alvenaria',0,'Razoável','Regularizada','Canalizada','Sim','0000-00-00','Oficina de Dança Gaúcha','2015-02-06 14:47:13','2015-02-06 16:03:48','Amigos','Feminino','');
/*!40000 ALTER TABLE `alunos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `atendimentos`
--

DROP TABLE IF EXISTS `atendimentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `atendimentos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `aluno_id` int(11) NOT NULL,
  `data` date NOT NULL,
  `horario` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `incidente` text COLLATE utf8_unicode_ci NOT NULL,
  `orientacoes` text COLLATE utf8_unicode_ci NOT NULL,
  `encaminhamentos` text COLLATE utf8_unicode_ci NOT NULL,
  `responsavel_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atendimentos`
--

LOCK TABLES `atendimentos` WRITE;
/*!40000 ALTER TABLE `atendimentos` DISABLE KEYS */;
INSERT INTO `atendimentos` VALUES (1,7,'2015-02-12','10h','Incidente texto','Orientações texto','Encaminhamentos teste',1,'2015-02-04 13:51:39','2015-02-04 13:51:39');
/*!40000 ALTER TABLE `atendimentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `colaboradores`
--

DROP TABLE IF EXISTS `colaboradores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colaboradores` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `data_nascimento` date DEFAULT NULL,
  `naturalidade` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `cidade` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `bairro` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `numero` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `rg` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `cpf` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `funcao_id` int(10) unsigned NOT NULL,
  `admissao` date DEFAULT NULL,
  `demissao` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colaboradores`
--

LOCK TABLES `colaboradores` WRITE;
/*!40000 ALTER TABLE `colaboradores` DISABLE KEYS */;
INSERT INTO `colaboradores` VALUES (1,'Flavio',NULL,'','','RS','','','','','','','',2,NULL,NULL,'2014-12-08 16:57:19','2014-12-08 19:50:50'),(3,'Eliane Dal Bello','1980-10-30','Antônio Prado','5185211085','RS','Guaíba','Bom Fim Novo','Rua Pedro Caldas ','367','7056570241','','elianedalbelloadm@gmail.com',5,'2002-02-01',NULL,'2014-12-08 21:21:01','2014-12-08 21:21:46'),(4,'Maicon Junior dos Santos Macedo','1986-09-13','Dois Irmãos','5196139823','RS','Guaíba','Colina','Rua X 1','1881','2095591381','018.744.100','gilprestes@gmail.com',4,'2012-03-20','0000-00-00','2014-12-08 21:40:56','2015-01-13 16:22:06'),(5,'Carla Rodrigues de Souza','1980-07-26','Barão do Triunfo','5180260780','RS','Guaíba','Columbia City','Rua São Jeronimo ','197','2090568102','','',6,'2014-05-02',NULL,'2014-12-08 21:46:52','2014-12-08 21:46:52'),(6,'Gabriel Ribeiro dos Santos','1998-03-24','Eldorado do Sul','5197907497','RS','Guaíba','Vera Cruz','Rua 4','200','9118310854','','gabrielribeirodossantos2014@gmail.com',7,'2014-02-10',NULL,'2014-12-08 22:24:42','2014-12-08 22:26:26'),(7,'Adão Clóvis Radde','1954-04-15','Guaíba','5134911089','RS','Guaíba','Bom Fim Novo','Rua Pedro Caldas ','275','4018030298','29890730006','',24,'2011-06-06',NULL,'2014-12-12 20:14:59','2014-12-12 20:14:59'),(8,'Dilce Teresinha de Souza Radde','1957-07-07','São Jeronimo','5134911089','RS','Guaíba','Bom Fim Novo','Rua Pedro Caldas ','275','4073591804','31917542020','',31,'2011-03-03',NULL,'2014-12-12 20:17:05','2014-12-12 20:17:05'),(9,'Liliana Machado','1986-01-16','Porto Alegre','5199498481','RS','Guaíba','Bom Fim Novo','Rua Pedro Caldas ','154','9073714991','01380593069','liliana.letras@gmail.com',33,'2014-03-01',NULL,'2014-12-12 20:25:11','2014-12-12 20:26:23'),(10,'Avelino Laux','1945-09-30','Barra do Ribeiro','5134913445','RS','Guaíba','Bom Fim Novo','Rua José Curto','80','9013030755','25521659072','',32,'2011-03-05',NULL,'2014-12-12 20:28:28','2014-12-12 20:28:28'),(11,'Erozino Dias Medeiros','1944-01-10','Camaquã','5134912005','RS','Guaíba','Bom Fim Novo','Av. Seu Lereno','45','4022332706','11701307049','',34,'2011-05-30',NULL,'2014-12-12 20:31:28','2014-12-12 20:31:28'),(12,'Antônio Carlos Pereira Machado','1975-02-16','Porto Alegre','','RS','Guaíba','Coronel Nassuca','R. Inácio de Quadros','285','9053169356','65766873034','',16,'2011-08-30',NULL,'2014-12-12 20:38:52','2014-12-12 20:38:52'),(13,'Elza Laux','1948-03-30','Barra do Ribeiro','5134913445','RS','Guaíba','Bom Fim Novo','Rua José Curto','80','5042980499','','',6,'2011-03-05',NULL,'2014-12-12 20:40:26','2014-12-12 20:40:26'),(14,'Edimilson Radde da Costa','1964-05-05','Guaíba','5196517501','RS','Guaíba','Centro','Rua José Stortti','477','1564574','37613618072','',31,'2013-08-30',NULL,'2014-12-12 20:43:43','2014-12-12 20:43:43'),(15,'Loreni Silva da Conceição','0000-00-00','Encruzilhada do Sul','51-98631415','RS','Guaíba','Bom Fim Novo','Rua Tio Inica','241','5042965441','','',9,'2010-08-20','0000-00-00','2014-12-12 20:49:55','2014-12-22 23:12:20'),(16,'Jaqueline dos Santos Menezes','1982-11-18','Rosário do Sul','51-34031250 / 51-85080490 / 51','RS','Guaíba','Jardim dos Lagos','Rua Noel Guarani','421','6080691147','003.758.380','jaque.santos.menezes@hotmail.com',29,'2014-09-01','0000-00-00','2014-12-22 23:15:08','2015-01-15 18:15:48'),(17,'Marlene Duarte Medeiros','0000-00-00','Camaquã','51-34912005','RS','Guaíba','Bom Fim Novo','Av. Seu Lereno','45','6052571509','','',11,'2014-12-22','0000-00-00','2014-12-22 23:25:49','2014-12-22 23:25:49'),(18,'Sheila Renata Morais de Souza','1986-06-27','Guaíba/RS','51-96841391','RS','Guaíba','Columbia City','Rua Osvaldo Rodrigues Lemos','211','7090020012','01117057038','sheila@gavassoebianquini.com.br',31,'2013-09-05','0000-00-00','2014-12-22 23:29:07','2015-01-19 18:21:41'),(19,'Marines da Silva Oliveira','1978-08-20','Ijuí','','RS','Guaíba','São Francisco','Av. Encruzilhada do Sul','560','2073593697','','',12,'2012-04-02','0000-00-00','2014-12-22 23:34:10','2014-12-22 23:34:10'),(20,'Alex Bruno Ribeiro Marques','1997-02-16','Guaíba','51- 96823335- 96476577','RS','Guaíba','Bom Fim Novo','Rua Seu Onofre','220','2111129413','','',23,'2011-12-23','0000-00-00','2014-12-22 23:36:13','2014-12-22 23:36:13'),(21,'Sandro Costa Jardim','1972-10-16','Guaíba','','RS','Guaíba','Vila Nova','Rua Padre Cacique','124','2053361982','','',32,'2011-03-16','0000-00-00','2014-12-22 23:38:21','2014-12-22 23:38:21'),(22,'Veni Biazebeti Pires','0000-00-00','Guaíba','51-34912064','RS','Guaíba','Bom Fim Novo','Av. Zeferino da Boa Vista','429','3056153673','','',11,'2011-09-11','0000-00-00','2014-12-22 23:41:10','2014-12-22 23:41:10'),(24,'Luis Henrique Dantas da Conceição','1973-07-09','Porto Alegre','51-083640404','RS','Guaíba','Colina','Gelso Lopes','131','1061004519','','',16,'2008-03-03','0000-00-00','2014-12-22 23:54:09','2014-12-22 23:54:09'),(25,'Teresinha Soares Toledo','0000-00-00','General Camera','51-33212755','RS','Guaíba','São Francisco','Rua Carazinho','100','6062037145','','',22,'2011-10-18','0000-00-00','2014-12-22 23:56:10','2014-12-22 23:56:10'),(26,'Zelina da Silva','0000-00-00','Rio Pardo','','RS','Guaíba','Nova Guaíba','Rua 27','115','2049915784','','',22,'2010-09-01','0000-00-00','2014-12-22 23:58:02','2014-12-22 23:58:02'),(27,'Ilva Antônia Rodrigues de Souza','0000-00-00','Barra do Ribeiro','51-91697159','RS','Guaíba','São Jorge','Rua Mario Alves Py','41','5027093169','','',21,'2009-03-30','0000-00-00','2014-12-23 00:00:38','2014-12-23 00:00:38'),(28,'Sonia Borges da Silva','0000-00-00','Guaíba','','RS','Guaíba','São Jorge','Rua Carlos Motta','261','1074862564','','',21,'2007-06-07','0000-00-00','2014-12-23 00:03:25','2014-12-23 00:03:25'),(29,'Sônia Maria Gomes Alves','1974-06-01','Guaíba','51-97027224','RS','Guaíba','São Jorge','Rua Morro Santana','40','2066418399','','',21,'2007-06-07','0000-00-00','2014-12-23 00:05:43','2014-12-23 00:05:43'),(30,'Angela Pulla Jardim','1997-11-26','Guaíba','51-97620030','RS','Guaíba','Vila Nova','Rua Padre Cacique','124','1114264573','','',31,'2014-04-01','0000-00-00','2014-12-23 00:07:33','2014-12-23 00:07:33'),(31,'Zelma Tereza Rodrigues da Silva','0000-00-00','Camaquã','51-99113904','RS','Guaíba','Bom Fim Novo','Rua José Curto','58','5050360253','','',6,'2013-04-02','0000-00-00','2014-12-23 13:54:03','2014-12-23 13:54:03'),(32,'Marizete Sadoski Duarte','0000-00-00','Tapes','51- 34014003','RS','Guaíba','Bom Fim Novo','Rua Pedro Caldas ','327','1056153826','','',31,'2012-09-03','0000-00-00','2014-12-23 14:11:21','2014-12-23 14:11:21'),(33,'Joselaine dos Santos Quadros','1996-08-12','Guaíba','51-98173786','RS','Guaíba','Vera Cruz','Av. A','287','7094816341','','joselaine.quadros@outlook.com',7,'2014-03-10','0000-00-00','2014-12-23 14:14:34','2015-01-30 18:52:29'),(34,'Zilma Barão Petitembert','0000-00-00','Camaquã','51-96665224','RS','Guaíba','Columbia City','Av. Urias Lugon','325','2048039826','','',6,'2014-05-26','0000-00-00','2014-12-23 14:16:42','2014-12-23 14:16:42'),(35,'Doralcina Maria dos Santos da Silva','0000-00-00','Tapes','','RS','Guaíba','São Francisco','Av. Encruzilhada do Sul','679','3042965123','','',6,'2012-03-01','0000-00-00','2014-12-23 14:19:05','2014-12-23 14:19:05'),(36,'Jonas Paiva Flores','1980-06-17','Porto Alegre','51-30555871','RS','Guaíba','Cohab','Rua D 2','165','4063263158','','jonasfgt@gmail.com',2,'2010-06-10','0000-00-00','2014-12-23 14:28:20','2014-12-23 14:28:20'),(37,'Maria Luiza da Silva Oliveira','1976-11-19','Catuipe','51-98190036','RS','Guaíba','São Francisco','Rua Cidreira','45','7066423554','','',34,'2011-11-24','0000-00-00','2014-12-23 14:33:42','2014-12-23 14:33:42'),(38,'Nuria Helena Goulart Neumann','1983-01-25','Guaíba','51-99366052','RS','Guaíba','Columbia City','São Jerônimo','102','4078559228','','',1,'2013-08-05','0000-00-00','2014-12-23 14:36:40','2014-12-23 14:37:00'),(39,'Adriano Barbosa Machado ','1994-05-11','Guaíba','(51)97189153','RS','Guaíba','São Francisco','Rua Igrejinha ','186','4115713077','009.206.120','adrianomachado157@gmail.com',7,'2014-03-10','0000-00-00','2015-01-03 02:15:00','2015-01-03 02:19:01'),(40,'Roselaine da Silva Alves','1985-01-11','Porto Alegre','51-98724965','RS','Guaíba','Nova Guaiba','Travessa 5','101','1092842011','010.032.490','rosesd.alves@gmail.com',3,'2002-03-01','0000-00-00','2015-01-12 14:40:18','2015-01-12 14:40:18'),(41,'Marinês Oliveira ','1978-08-20','Ijuí','51-97219709 ou 51-97111385','RS','Guaíba','São Francisco','Avenida Encruzilhada do Sul','560','2073593697','12345678915','marinesoliveira130505@gmail.com',30,'2011-12-01','0000-00-00','2015-01-12 14:54:05','2015-01-12 21:51:05'),(42,'Diogo da Silva Pereira','1984-07-24','Guaíba','51-96064369','RS','Guaíba','Santa Rita','Rua Natal','482','3067453989','003 750 590','diogolc.pereira@gmail.com',2,'0000-00-00','0000-00-00','2015-01-13 16:17:16','2015-01-19 18:18:41'),(45,'Airton da Silva Martinicorena','1987-09-16','Guaíba','(51)93188116','RS','Guaíba','Parque 35','AV João de Arauújo Lessa','479','1093653317','8330642034','airtonmartin@yahoo.com.br',14,'2014-08-19','0000-00-00','2015-01-15 17:14:56','2015-01-16 20:44:36'),(46,'ANTÔNIO CARLOS PEREIRA MACHADO','1975-02-16','PORTO ALEGRE',' (051) 8200-2194','RS',' GUAÍBA','JARDIM DOS LAGOS','LEOPOLDO RACIER','116',' 905316-9356',' 9053169356','centro.cultural.capoarte@hotmail.com',16,'2007-03-01','0000-00-00','2015-01-17 01:37:27','2015-01-17 01:37:27'),(47,'Daiana Fülber','1985-08-28','Sapiranga','(51)9739-1800','RS','Porto Alegre','Jardim Botânico','Rua Veríssimo Rosa','798, ap 40',' 9090172587','00648207005','daianafulber@gmail.com',17,'2014-03-10','0000-00-00','2015-01-30 06:58:16','2015-01-30 20:03:03'),(48,'Elton Bozzetto','0000-00-00','Ponte Serrada - SC','51-96665592','RS','Porto Alegre','Santo Antônio','Rua São Francisco de Assis','503','2069506018','55620531953','eltonbozzetto@gmail.com',36,'2013-05-02','0000-00-00','2015-02-03 15:28:02','2015-02-03 15:28:02'),(49,'Nilva Dal Bello','0000-00-00','Antônio Prado','51-92454838-(51) 8440 4567','RS','Guaíba','Bom Fim Novo','Rua Siá Alice','291','5020892922','328.872.210','nilvadb2010@gmail.com',37,'1987-03-19','0000-00-00','2015-02-03 15:43:15','2015-02-03 15:43:15'),(50,'Lenise da Silva Santos','0000-00-00','Cachoeira do Sul','51-95269013','RS','Guaíba','Columbia City','Rua Colombo dos Santos Marques','610','1077259222','82301492087','',8,'2011-06-14','0000-00-00','2015-02-03 20:44:11','2015-02-03 20:44:11'),(51,'Ângela Maria Ribas','0000-00-00','Santa Barbará do Sul','51-95603924','SC','Guaíba','Bom Fim Novo','Rua Siá Alice','291','4034413197','63516535091','angelriba2012@gmail.com',37,'2005-01-10','0000-00-00','2015-02-03 20:51:25','2015-02-03 20:51:25'),(52,'Daniel Santos','0000-00-00','Guaíba','','AC','','','','','','','',15,'0000-00-00','0000-00-00','2015-02-08 21:51:23','2015-02-08 21:51:23'),(53,'Marcia','0000-00-00','Guaíba','','AC','','','','','','','',40,'0000-00-00','0000-00-00','2015-02-08 21:59:20','2015-02-08 21:59:20'),(54,'André Coimbra Meneghello','0000-00-00','Porto Alegre','','AC','','','','','','','',17,'0000-00-00','0000-00-00','2015-02-08 22:36:35','2015-02-08 22:36:35'),(55,'Maria Fernanda Canabarro','0000-00-00','Porto Alegre','','AC','','','','','','','',17,'0000-00-00','0000-00-00','2015-02-08 22:37:34','2015-02-08 22:37:34'),(56,'Alexandre Diel','0000-00-00','Porto Alegre','','AC','','','','','','','',17,'0000-00-00','0000-00-00','2015-02-08 22:38:19','2015-02-08 22:38:19'),(57,'Silvio Sandro de Souza','0000-00-00','Porto Alegre','','AC','','','','','','','',17,'0000-00-00','0000-00-00','2015-02-08 22:39:32','2015-02-08 22:39:32');
/*!40000 ALTER TABLE `colaboradores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `escolas`
--

DROP TABLE IF EXISTS `escolas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `escolas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `escolas`
--

LOCK TABLES `escolas` WRITE;
/*!40000 ALTER TABLE `escolas` DISABLE KEYS */;
INSERT INTO `escolas` VALUES (1,'Escola Municipal Senador Teotonio Brandão Vilela','Municipal','2014-12-29 23:03:09','2014-12-29 23:03:09'),(2,'Escola Otero Paiva','Estadual','2014-12-29 23:04:52','2014-12-29 23:04:52'),(3,'Escola Municipal  Arlindo Stringhini','Municipal','2014-12-29 23:05:16','2014-12-29 23:05:16'),(4,'Escola Municipal Zilá Paiva Rodrigues','Municipal','2015-01-08 17:25:48','2015-01-08 17:25:48'),(5,'Escola Municipal Amadeu Bolognesi','Municipal','2015-01-10 17:19:57','2015-01-10 17:19:57'),(6,'Escola Municipal Anita Garibaldi','Municipal','2015-01-10 17:20:43','2015-01-10 17:20:43'),(7,'Escola Municipal Breno Guimarães','Municipal','2015-01-10 17:21:52','2015-01-10 17:21:52'),(8,'Escola Municipal Darcy Berbigier','Municipal','2015-01-10 17:22:58','2015-01-10 17:22:58'),(9,'Escola Municipal Inácio de Quadros','Municipal','2015-01-10 17:23:49','2015-01-10 17:23:49'),(10,'Escola Municipal José Carlos Ferreira','Municipal','2015-01-10 17:24:59','2015-01-10 17:24:59'),(11,'Escola Municipal Liberato Salzano Vieira da Cunha','Municipal','2015-01-10 17:26:59','2015-01-10 17:26:59'),(12,'Escola Municipal Máximo Laviaguerre','Municipal','2015-01-10 17:27:52','2015-01-10 17:27:52'),(13,'Escola Municipal Rio Grande do Sul','Municipal','2015-01-10 17:28:40','2015-01-10 17:28:40'),(14,'Escola Municipal Santa Catarina','Municipal','2015-01-10 17:29:20','2015-01-10 17:29:20'),(15,'Escola Municipal Santa Rita de Cássia','Municipal','2015-01-10 17:29:57','2015-01-10 17:29:57'),(16,'Escola Municipal São Francisco de Assis','Municipal','2015-01-10 17:30:41','2015-01-10 17:30:41'),(17,'Escola Municipal São Paulo','Municipal','2015-01-10 17:31:02','2015-01-10 17:31:02'),(18,'Escola Estadual Nestor de Moura Jardim','Estadual','2015-01-10 17:34:45','2015-01-10 17:34:45'),(19,'Escola Estadual Otaviano Manoel de Oliveira Junior','Estadual','2015-01-10 17:37:17','2015-01-10 17:37:17'),(20,'Escola Estadual Ismael Chaves Barcellos','Estadual','2015-01-10 17:38:27','2015-01-10 17:38:27'),(21,'Escola Estadual Dr Gastão Leão','Estadual','2015-01-10 17:39:12','2015-01-10 17:39:12'),(22,'Escola Estadual Augusto Meyer','Estadual','2015-01-10 17:39:45','2015-01-10 17:39:45'),(23,'Escola Estadual Cônego Scherer','Estadual','2015-01-10 17:40:36','2015-01-10 17:40:36'),(24,'Escola Estadual Gomes Jardim','Estadual','2015-01-10 17:41:16','2015-01-10 17:41:16'),(25,'Escola Estadual Ruy Coelho Gonçalves','Estadual','2015-01-10 17:42:23','2015-01-10 17:42:23'),(26,'Escola Estadual Carmem Alice Laviaguerre','Estadual','2015-01-10 17:45:07','2015-01-10 17:45:07'),(27,'Escola Estadual Albino Hachmann','Estadual','2015-01-10 17:46:36','2015-01-10 17:46:36'),(28,'Escola Estadual Cel Frederico Linck','Estadual','2015-01-10 17:48:29','2015-01-10 17:48:29'),(29,'Escola Estadual Evaristo da Veiga','Estadual','2015-01-10 17:49:55','2015-01-10 17:49:55'),(30,'Escola Estadual Itororó','Estadual','2015-01-10 17:50:36','2015-01-10 17:50:36'),(31,'Escola Estadual Nossa Senhora do Livramento','Estadual','2015-01-10 17:51:20','2015-01-10 17:51:20'),(32,'Escola Estadual Professora Agle Kehl','Estadual','2015-01-10 17:53:25','2015-01-10 17:53:25'),(33,'Escola Estadual Izaura Ibanez Paiva','Estadual','2015-01-10 17:54:17','2015-01-10 17:54:17'),(34,'Escola Estadual de Educação Profissional Dr Solon Tavares','Estadual','2015-01-10 17:56:50','2015-01-10 17:56:50'),(35,'Escola Estadual Moura e Cunha','Estadual','2015-01-12 14:35:47','2015-01-12 14:35:47'),(36,'Nenhuma','Municipal','2015-01-12 16:29:44','2015-01-12 16:29:44'),(37,'ULBRA','Particular','2015-02-06 14:31:37','2015-02-06 14:31:37'),(38,'Unopar','Particular','2015-02-06 14:31:47','2015-02-06 14:31:47'),(39,'Uniasselvi','Particular','2015-02-06 14:32:00','2015-02-07 21:48:41'),(40,'Dimensão','Particular','2015-02-06 14:32:41','2015-02-06 14:32:41'),(41,'Pensar','Particular','2015-02-06 14:32:51','2015-02-06 14:32:51'),(42,'QI FAculd','Particular','2015-02-06 14:33:29','2015-02-06 14:33:29'),(43,'Escola Santo Antonio Kids','Particular','2015-02-07 21:51:15','2015-02-07 21:51:15');
/*!40000 ALTER TABLE `escolas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado_civil`
--

DROP TABLE IF EXISTS `estado_civil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado_civil` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado_civil`
--

LOCK TABLES `estado_civil` WRITE;
/*!40000 ALTER TABLE `estado_civil` DISABLE KEYS */;
INSERT INTO `estado_civil` VALUES (1,'Solteiro','2014-12-29 22:00:37','2014-12-29 22:00:37'),(2,'Casado','2014-12-29 22:00:43','2014-12-29 22:00:43'),(3,'União estável','2014-12-29 22:00:53','2014-12-29 22:00:53'),(4,'Viúvo','2014-12-29 22:01:00','2014-12-29 22:01:00');
/*!40000 ALTER TABLE `estado_civil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estados`
--

DROP TABLE IF EXISTS `estados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estados` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `sigla` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `pais_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estados`
--

LOCK TABLES `estados` WRITE;
/*!40000 ALTER TABLE `estados` DISABLE KEYS */;
INSERT INTO `estados` VALUES (1,'Acre','AC',1,'2014-12-19 01:39:19','2014-12-19 01:39:19'),(2,'Alagoas','AL',1,'2014-12-19 01:39:19','2014-12-19 01:39:19'),(3,'Amazonas','AM',1,'2014-12-19 01:39:19','2014-12-19 01:39:19'),(4,'Amapá','AP',1,'2014-12-19 01:39:19','2014-12-19 01:39:19'),(5,'Bahia','BA',1,'2014-12-19 01:39:19','2014-12-19 01:39:19'),(6,'Ceará','CE',1,'2014-12-19 01:39:19','2014-12-19 01:39:19'),(7,'Distrito Federal','DF',1,'2014-12-19 01:39:19','2014-12-19 01:39:19'),(8,'Espírito Santo','ES',1,'2014-12-19 01:39:19','2014-12-19 01:39:19'),(9,'Goiás','GO',1,'2014-12-19 01:39:19','2014-12-19 01:39:19'),(10,'Maranhão','MA',1,'2014-12-19 01:39:19','2014-12-19 01:39:19'),(11,'Minas Gerais','MG',1,'2014-12-19 01:39:19','2014-12-19 01:39:19'),(12,'Mato Grosso do Sul','MS',1,'2014-12-19 01:39:19','2014-12-19 01:39:19'),(13,'Mato Grosso','MT',1,'2014-12-19 01:39:19','2014-12-19 01:39:19'),(14,'Pará','PA',1,'2014-12-19 01:39:19','2014-12-19 01:39:19'),(15,'Paraíba','PB',1,'2014-12-19 01:39:19','2014-12-19 01:39:19'),(16,'Pernambuco','PE',1,'2014-12-19 01:39:19','2014-12-19 01:39:19'),(17,'Piauí','PI',1,'2014-12-19 01:39:19','2014-12-19 01:39:19'),(18,'Paraná','PR',1,'2014-12-19 01:39:19','2014-12-19 01:39:19'),(19,'Rio de Janeiro','RJ',1,'2014-12-19 01:39:19','2014-12-19 01:39:19'),(20,'Rio Grande do Norte','RN',1,'2014-12-19 01:39:19','2014-12-19 01:39:19'),(21,'Rondônia','RO',1,'2014-12-19 01:39:19','2014-12-19 01:39:19'),(22,'Roraima','RR',1,'2014-12-19 01:39:19','2014-12-19 01:39:19'),(23,'Rio Grande do Sul','RS',1,'2014-12-19 01:39:19','2014-12-19 01:39:19'),(24,'Santa Catarina','SC',1,'2014-12-19 01:39:19','2014-12-19 01:39:19'),(25,'Sergipe','SE',1,'2014-12-19 01:39:19','2014-12-19 01:39:19'),(26,'São Paulo','SP',1,'2014-12-19 01:39:19','2014-12-19 01:39:19'),(27,'Tocantins','TO',1,'2014-12-19 01:39:19','2014-12-19 01:39:19');
/*!40000 ALTER TABLE `estados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `etnias`
--

DROP TABLE IF EXISTS `etnias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `etnias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `etnias`
--

LOCK TABLES `etnias` WRITE;
/*!40000 ALTER TABLE `etnias` DISABLE KEYS */;
INSERT INTO `etnias` VALUES (1,'Negro','2014-12-29 23:03:24','2014-12-29 23:03:24'),(2,'Branco','2014-12-29 23:03:34','2014-12-29 23:03:34'),(3,'Indígina','2014-12-29 23:03:49','2014-12-29 23:03:49'),(4,'Mulato','2014-12-29 23:04:00','2014-12-29 23:04:00'),(5,'Pardo','2014-12-29 23:04:22','2014-12-29 23:04:22');
/*!40000 ALTER TABLE `etnias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formas_pagamentos`
--

DROP TABLE IF EXISTS `formas_pagamentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formas_pagamentos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `forma` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formas_pagamentos`
--

LOCK TABLES `formas_pagamentos` WRITE;
/*!40000 ALTER TABLE `formas_pagamentos` DISABLE KEYS */;
INSERT INTO `formas_pagamentos` VALUES (2,'À vista','2014-12-08 16:18:23','2014-12-08 16:18:23'),(3,'Mensal','2014-12-08 16:18:33','2014-12-08 16:18:33');
/*!40000 ALTER TABLE `formas_pagamentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `funcoes`
--

DROP TABLE IF EXISTS `funcoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `funcoes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `funcao` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `funcoes`
--

LOCK TABLES `funcoes` WRITE;
/*!40000 ALTER TABLE `funcoes` DISABLE KEYS */;
INSERT INTO `funcoes` VALUES (1,'Secretaria ','2014-12-08 16:57:02','2014-12-08 16:57:02'),(2,'Monitor Esportes Coletivos','2014-12-08 16:57:08','2014-12-08 18:58:07'),(3,'Monitor de Hip Hop','2014-12-08 18:56:08','2014-12-08 18:56:08'),(4,'Monitor Ballet','2014-12-08 18:56:18','2014-12-08 18:56:18'),(5,'Auxiliar Administrativo','2014-12-08 21:21:31','2014-12-08 21:21:31'),(6,'Cozinheira','2014-12-08 21:42:24','2015-02-07 00:22:50'),(7,'Menor Aprendiz','2014-12-08 22:24:56','2014-12-08 22:24:56'),(8,'Monitor de Biscuit','2014-12-08 22:27:42','2014-12-08 22:27:42'),(9,'Monitor/a Pintura em tecido','2014-12-08 22:27:59','2014-12-08 22:27:59'),(10,'Monitor/a Pintura em Madeira','2014-12-08 22:28:18','2015-02-08 22:29:08'),(11,'Monitora de Corte e Costura','2014-12-08 22:28:34','2014-12-08 22:28:34'),(12,'Monitora de Reciclart','2014-12-08 22:28:51','2014-12-08 22:28:51'),(13,'Monitora de Costumização','2014-12-08 22:29:11','2014-12-08 22:29:11'),(14,'Monitor/a Karate','2014-12-08 22:29:24','2014-12-08 22:29:24'),(15,'Monitor/a Taekwondo','2014-12-08 22:29:39','2014-12-08 22:29:39'),(16,'Monitor/ Capoeira','2014-12-08 22:29:50','2014-12-08 22:29:50'),(17,'Músico/a','2014-12-08 22:30:03','2014-12-08 22:30:03'),(18,'Monitora Movimentt\'us Dancing','2014-12-08 22:30:22','2014-12-08 22:30:22'),(19,'Instrutor de Percussão','2014-12-08 22:30:42','2014-12-08 22:30:42'),(20,'Monitor/a Artesanato em Geral','2014-12-08 22:31:11','2014-12-08 22:31:11'),(21,'Alimento Solidário São Jorge','2014-12-08 22:31:46','2014-12-08 22:31:46'),(22,'Alimento Solidário São Francisco','2014-12-08 22:31:58','2014-12-08 22:31:58'),(23,'Monitor/a Violão','2014-12-08 22:32:11','2014-12-08 22:32:11'),(24,'Monitor/a Recreando a Vida com Ação e Reflexão','2014-12-08 22:32:31','2014-12-08 22:32:31'),(25,'Monitor/a Ginástica terceira Idade','2014-12-08 22:32:55','2014-12-08 22:32:55'),(26,'Responsavel Jornal Solidário','2014-12-08 22:33:15','2014-12-08 22:33:15'),(27,'Instrutor Banda Musical','2014-12-08 22:33:28','2014-12-08 22:33:28'),(28,'Monitor academia','2014-12-08 22:33:40','2014-12-08 22:33:40'),(29,'Pedagógico','2014-12-12 19:44:29','2014-12-12 19:44:29'),(30,'Monitor/a Recriando Vida com Ação e Reflexão','2014-12-12 20:12:40','2014-12-12 20:12:40'),(31,'Recepção','2014-12-12 20:15:24','2014-12-12 20:15:24'),(32,'Eventos','2014-12-12 20:17:29','2014-12-12 20:17:29'),(33,'Monitor/a Informática','2014-12-12 20:26:02','2014-12-12 20:26:02'),(34,'Manutenção','2014-12-12 20:28:45','2014-12-12 20:28:45'),(35,'Serviços Gerais','2014-12-23 14:37:14','2014-12-23 14:37:14'),(36,'Assessor de Imprensa','2015-02-03 15:24:25','2015-02-03 15:24:53'),(37,'Diretoria','2015-02-03 15:30:14','2015-02-03 15:30:14'),(40,'Monitor de Danças Gaúchas','2015-02-08 21:58:50','2015-02-08 21:58:50');
/*!40000 ALTER TABLE `funcoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_22_145953_confide_setup_users_table',1),('2014_10_23_211602_create_funcoes_table',1),('2014_10_27_195502_create_unidades_table',2),('2014_10_27_205732_create_agenda_table',2),('2014_10_31_142226_CadastroFormaPagamentos',2),('2014_11_01_210155_create_servicos_terceiros',2),('2014_12_08_105640_create_table_escolas',3),('2014_10_17_222639_create_colaboradores_table',4),('2014_12_18_220200_cidades',5),('2014_12_18_235220_create_table_etnias',6),('2014_12_18_235234_create_table_religiao',6),('2014_12_18_235244_create_table_estadocivil',6),('2014_12_19_220142_create_escolas',6),('2014_12_23_152138_create_alunos',7),('2015_01_05_190911_create_oficinas_table',8),('2015_01_05_191043_create_aluno_oficina_table',8),('2015_01_12_175521_create_atendimentos_table',9),('2015_01_19_143928_add_alunos_table',10),('2015_01_19_170434_add_oficinas_table',11),('2015_02_06_020953_add_sexo_to_alunos',12),('2015_02_06_131138_add_alunos_table',13),('2015_02_06_132651_add_oficinas_table',13);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oficinas`
--

DROP TABLE IF EXISTS `oficinas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oficinas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `responsavel_id` int(11) NOT NULL,
  `local` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `turno` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` date NOT NULL,
  `hora_inicio` time NOT NULL,
  `hora_fim` time NOT NULL,
  `max_participantes` int(11) NOT NULL,
  `idade_minima` int(11) NOT NULL,
  `idade_maxima` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `dias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `obs_autorizacao` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oficinas`
--

LOCK TABLES `oficinas` WRITE;
/*!40000 ALTER TABLE `oficinas` DISABLE KEYS */;
INSERT INTO `oficinas` VALUES (1,'Ballet Tarde (5 a 10)',4,'Associação Beneficente São Jose/ PROJARI','Tarde','2015-01-12','13:30:00','15:00:00',30,5,10,'2015-01-07 06:33:06','2015-02-06 21:51:35','Segundas/Quartas/Sextas',''),(2,'Hip Hop(Space Dance manhã)',40,'Associação Beneficente São Jose/ PROJARI','Manhã','2015-01-13','08:30:00','11:30:00',25,6,100,'2015-01-12 15:15:13','2015-02-08 21:48:34','Segundas/Quartas/Sextas',''),(3,'Ballet(manhã 11 à 18)',4,'Associação Beneficente São Jose/ PROJARI','Manhã','2015-01-13','08:30:00','10:00:00',30,11,18,'2015-01-12 15:32:21','2015-02-08 21:46:20','Segundas/Quartas/Sextas',''),(4,'Hip Hop (Space Dance) Tarde',40,'Associação Beneficente São Jose/ PROJARI','Tarde','2015-01-13','13:30:00','17:00:00',25,6,100,'2015-01-12 15:39:30','2015-02-03 14:30:00','',''),(5,'Corte e Costura',17,'Associação Beneficente São Jose/ PROJARI','Tarde','2015-01-13','13:30:00','16:30:00',30,18,100,'2015-01-12 16:27:06','2015-02-08 22:27:26','Segundas',''),(6,'Costumização',41,'Associação Beneficente São Jose/ PROJARI','Tarde','2015-01-13','13:30:00','17:00:00',15,18,100,'2015-01-12 16:28:45','2015-01-12 16:28:45','',''),(7,'Esporte Coletivo(manhã 7 à 9)',42,'Associação Beneficente São Jose/ PROJARI','Manhã','2015-03-02','08:30:00','11:30:00',40,7,9,'2015-01-14 18:41:46','2015-02-08 21:45:28','Segundas/Quintas',''),(8,'Esporte Coletivo(manhã 10 a 14)',42,'Associação Beneficente São Jose/ PROJARI','Manhã','0000-00-00','08:30:00','11:30:00',40,10,14,'2015-01-14 18:42:59','2015-01-14 18:42:59','',''),(9,'Esporte Coletivo(manhã 6 anos)',42,'Associação Beneficente São Jose/ PROJARI','Manhã','2015-03-02','09:30:00','11:30:00',25,6,6,'2015-01-14 18:44:40','2015-02-08 22:52:14','Terças',''),(10,'Esporte Coletivo(tarde 7 à 9)',42,'Associação Beneficente São Jose/ PROJARI','Tarde','2015-03-02','13:30:00','16:30:00',40,7,9,'2015-01-14 18:45:49','2015-02-08 21:44:31','Segundas/Quintas',''),(11,'Esporte Coletivo(tarde 10 a 14 anos)',42,'Associação Beneficente São Jose/ PROJARI','Tarde','0000-00-00','13:30:00','16:30:00',40,10,14,'2015-01-14 18:49:38','2015-02-06 15:06:57','Quarta e sexta feira',''),(12,'Esporte Coletivo(tarde 6)',42,'Associação Beneficente São Jose/ PROJARI','Tarde','0000-00-00','13:30:00','17:00:00',25,6,6,'2015-01-14 18:50:46','2015-01-14 18:50:46','',''),(13,'Karate(manhã 05 à 25 anos)',45,'Associação Beneficente São Jose/ PROJARI','Manhã','2015-03-02','08:30:00','10:00:00',50,5,25,'2015-01-14 18:57:11','2015-02-08 21:54:34','Segundas/Terças',''),(14,'Karate(tarde 05 à 11 anos)',45,'Associação Beneficente São Jose/ PROJARI','Tarde','2015-03-02','15:15:00','16:15:00',50,4,25,'2015-01-14 18:59:14','2015-02-06 15:01:36','Terça e quinta',''),(15,'Biscuit (Tarde 12 à 20 anos)',50,'Bom Fim','Tarde','2015-02-04','13:30:00','16:30:00',10,12,20,'2015-02-04 14:18:48','2015-02-06 14:59:58','Segunda',''),(16,'Pintura em Tecido iniciantes  (tarde 12 à 20 anos) ',50,'Bom Fim','Tarde','2015-02-04','13:30:00','16:30:00',10,13,20,'2015-02-04 14:23:07','2015-02-06 14:59:10','Sexta ',''),(17,'Pintura em Tecido avançado (21 à 100 anos)',15,'Bom Fim','Tarde','2015-02-04','13:30:00','16:30:00',10,21,100,'2015-02-04 14:39:51','2015-02-04 14:39:51','Terça',''),(18,'Artesanato em Geral (tarde 18 à 80 anos)',17,'Bom Fim','Tarde','2015-02-04','13:30:00','16:30:00',12,18,80,'2015-02-04 14:41:25','2015-02-04 14:41:25','Terça',''),(19,'Karate (tarde acima de 12 anos)',45,'Bom Fim','Tarde','2015-02-04','16:00:00','17:00:00',50,12,80,'2015-02-06 15:03:14','2015-02-07 22:03:07','Terça/Quinta',''),(20,'Ballet (tarde 11 a 18 anos)',4,'Associação Beneficente São Jose/ PROJARI','Tarde','2015-01-13','15:15:00','17:00:00',30,11,18,'2015-02-06 21:51:10','2015-02-07 22:04:12','Segundas/Quartas/Sextas',''),(21,'Ballet(manhã 5 à 10 anos)',4,'Associação Beneficente São Jose/ PROJARI','Manhã','2015-01-13','10:15:00','11:30:00',30,5,10,'2015-02-06 21:54:54','2015-02-08 21:47:01','Segundas/Quartas/Sextas',''),(22,'Taekwondo(manhã)',52,'Associação Beneficente São Jose/ PROJARI','Manhã','2015-03-02','10:15:00','11:30:00',30,5,25,'2015-02-08 21:53:23','2015-02-08 22:26:29','Segundas/Quartas',''),(23,'Artes Gaúchas(manhã)',53,'Associação Beneficente São Jose/ PROJARI','Manhã','2015-03-02','10:00:00','11:30:00',30,12,100,'2015-02-08 22:03:08','2015-02-08 22:03:25','Segundas',''),(24,'Grupo Folclórico(manhã)',53,'Associação Beneficente São Jose/ PROJARI','Manhã','2015-03-02','09:00:00','11:30:00',30,12,100,'2015-02-08 22:05:15','2015-02-08 22:08:48','Quintas',''),(25,'Artes Gaúchas(tarde)',53,'Associação Beneficente São Jose/ PROJARI','Tarde','2015-03-02','15:15:00','17:00:00',30,12,100,'2015-02-08 22:06:35','2015-02-08 22:06:35','Segundas',''),(26,'Grupo Folclórico(tarde)',53,'Associação Beneficente São Jose/ PROJARI','Tarde','2015-03-02','13:30:00','16:00:00',30,12,100,'2015-02-08 22:08:28','2015-02-08 22:08:28','Quintas',''),(27,'Informática(tarde)',9,'Associação Beneficente São Jose/ PROJARI','Tarde','2015-03-02','13:30:00','16:30:00',20,8,18,'2015-02-08 22:22:53','2015-02-08 22:22:53','Segundas',''),(28,'Taekwondo(tarde)',52,'Associação Beneficente São Jose/ PROJARI','Tarde','2015-03-02','13:30:00','15:00:00',30,5,25,'2015-02-08 22:25:58','2015-02-08 22:25:58','Segundas/Quartas',''),(29,'Orquestra(manhã)',54,'Associação Beneficente São Jose/ PROJARI','Manhã','2015-03-02','10:00:00','11:30:00',30,8,70,'2015-02-08 22:47:37','2015-02-08 22:47:37','Terças',''),(30,'Orquestra(tarde)',54,'Associação Beneficente São Jose/ PROJARI','Tarde','2015-03-02','13:30:00','16:30:00',30,8,70,'2015-02-08 22:48:56','2015-02-08 22:48:56','Terças','');
/*!40000 ALTER TABLE `oficinas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_reminders`
--

DROP TABLE IF EXISTS `password_reminders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_reminders` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_reminders`
--

LOCK TABLES `password_reminders` WRITE;
/*!40000 ALTER TABLE `password_reminders` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_reminders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `religioes`
--

DROP TABLE IF EXISTS `religioes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `religioes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `religioes`
--

LOCK TABLES `religioes` WRITE;
/*!40000 ALTER TABLE `religioes` DISABLE KEYS */;
INSERT INTO `religioes` VALUES (1,'Católica','2014-12-30 16:08:31','2014-12-30 16:08:31'),(2,'Evangélica','2014-12-30 16:08:51','2014-12-30 16:08:51'),(5,'Luterana','2014-12-30 16:21:27','2014-12-30 16:21:27'),(6,'Umbanda','2015-01-12 14:29:21','2015-01-12 14:29:21'),(7,'Adventista','2015-01-12 14:30:05','2015-01-12 14:30:05'),(9,'Espirita','2015-01-12 14:30:37','2015-01-12 14:30:37'),(10,'Mormon','2015-01-12 14:31:09','2015-01-12 14:31:09'),(11,'Testemunha de Jeová','2015-01-12 14:32:31','2015-02-03 14:29:04'),(12,'Metodista','2015-02-04 18:00:18','2015-02-04 18:00:18');
/*!40000 ALTER TABLE `religioes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicos_terceiros`
--

DROP TABLE IF EXISTS `servicos_terceiros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicos_terceiros` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome_empresa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data_servico` date DEFAULT NULL,
  `valor` decimal(10,2) NOT NULL,
  `telefone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nota_fiscal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `formas_pagamentos_id` int(10) unsigned NOT NULL,
  `orcamento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `servicos_terceiros_formas_pagamentos_id_foreign` (`formas_pagamentos_id`),
  CONSTRAINT `servicos_terceiros_formas_pagamentos_id_foreign` FOREIGN KEY (`formas_pagamentos_id`) REFERENCES `formas_pagamentos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicos_terceiros`
--

LOCK TABLES `servicos_terceiros` WRITE;
/*!40000 ALTER TABLE `servicos_terceiros` DISABLE KEYS */;
INSERT INTO `servicos_terceiros` VALUES (3,'KR Tur Transportes','2014-12-07',100.00,'5134913266','','',3,'5.000,00','2014-12-08 21:23:08','2014-12-08 21:24:46');
/*!40000 ALTER TABLE `servicos_terceiros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `table_escolas`
--

DROP TABLE IF EXISTS `table_escolas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `table_escolas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `publica` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `table_escolas`
--

LOCK TABLES `table_escolas` WRITE;
/*!40000 ALTER TABLE `table_escolas` DISABLE KEYS */;
/*!40000 ALTER TABLE `table_escolas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unidades`
--

DROP TABLE IF EXISTS `unidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unidades` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `unidade` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `local` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `responsavel_id` int(10) unsigned NOT NULL,
  `telefone` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `cidade` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `bairro` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `numero` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `unidades_responsavel_id_foreign` (`responsavel_id`),
  CONSTRAINT `unidades_responsavel_id_foreign` FOREIGN KEY (`responsavel_id`) REFERENCES `colaboradores` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unidades`
--

LOCK TABLES `unidades` WRITE;
/*!40000 ALTER TABLE `unidades` DISABLE KEYS */;
INSERT INTO `unidades` VALUES (1,'Unidade São Francisco','Rua Estância Velha, 112 São Francisco',3,'5195181207','RS','Guaíba','São Francisco','Rua Igrejinha','186','2014-12-08 16:24:46','2014-12-08 21:24:57'),(2,'Unidade São Jorge','São Jorge',29,'5180227411','RS','Guaíba','São Jorge','Rua Terra Dura','387','2014-12-23 14:56:36','2014-12-23 14:56:36');
/*!40000 ALTER TABLE `unidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `confirmation_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','nome@projari.com.br','$2y$10$76GevFuojVNvx/eMOy19zeBss0pofHyFjAwqr1wETCpNbpK4xx2TK','99880365a9f21f98f457c51774b8ba71','1XXuQttk0Hb3Cu4KDAdrrG71JjJX7Cx1ZksaCKiIjiLiDKX1n162xWCvCLUL',1,'2014-10-28 04:29:34','2014-10-28 13:37:07');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-02-08 16:29:32
