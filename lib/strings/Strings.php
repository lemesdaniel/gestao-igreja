<?php
/**
 * Classe para tratamento de strings
 *
 * @author Rodrigo de Brum Chimainski <rodrigo@iconeweb.com.br>
 */
class Strings {

    /**
     * Retorna apenas números
     * @param string $string
     * @return string
     */
    public static function OnlyNumber($string) {
        return preg_replace('/[^0-9]/', '', $string);
    }

    /**
     * Retorna apenas letras
     * @param string $string
     * @return string
     */
    public static function OnlyAlpha($string) {
        return preg_replace("/[^a-zA-Z\s]/", '', $string);
    }

    /**
     * Retorna apenas letras e números
     * @param string $string
     * @return string
     */
    public static function OnlyAlphaAndNumber($string) {
        return preg_replace("/[^a-zA-Z0-9\s]/", '', $string);
    }

    /**
     * Retorna a data no formato 00/00/0000
     * @param string $data Data no formato aceitável por strtotime ver http://php.net/strtotime
     * @return string
     */
    public static function FormatDate($data) {
        if ($data == null || $data == '0000-00-00') {
            $data = '';
        } else {
            $data = date('d/m/Y', strtotime($data));
        }
        return $data;
    }
    
    /**
     * Retorna data no formato aceito pelo banco de dados
     * @param type $data Data no formato aceito por strtotime ver: http://php.net/strtotime
     * @return string
     */
    public static function FormatDatetoBD($data) {
        if($data != '' || $data != '0000-00-00') {
            $data = str_replace('/', '-',$data);
            if (!$data) {
                return null;
            }
            
            $data = strtotime($data);
            $data = date('Y-m-d', $data);
        } else {
            $data = null;
        }
        return $data;
    }

    
    /**
     * Retorna valor formatado
     * @return string
     */
    public static function FormatMoney($value) {
        setlocale(LC_MONETARY,"pt_BR.UTF-8");
        return money_format('%!n', $value);
    }

    /**
     * Retorna valor no formato aceito pelo banco de dados
     * @param type $value Valor no formato float Ex.: 5789.25
     * @return float
     */
    public static function FormatMoneytoBD($value) {
        if($value != '') {
            $value = str_replace(",", ".", str_replace(".", "", $value));
        } else {
            $value = null;
        }
        return $value;
    }
    
    /**
     * Retorna valor no formato aceito pelo banco de dados
     * @param type $value Valor sem mascara de formatação
     */
    public static function FormatPhonetoBD($value) {
        if($value != '') {
            $search = array('(', ')', '-', ' '); 
            $value = str_replace($search, "", $value);
        } else {
            $value = null;
        }
        return $value;
    }
    
}