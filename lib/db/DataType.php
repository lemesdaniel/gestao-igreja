<?php

/**
 * Classe para retornar os tipos de dados disponíveis para criação de campos em tabelas.
 *
 * @author Rodrigo de Brum Chimainski <rodrigo@iconeweb.com.br>
 */
class DataType
{

    /**
	 * Campo tipo data
	 * @param type $name
	 * @param array $opt Opções extras para o campo. Disponíveis: Size : Integer, label: String, Size: Integer
	 * @return array
	 */
    public static function Date($name, $opt = [])
    {

        if (isset($opt['Size'])) {
            $Size = $opt['Size'];
        } else {
            $Size = 11;
        }

        return [
            'Name'     => $name,
            'Type'     => 'date',
            'Size'     => $Size,
            'Default'  => (isset($opt['Default'])?$opt['Default']:''),
            'Label'    => (isset($opt['Label'])?$opt['Label']:''),
            'TabPage'  => (isset($opt['TabPage'])?$opt['TabPage']:''),
            'Line'     => (isset($opt['Line'])?$opt['Line']:'1'),
            'Column'   => (isset($opt['Column'])?$opt['Column']:''),
            'TabIndex' => (isset($opt['TabIndex'])?$opt['TabIndex']:''),
        ];
    }

    /**
	 * Campo q monta um combobox
	 * @param type $name
	 * @param array $opt Opções extras para o campo. Disponíveis: Options[array chave/valor] : label: Style, Size: Integer
	 * @return array
	 */
    public static function Select($name, $opt = [])
    {

        if (isset($opt['Size'])) {
            $Size = $opt['Size'];
        } else {
            $Size = 11;
        }

        return [
            'Name'     => $name,
            'Options'  => $opt['Options'],
            'Type'     => 'select',
            'Default'  => $opt['Default'],
            'Style'    => (isset($opt['Style'])?$opt['Style']:''),
            'Label'    => (isset($opt['Label'])?$opt['Label']:''),
            'TabPage'  => (isset($opt['TabPage'])?$opt['TabPage']:''),
            'Line'     => (isset($opt['Line'])?$opt['Line']:'1'),
            'Column'   => (isset($opt['Column'])?$opt['Column']:''),
            'TabIndex' => (isset($opt['TabIndex'])?$opt['TabIndex']:''),
        ];
    }

    public static function States($name, $opt = [])
    {
        if (!isset($opt['Default'])) {
            $opt['Default'] = 'RS';
        }
        $opt['Size']    = 2;
        $opt['Options'] = [
            "AC" => "Acre",
            "AL" => "Alagoas",
            "AM" => "Amazonas",
            "AP" => "Amapá",
            "BA" => "Bahia",
            "CE" => "Ceará",
            "DF" => "Distrito Federal",
            "ES" => "Espírito Santo",
            "GO" => "Goiás",
            "MA" => "Maranhão",
            "MT" => "Mato Grosso",
            "MS" => "Mato Grosso do Sul",
            "MG" => "Minas Gerais",
            "PA" => "Pará",
            "PB" => "Paraíba",
            "PR" => "Paraná",
            "PE" => "Pernambuco",
            "PI" => "Piauí",
            "RJ" => "Rio de Janeiro",
            "RN" => "Rio Grande do Norte",
            "RO" => "Rondônia",
            "RS" => "Rio Grande do Sul",
            "RR" => "Roraima",
            "SC" => "Santa Catarina",
            "SE" => "Sergipe",
            "SP" => "São Paulo",
            "TO" => "Tocantins"
        ];
        return self::Select($name, $opt);
    }

    /**
	 * Campo que monta um radio
	 * @param type $name
	 * @param array $opt Opções extras para o campo. Disponíveis: Options[array chave/valor] : label: Style, Size: Integer
	 * @return array
	 */
    public static function Radio($name, $opt = [])
    {

        if (isset($opt['Size'])) {
            $Size = $opt['Size'];
        } else {
            $Size = 11;
        }

        return [
            'Name'     => $name,
            'Options'  => $opt['Options'],
            'Type'     => 'radio',
            'Default'  => $opt['Default'],
            'Style'    => (isset($opt['Style'])?$opt['Style']:''),
            'Label'    => (isset($opt['Label'])?$opt['Label']:''),
            'TabPage'  => (isset($opt['TabPage'])?$opt['TabPage']:''),
            'Line'     => (isset($opt['Line'])?$opt['Line']:'1'),
            'Column'   => (isset($opt['Column'])?$opt['Column']:''),
            'TabIndex' => (isset($opt['TabIndex'])?$opt['TabIndex']:''),
        ];
    }

    /**
	 * Campo que monta um radio group
	 * @param type $name
	 * @param array $opt Opções extras para o campo. Disponíveis: Options[array chave/valor] : label: Style, Size: Integer
	 * @return array
	 */
    public static function RadioGroup($name, $opt = [])
    {

        if (isset($opt['Size'])) {
            $Size = $opt['Size'];
        } else {
            $Size = 11;
        }

        return [
            'Name'         => $name,
            'LabelOptions' => $opt['LabelOptions'],
            'Options'      => $opt['Options'],
            'Type'         => 'radio-group',
            'Default'      => $opt['Default'],
            'Style'        => (isset($opt['Style'])?$opt['Style']:''),
            'Label'        => (isset($opt['Label'])?$opt['Label']:''),
            'TabPage'      => (isset($opt['TabPage'])?$opt['TabPage']:''),
            'Line'         => (isset($opt['Line'])?$opt['Line']:'1'),
            'Column'       => (isset($opt['Column'])?$opt['Column']:''),
            'TabIndex'     => (isset($opt['TabIndex'])?$opt['TabIndex']:''),
        ];
    }

    /**
	 * Campo tipo inteiro
	 * @param type $name
	 * @param array $opt Opções extras para o campo. Disponíveis: Size : Integer, label: String, Size: Integer
	 * @return array
	 */
    public static function Integer($name, $opt = [])
    {

        if (isset($opt['Size'])) {
            $Size = $opt['Size'];
        } else {
            $Size = 11;
        }

        $primaryKey = false;
        if (isset($opt['PrimaryKey'])) {
            $primaryKey = true;
        }

        return [
            'Name'       => $name,
            'Type'       => 'int',
            'Size'       => $Size,
            'Label'      => (isset($opt['Label'])?$opt['Label']:''),
            'PrimaryKey' => $primaryKey,
            'TabPage'    => (isset($opt['TabPage'])?$opt['TabPage']:''),
            'Line'       => (isset($opt['Line'])?$opt['Line']:'1'),
            'Column'     => (isset($opt['Column'])?$opt['Column']:''),
            'TabIndex'   => (isset($opt['TabIndex'])?$opt['TabIndex']:''),
            'Required'   => (isset($opt['Required'])?$opt['Required']:false),
        ];
    }

    /**
	 * Campo tipo Texto
	 * @param string $name Nome do campo
	 * @param type $opt Opções extras para o campo. Disponíveis: Size : Integer, label: String, Size: Integer
	 * @return array
	 */
    public static function Text($name, $opt = [])
    {

        if (isset($opt['Size'])) {
            $Size = $opt['Size'];
        } else {
            $Size = 255;
        }

        return [
            'Name'     => $name,
            'Type'     => 'varchar',
            'Size'     => $Size,
            'Label'    => (isset($opt['Label'])?$opt['Label']:''),
            'TabPage'  => (isset($opt['TabPage'])?$opt['TabPage']:''),
            'Line'     => (isset($opt['Line'])?$opt['Line']:'1'),
            'Column'   => (isset($opt['Column'])?$opt['Column']:''),
            'TabIndex' => (isset($opt['TabIndex'])?$opt['TabIndex']:''),
            'Required' => (isset($opt['Required'])?'required':''),
        ];
    }

    public static function Phone($name, $opt = [])
    {

        if (isset($opt['Size'])) {
            $Size = $opt['Size'];
        } else {
            $Size = 20;
        }

        return array_merge(self::Text($name, $opt), [
                'Size' => $Size,
                'Type' => 'phone'
            ]);
    }

    public static function CEP($name, $opt = [])
    {

        if (isset($opt['Size'])) {
            $Size = $opt['Size'];
        } else {
            $Size = 9;
        }

        return array_merge(self::Text($name, $opt), [
                'Size' => $Size,
                'Type' => 'cep'
            ]);
    }

    public static function CNPJ($name, $opt = [])
    {

        if (isset($opt['Size'])) {
            $Size = $opt['Size'];
        } else {
            $Size = 20;
        }

        return array_merge(self::Text($name, $opt), [
                'Size' => $Size,
                'Type' => 'cnpj'
            ]);
    }

    public static function CPF($name, $opt = [])
    {

        if (isset($opt['Size'])) {
            $Size = $opt['Size'];
        } else {
            $Size = 15;
        }

        return array_merge(self::Text($name, $opt), [
                'Size' => $Size,
                'Type' => 'cpf'
            ]);
    }

    public static function Money($name, $opt = [])
    {

        if (isset($opt['Size'])) {
            $Size = $opt['Size'];
        } else {
            $Size = 15;
        }

        return array_merge(self::Text($name, $opt), [
                'Size' => $Size,
                'Type' => 'money'
            ]);
    }

    public static function Email($name, $opt = [])
    {

        if (isset($opt['Size'])) {
            $Size = $opt['Size'];
        } else {
            $Size = 255;
        }

        return [
            'Name'     => $name,
            'Type'     => 'email',
            'Size'     => $Size,
            'Label'    => (isset($opt['Label'])?$opt['Label']:''),
            'TabPage'  => (isset($opt['TabPage'])?$opt['TabPage']:''),
            'Line'     => (isset($opt['Line'])?$opt['Line']:'1'),
            'Column'   => (isset($opt['Column'])?$opt['Column']:''),
            'TabIndex' => (isset($opt['TabIndex'])?$opt['TabIndex']:''),
        ];
    }

    /**
	 * Campo tipo TextArea
	 * @param string $name Nome do campo
	 * @param type $opt Opções extras para o campo. Disponíveis: Size : Integer, label: String, Size: Integer
	 * @return array
	 */
    public static function TextArea($name, $opt = [])
    {

        if (isset($opt['Size'])) {
            $Size = $opt['Size'];
        } else {
            $Size = 255;
        }

        return [
            'Name'     => $name,
            'Type'     => 'longtext',
            'Size'     => $Size,
            'Label'    => (isset($opt['Label'])?$opt['Label']:''),
            'TabPage'  => (isset($opt['TabPage'])?$opt['TabPage']:''),
            'Line'     => (isset($opt['Line'])?$opt['Line']:'1'),
            'Column'   => (isset($opt['Column'])?$opt['Column']:''),
            'TabIndex' => (isset($opt['TabIndex'])?$opt['TabIndex']:''),
            'Required' => (isset($opt['Required'])?$opt['Required']:false),
        ];
    }

    /**
	 * Campo tipo função
	 * @param type $name
	 * @param array $opt Opções extras para o campo.
	 * @return array
	 */
    public static function Funcao($name, $opt = [])
    {

        if (!isset($opt['Default'])) {
            $opt['Default'] = '';
        }

        $opt['Options'] = ['' => 'Selecione'];
        $funcoes        = Funcoes::all();

        foreach ($funcoes as $funcao) {
            $opt['Options'][$funcao->id] = $funcao->funcao;
        }

        return self::Select($name, $opt);
    }

    /**
	 * Campo tipo Responsavel
	 * @param type $name
	 * @param array $opt Opções extras para o campo.
	 * @return array
	 */
    public static function Responsavel($name, $opt = [])
    {

        if (!isset($opt['Default'])) {
            $opt['Default'] = '';
        }

        $opt['Options'] = ['' => 'Selecione'];
        $colaboradores  = Colaborador::all();

        foreach ($colaboradores as $colaborador) {
            $opt['Options'][$colaborador->id] = $colaborador->nome;
        }

        return self::Select($name, $opt);
    }

    /**
	 * Campo tipo Formas de Pagamento
	 * @param type $name
	 * @param array $opt Opções extras para o campo.
	 * @return array
	 */
    public static function FormasPagamento($name, $opt = [])
    {

        if (!isset($opt['Default'])) {
            $opt['Default'] = '';
        }

        $opt['Options'] = ['' => 'Selecione'];
        $formas         = FormasPagamentos::all();

        foreach ($formas as $forma) {
            $opt['Options'][$forma->id] = $forma->forma;
        }

        return self::Select($name, $opt);
    }

}

class DT extends DataType
{

}

