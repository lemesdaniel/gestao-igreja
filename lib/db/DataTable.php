<?php

/**
 * Gera uma tabela para listagem de registros.
 * Automaticamente adiciona campos para seleção de edição e remoção
 *
 * @author Rodrigo de Brum Chimainski <rodrigo@iconeweb.com.br>
 */
class DataTable {

    /**
     * Registros para gerar linhas da tabela
     * @var array
     */
    private $regs = array();

    /**
     * Limite padrão para listagem
     * @var int
     */
    private $limit = 10;

    /**
     * Valor do offset para a listagem
     * @var int
     */
    private $offset = 0;

    /**
     * Classe do modelo da tabela
     * @var object
     */
    private $model;

    /**
     * Classe CSS para tag table
     * @var string
     */
    private $cssClass = 'table';

    /**
     * ID da tag table
     * @var string
     */
    private $id = 'datatable';

    /**
     * Armazena colunas extras para adicionar ao Grid
     * @var array
     */
    private $columns = array();

    public function __construct($model) {
        // assim deixa o relacionamento disponível
        $this->model = $model;
        $this->regs = $model::paginate($this->getLimit());
    }

    /**
     * Seta classe do CSS para tag table
     * @param string $cssClass
     */
    public function setCssClass($cssClass) {
        $this->cssClass = $cssClass;
        return $this;
    }

    /**
     * Retorna classe do CSS para tag table
     * @return string
     */
    public function getCssClass() {
        return $this->cssClass;
    }

    /**
     * Seta o ID da tag table
     * @param string $id
     */
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    /**
     * Retorna o ID da tag table
     * @return string
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Seta o valor de limit para limitar o número de registros que serão lidos na tabela
     * @param int $limit
     */
    public function setLimit($limit) {
        $this->limit = $limit;
    }

    /**
     * Retorna o valor de registros máximo que serão lidos na tabela
     * @return int
     */
    public function getLimit() {
        return $this->limit;
    }

    /**
     * Seta o valor de offset da listagem
     * @param int $offset
     */
    public function setOffset($offset) {
        $this->offset = $offset;
    }

    /**
     * Retorna o valor de offset setado para a listagem
     * @return int
     */
    public function getOffset() {
        return $this->offset;
    }

    /**
     * Recebe classe de modelo de banco de dados para listar os registros
     * @param string Modelo da classe modelo
     * @return self
     */
    public static function model($model) {

        return new DataTable($model);
    }

    /**
     * Adiciona colunas extras ao Grid
     * @param array $el Dados da coluna extra. Elementos: 'Title'(Opcional) = Título da coluna, 'Content' = Conteúdo da Coluna
     */
    public function addColumn($el) {
        $this->columns[] = $el;
        return $this;
    }

    /**
     * Retorna as colunas extras adicionadas ao Grid
     * @return array
     */
    private function getColumnsExtra() {
        return $this->columns;
    }

    private function FormatValues($col, $value) {

        switch ($col['Type']) {
            case 'date':
                $value = Strings::FormatDate($value);
            case 'money':
                $value = Strings::FormatMoney($value);
            default:
        }

        return $value;
    }

    

    /**
     * Retorna a tabela gerada após configurada a classe DataTable
     * @return string
     */
    public function make() {

        $columns = $this->model->structure();
        $fields_grid = $this->model->fields_grid();
        $host = URL::to(Route::getCurrentRoute()->getPath());
        $titles = [];
            
        if (is_array($fields_grid) && count($fields_grid) > 0) {
            foreach ($columns as $column) {
                if (in_array($column['Name'], $fields_grid)) {
                    $titles[$column['Name']] = (isset($column['Label']) && $column['Label'] != '' ? $column['Label'] : $column['Name']);
                }
            }
        } else {
            foreach ($columns as $column) {
                $titles[$column['Name']] = (isset($column['Label']) && $column['Label'] != '' ? $column['Label'] : $column['Name']);
            }
        }

        $regs = [];

        foreach ($this->regs AS $reg) {
            $reg_temp = [];

            if (is_array($fields_grid) && count($fields_grid) > 0) {
                foreach ($columns as $column) {
                    if (in_array($column['Name'], $fields_grid)) {

                        $reg_temp[$titles[$column['Name']]] = $this->FormatValues($column, $reg->$column['Name']);
                    }
                }
            } else {
                foreach ($columns as $column) { // Se não DataTable usa as colunas na tabela do banco
                    $reg_temp[$titles[$column['Name']]] = $this->FormatValues($column, $reg->$column['Name']);
                }
            }

            $id = Crypt::encrypt($reg->{$this->model->getKeyName()});
            $event = ' href="' . $host . '/edit/' . $id . '"';
            if (Request::ajax()) {
                $event = " onclick=\"loadpage('$host/edit/" . $id . "', '#ajax-content')\" href=\"#\"";
            }
            $reg_temp['Ações'] = ' <a class="" role="button"' . $event . ' rel="tooltip" data-toggle="tooltip" title="Editar"><span class="glyphicon glyphicon-edit"></span> </a>';
            $event = " onclick=\"deleteRegister('$host/delete/$id', '$host')\"";
            $reg_temp['Ações'] .= ' <a class="" role="button"' . $event . ' rel="tooltip" data-toggle="tooltip" title="Excluir"><span class="glyphicon glyphicon-remove"></span> </a>';
            $regs[] = $reg_temp;
        }

        $html = Table::withContents($regs)->hover();

        $html .= $this->regs->links();

        return $html;
    }

}
