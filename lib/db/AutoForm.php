<?php
/**
 * Geração automatica do formulário de acordo com classe modelo configurado
 *
 * @author Rodrigo de Brum Chimainski <rodrigo@iconeweb.com.br>
 */
class AutoForm {

    /**
     * Armazena o model passado para a classe
     * @var object
     */
    private $model = null;

    /**
     * Armazena valor da ação do action do submit
     * @var string
     */
    private $action = 'save';

    /**
     * Prefixo do nome dos noms de campos dos formulários
     * @var string
     */
    private $prename = '';

    /**
     * Id do registro a ser editado ou removido
     * @var int
     */
    private $id = 0;

    /**
     * Id do registro criptografado
     * @var string
     */
    private $id_cripto = null;

    /**
     * Função construtora do autoform
     * @param type $model
     */
    public function __construct($model, $id = 0) {
        $this->model = $model;
        if ($id !== 0) {
            $this->id_cripto = $id;
            $this->id = (int) Crypt::decrypt($id);
        }
    }

    /**
     * Seta o valor da action do botão submit do formulário
     * @param string $submit Ação do submit
     * @return object
     * 
     * @author Jean Dias <jean@iconeweb.com.br>
     */
    public function setAction($action) {
        $this->action = $action;
        return $this;
    }

    /**
     * Retorna a action do botão submit do formulário
     * 
     * @author Jean Dias <jean@iconeweb.com.br>
     */
    public function getAction() {
        return $this->action;
    }

    /**
     * Recebe a classe modelo para gerar o formulário a partir da tabela do banco de dados
     * @param string $model Modelo da classe modelo
     * @return \AutoForm
     */
    public static function model($model, $id = 0) {
        return new AutoForm($model, $id);
    }

    /**
     * Confirma se o formulário enviado é o mesmo
     * @return boolean Retorna true se formulário é o enviado
     */
    private function isSend() {
        return false;
    }

    /**
     * Salva os dados enviados pelo formulário
     * @return boolean True se for salvo com sucesso
     */
    public function save($data = null) {

        if ($this->id > 0) {
            $model = $this->model->find($this->id);
        } else {
            $model = $this->model;
        }
        
        $columns = $model->structure();
        
        if ($data != null) {
            $data = $this->formatFields($columns, $data);

            if ($this->id > 0) {
                $model->update($data);
            } else {
                $model::create($data);
            }
        }
        
        return $model->getKey();
    }
    
    public function formatFields($fields = [], $data) {
        
        foreach ($fields as $col) {
            if ($col['Type'] == 'radio-group') {
                $value = $data[$this->prename . $col['Name']];
                $data = $this->formatFields($col['Options'][$value], $data);
            } else {
                if (!isset($col['PrimaryKey']) || (isset($col['PrimaryKey']) && !$col['PrimaryKey'])) {
                    if (isset($data[$this->prename . $col['Name']])) {
                        $value = $data[$this->prename . $col['Name']];

                        if ($col['Type'] == 'date') {
                            $value = Strings::FormatDatetoBD($value);
                            if (isset($col['Null']) && !$col['Null'] && $value == null) {
                                $value = '0000-00-00';
                            }
                        }

                        if ($col['Type'] == 'money') {
                            $value = Strings::FormatMoneytoBD($value);
                            if (isset($col['Null']) && !$col['Null'] && $value == null) {
                                $value = 0.00;
                            }
                        }

                        if ($col['Type'] == 'int') {
                            $value = (int)$value;
                            if (isset($col['Null']) && !$col['Null'] && $value == null) {
                                $value = 0;
                            }
                        }

                        if ($col['Type'] == 'password' && !empty($value)) {
                            $value = Crypt::encrypt($value);
                        }

                        if (($col['Type'] == 'phone') || ($col['Type'] == 'cpf') || ($col['Type'] == 'cnpj') || ($col['Type'] == 'cep')) {
                            $value = Strings::OnlyNumber($value);
                            if (isset($col['Null']) && !$col['Null'] && $value == null) {
                                $value = '';
                            }
                        }

                        $data[$this->prename . $col['Name']] = $value;
                    }
                }
            }
        }
        
        return $data;
    }

    /**
     * Une as possíveis strings faltantes e retorna o formulário gerado
     * @return string
     */
    public function make() {

        if ($this->isSend()) {
            if ($this->getAction() == 'save') {
                $this->save();
            }
        }

        $id = $this->id;
        $btnDel = '';
        $action = 'add';
        $host = url(Request::segment(1));

        if ($id > 0) {
            $action = 'edit';
            $model = $this->model->find($id);

            $btnDel = '<button type="button" id="AFBtnDelete" class="btn btn-primary" data-delete="' . $host . '/delete/' . Crypt::encrypt($id) . '" data-redirect="' . $host . '">Excluir</button>';
        } else {
            $model = $this->model;
        }

        $form = Form::model($model, array('files' => true));

        $columns = [];
        $tabs = $model->form_tabs();
        $tabsElements = [];
        $noTabsElements = [];
        $colName = '';
        $value = '';
        $html = '';
        $element = '';

        // Apenas ajusta $columns para contar no valor o nome do campo, facilitando a busca de parâmetros
        foreach ($model->structure() as $column) {
            $columns[$column['Name']] = $column;
        }
        
        foreach ($columns as $col) {

            $colName = $col['Name'];
            
            if (isset($col['Label']) && $col['Label'] != '') {
                $label = $col['Label'];
            } else {
                $label = $colName;
            }

            if (isset($col['Id']) && $col['Id'] != '') {
                $id = $col['Id'];
            } else {
                $id = $colName;
            }

            switch ($col['Type']) {
                case 'radio-group':
                    
                    $idDiv = 'div_'.$this->prename . $colName;
                    $el = '<div class="input-group-addon" id="'.$idDiv.'">';
                    $optDefault = $col['Default'];
                    $elInput = '';

                    foreach ($col['LabelOptions'] as $kselect => $vselect) {
                        $id_element = $this->prename . $colName . '_' . $kselect;
                        
                        if (Input::old($this->prename . $colName)) {
                            $optDefault = Input::old($this->prename . $colName);
                        }
                        
                        $el .= Form::radio($this->prename . $colName, $kselect, ($optDefault == $kselect), ['id' => $id_element, 'tabindex' => $col['TabIndex']]) . Form::label($id_element, $vselect);
                        
                        
                        $elInput .= '<div id="group_'.$id_element.'" class="radio-group row" style="display: none;">';
                        
                        foreach($col['Options'][$kselect] as $option) {
                            
                            $colNameOption = $option['Name'];

                            if (isset($option['Label']) && $option['Label'] != '') {
                                $labelOption = $option['Label'];
                            } else {
                                $labelOption = $colNameOption;
                            }

                            if (isset($option['Id']) && $option['Id'] != '') {
                                $idOption = $option['Id'];
                            } else {
                                $idOption = $colName;
                            }

                            $elInput .= '<div class="col-md-6">';
                            $elInput .= Form::label($this->prename . $option['Name'], $labelOption);
                            $elInput .= $this->htmlElement($model, $option);
                            $elInput .= '</div>';
                            
                        }
                        $elInput .= '</div>';
                        
                        $elInput .= '
                            <script type="text/javascript">
                                $( "#'.$id_element.'" ).on( "click", function() {
                                    $("#'.$idDiv.' .radio-group").hide();
                                    $( "#group_'.$id_element.'" ).show();

                                });
                            </script>';
                    }

                    $elInput .= '
                        <script type="text/javascript">
                            $( document ).ready(function() {
                                var selected = $("#'.$idDiv.' input:radio:checked").val();
                                var element = "#group_'.$colName.'_"+selected;
                                $( "#'.$idDiv.' .radio-group" ).hide();
                                $( element ).show();
                            });
                        </script>';
                    
                    $el .= $elInput;
                    $el .= '</div>';
                    
                    unset($id_element);
                    
                    break;
                    
                default:
                    $el = $this->htmlElement($model, $col);
            }
            
            if (isset($el)) {
                
                $element = Form::label($this->prename . $colName, $label) . $el;
                
                if (count($tabs) > 0) {// Se form_tabs() retornar configurado na classe modelo
                    if (empty($columns[$colName]['TabPage'])) {
                        throw new Exception('Campo "' . $colName . '" sem TabPage definido! ');
                    }
                    if (empty($columns[$colName]['Line']) || !isset($columns[$colName]['Line']))                    {
                        $columns[$colName]['Line'] = '1';
                    }
                    if (empty($columns[$colName]['Column'])) {
                        $columns[$colName]['Column'] = '1';
                    }
                    if (!isset($tabsElements[$columns[$colName]['TabPage']])) {
                        $tabsElements[$columns[$colName]['TabPage']] = '';
                    }
                    if (!isset($tabsElements[$columns[$colName]['TabPage']][$columns[$colName]['Line']][$columns[$colName]['Column']])) {
                        $tabsElements[$columns[$colName]['TabPage']][$columns[$colName]['Line']][$columns[$colName]['Column']] = '';
                    }
                    // Indices: [Tab][Linha][Coluna]
                    $tabsElements[$columns[$colName]['TabPage']][$columns[$colName]['Line']][$columns[$colName]['Column']] .= $element;
                } else {
                    // Configuração sem os tabs
                    if (!isset($noTabsElements[$columns[$colName]['Line']][$columns[$colName]['Column']])) {
                        $noTabsElements[$columns[$colName]['Line']][$columns[$colName]['Column']] = '';
                    }
                    $noTabsElements[$columns[$colName]['Line']][$columns[$colName]['Column']] .= $element;
                }
            }
        }

        if (count($tabs) > 0) {
            $ctab = new Tab();
            foreach ($tabs as $tabId => $tabTitle) {

                $divs = '';
                if (isset($tabsElements[$tabId])) {
                    ksort($tabsElements[$tabId]);
                    foreach ($tabsElements[$tabId] as $line) {
                        $html = '<div class="row">';
                        $cntCol = floor(12 / count($line));
                        foreach ($line as $elements) {
                            $html .= '<div class="col-md-' . $cntCol . '">' . $elements . '</div>';
                        }
                        $divs .= $html . '</div>';
                    }
                    $ctab->addPage($tabTitle, $tabId, '#' . $tabId, $divs, (!isset($active) ? $active = true : false));
                }
            }
            $form .= ($ctab->make());
        } else {

            $divs = '';
            foreach ($noTabsElements as $line) {
                $html = '<div class="row">';
                $cntCol = floor(12 / count($line));
                foreach ($line as $elements) {
                    $html .= '<div class="col-md-' . $cntCol . '">' . $elements . '</div>';
                }
                $divs .= $html . '</div>';
            }

            $form .= $divs;
        }
        
        $form .= '<div class="btn-group">' . Form::submit('Salvar', ['class' => 'btn btn-primary']) . $btnDel . Button::primary('Cancelar')->asLinkTo($host) . '</div>';

        $form .= Form::close();

        /*if (Session::has('errors')) {
            $messageError = '';
            $errors = Session::get('errors');
            foreach ($errors->all(':message') as $message) {
                $messageError .= Alert::danger($message)->close();
            }
            return $messageError . $form;
        }*/

        return $form;
    }
    
    public function htmlElement($model, $col) {
        
        $el = '';
        
        $colName = $col['Name'];

        switch ($col['Type']) {

            case 'int':
                if (!$col['PrimaryKey']) {

                    $el = Form::text($this->prename . $colName, Input::old($this->prename . $colName), ['id' => $this->prename . $colName, 'tabindex' => $col['TabIndex']]);
                    $el .= '
                        <script type="text/javascript">
                            $("#' . $this->prename . $colName . '").TouchSpin({
                              verticalbuttons: true,
                              verticalupclass: "glyphicon glyphicon-plus",
                              verticaldownclass: "glyphicon glyphicon-minus"
                            });
                        </script>';
                }
                break;

            case 'longtext':
                $el = Form::textarea($this->prename . $colName, Input::old($this->prename . $colName));
                break;

            case 'enum':
            case 'radio':
                $el = '<div class="input-group-addon">';
                $optDefault = $col['Default'];
                foreach ($col['Options'] as $kselect => $vselect) {
                    $id_element = $this->prename . $colName . '_' . $kselect;
                    if (Input::old($this->prename . $colName)) {
                        $optDefault = Input::old($this->prename . $colName);
                    }
                    $el .= Form::radio($this->prename . $colName, $kselect, ($optDefault == $kselect), ['id' => $id_element, 'tabindex' => $col['TabIndex']]) . Form::label($id_element, $vselect);
                }
                unset($id_element);
                $el .= '</div>';
                break;

            case 'select':
                if (!empty($model->$colName)) {
                    $col['Default'] = $model->$colName;
                }
                $el = Form::select($this->prename . $colName, $col['Options'], $col['Default'], ['tabindex' => $col['TabIndex']]);
                break;

            case 'date':
                $model->$colName = Strings::FormatDate($model->$colName);
                $el = Form::text($this->prename . $colName, Input::old($this->prename . $colName), ['class' => 'date', 'id' => $this->prename . $colName, 'tabindex' => $col['TabIndex']]);
                $el .= '<script type="text/javascript">
                            jQuery(function($){
                                $("#' . $this->prename . $colName . '").mask("99/99/9999");
                            });
                        </script>';
                break;

            case 'money':
                $model->$colName = Strings::FormatMoney($model->$colName);

                $el = Form::text($this->prename . $colName, Input::old($this->prename . $colName), ['class' => 'money', 'id' => $this->prename . $colName, 'tabindex' => $col['TabIndex']]);
                $el = InputGroup::withContents($el)->prepend('R$');
                break;

            case 'email':
                $el = Form::email($this->prename . $colName, Input::old($this->prename . $colName), ['id' => $this->prename . $colName, 'tabindex' => $col['TabIndex']]);
                break;
            case 'password':
                $el = Form::password($this->prename . $colName, Input::old($this->prename . $colName), ['id' => $this->prename . $colName, 'tabindex' => $col['TabIndex']]);
                break;

            case 'phone':
                $el = Form::text($this->prename . $colName, Input::old($this->prename . $colName), ['id' => $this->prename . $colName, 'tabindex' => $col['TabIndex']]);
                $el .= '<script type="text/javascript">
                            jQuery(function($){
                                $("#' . $this->prename . $colName . '").mask("(99) ?9999-99999");
                            });
                        </script>';
                break;
            case 'cep':
                $el = Form::text($this->prename . $colName, Input::old($this->prename . $colName), ['id' => $this->prename . $colName, 'tabindex' => $col['TabIndex']]);
                $el .= '<script type="text/javascript">
                            jQuery(function($){
                                $("#' . $this->prename . $colName . '").mask("99999-999");
                            });
                        </script>';
                break;
            case 'cpf':
                $el = Form::text($this->prename . $colName, Input::old($this->prename . $colName), ['id' => $this->prename . $colName, 'tabindex' => $col['TabIndex']]);
                $el .= '<script type="text/javascript">
                            jQuery(function($){
                                $("#' . $this->prename . $colName . '").mask("999.999.999-99");
                            });
                        </script>';
                break;
            case 'cnpj':
                $el = Form::text($this->prename . $colName, Input::old($this->prename . $colName), ['id' => $this->prename . $colName, 'tabindex' => $col['TabIndex']]);
                $el .= '<script type="text/javascript">
                            jQuery(function($){
                                $("#' . $this->prename . $colName . '").mask("?999.999.999/9999-99");
                            });
                        </script>';
                break;
            case 'varchar':
            default:
                $el = Form::text($this->prename . $colName, Input::old($this->prename . $colName), ['id' => $this->prename . $colName, 'tabindex' => $col['TabIndex']]);
        }

        return $el;
    }

}