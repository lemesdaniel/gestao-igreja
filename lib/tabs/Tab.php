<?php

/*
 * Licença Comercial
 * Código de Propriedade da Icone - http://www.iconeweb.com.br
 */

/*
 *  Exemplo de uso
        $tabs = new Tab();
        $tabs->addPage($title = 'Title Page1', $id = 'id1', $href = '#id1', "teste page 1" , $active = true);
        $tabs->addPage($title = 'Title Page2', $id = 'id2', $href = '#id2', "teste page 2");
        $tabs->addPage($title = 'Title Page3', $id = 'id3', $href = '#id3', "teste page 3");
        $tabs->addPage($title = 'Title Page4', $id = 'id4', $href = '#id4');
        $form->add($tabs->make());
 */

/**
 * Classe padrão para criação de tabs para formulários
 *
 * @author Jean Dias <jean@iconeweb.com.br>
 */
class Tab {

    /**
     * Armazena o html completo da tab a ser criada
     * @var string 
     */
    private $html = '';
    
    /**
     * Armazena as opções usadas na criação da tab
     * @var array
     */
    private  $options = array();

    /**
     * Função construtora da classe de tabs
     */
    public function __construct() {
        
    }

    /**
     * Armazena informações referente a cada page da tab
     * @param string $title Titulo da tab
     * @param string $id Id da tab
     * @param string $href Link da tab
     * @param string $html Conteudo de dentro da tab
     * @param boolean $active Indica se a tab está ativa ou não
     */
    public function addPage($title = 'Título', $id = 'id', $href = '#', $html = 'Sem informações', $active = false) {
        $this->options[] = [
            'title' => $title,
            'id' => $id,
            'href' => $href,
            'html' => $html,
            'active' => $active
        ];
    }
    
    /**
     * Retorna o html completo da tab criada
     * @return string
     */
    public function make() {

        $this->html .= '<ul class="nav nav-tabs" role="tablist">';
        
        foreach ($this->options as $option) {
            $this->html .= '<li'.($option['active'] ? ' class="active"' : '').'><a onfocus="this.blur()" href="' . $option['href'] . '" role="tab" data-toggle="tab">' . $option['title'] . '</a></li>';
        }
        
        $this->html .= '</ul>';
        $this->html .= '<div class="tab-content">';

        foreach ($this->options as $option) {
            $this->html .= '<div class="tab-pane' . ($option['active'] ? " active" : "") . '" id="' . $option['id'] . '">' . $option['html'] . '</div>';
        }

        $this->html .= '</div>';

        return $this->html;
    }
}
